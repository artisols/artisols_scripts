-- Requête : 01_Requete_dissolve.sql
-- ===
-- Fusion (dissolve) des entités contigues de la table spatiale en entrée
-- Les entités qui se touchent sont fusionnées quelle que soit la classe

-- Input parameters :
-- 	0 = input table
-- 	1 = output table 

-- =======================================================================

-- suppresion de la table output si elle existe
DROP TABLE IF EXISTS {1};

-- creation de multipolygones fonction de la géométrie :
--	* st_accum : fonction d'aggrégation pour construire des geometrycollection
--	* st_clusterintersecting : fonction d'aggrégation qui retourne un array de geometrycollections
--	* unnest : convertit un array en un jeu de lignes
--	* st_collectionextract : retourne une (multi-)géométrie en fonction d'un type (ici 3 = polygone)
WITH collect_extract AS
	(SELECT st_collectionextract(unnest(st_clusterintersecting(st_accum(geom))),3) as geom FROM {0}), 
-- 	creation d'un gid pour chaque entite de la table
	prod_row_nb as (SELECT row_number() OVER() as gid, geom FROM collect_extract )

-- recuperation du gid commun, et de la géométrie d'origine
--	* st_dump : retourne les géométries constitutives d'une (multi-)géométrie
SELECT gid, ((st_dump(geom)).geom) as geom
	INTO table_temp
	FROM prod_row_nb;

-- Fusion des polygones contingues et creation d'une nouvelle géométrie (enveloppe englobante)	
SELECT gid, st_union(st_makevalid(geom)) as geom
	INTO {1}
	FROM table_temp
	GROUP BY gid;

-- suppression de la table temporaire
DROP TABLE table_temp;

-- creation d'un index spatial
CREATE INDEX {1}_geom_gist ON {1} USING GIST (geom);