-- Requête : 02_Requete_IOU.sql
-- ===
-- Création d'une nouvelle table qui comporte toutes les intersections 2 à 2 possibles
-- entre la table produite fusionnée (prod_fusion_xx) et la table test fusionnée (test_fusion_xx)

-- Input parameters :
-- 	0 = prod table
-- 	1 = test table 
--	2 = output table (iou)

-- =======================================================================

-- Intersection par jointure gauche des 2 tables pour avoir les faux négatifs (entités non détectées par le classifieur). 
-- Conservation de toutes les entités test (référence), intersectées et non intersectées (valeur NULL dans colonne prod). 
-- Calcul des superficies des entités de chacune des couches, des intersections, des unions d’entités 2 à 2
-- Calcul de l’IoU ligne par ligne, entités 2 à 2. 

-- LEFT JOIN
DROP TABLE IF EXISTS left_join;
CREATE TEMPORARY TABLE left_join AS (
    SELECT prod.gid as prod_id, test.gid as test_id, prod.geom as prod_geom, test.geom as test_geom,
    ST_Area(prod.geom) as prod_surf,
    ST_Area(test.geom) as test_surf,
    ST_Area(ST_Intersection(ST_MakeValid(prod.geom), ST_MakeValid(test.geom))) as surf_int,
        ST_Area(ST_Union(ST_MakeValid(prod.geom), ST_MakeValid(test.geom))) as surf_uni
    FROM {1} AS test     
    LEFT JOIN {0} AS prod ON    
        ST_intersects(prod.geom, test.geom)
    GROUP BY prod_id, test_id, test.geom, prod.geom
    ORDER BY test_id                     
    );

-- ADD IoU COLUMN ON LEFT_JOIN
ALTER TABLE left_join
    ADD COLUMN IoU double precision;
UPDATE left_join
    SET IoU=100*surf_int/surf_uni;

-- Intersection par jointure droitedes 2 tables pour avoir les faux positifs (entités détectées à tort par le classifieur). 
-- Conservation de toutes les entités produites, intersectées et non intersectées (valeur NULL dans la colonne test).
-- Calcul des superficies des entités de chacune des couches, des intersections, des unions d’entités 2 à 2
-- Calcul de l’IoU ligne par ligne, entités 2 à 2.

-- RIGHT JOIN
DROP TABLE IF EXISTS right_join;
CREATE TEMPORARY TABLE right_join AS (
    SELECT prod.gid as prod_id, test.gid as test_id, prod.geom as prod_geom, test.geom as test_geom,
    ST_Area(prod.geom) as prod_surf,
    ST_Area(test.geom) as test_surf,
    ST_Area(ST_Intersection(ST_MakeValid(prod.geom), ST_MakeValid(test.geom))) as surf_int,
        ST_Area(ST_Union(ST_MakeValid(prod.geom), ST_MakeValid(test.geom))) as surf_uni
    FROM {1} AS test     
    RIGHT JOIN {0} AS prod ON    
        ST_intersects(prod.geom, test.geom)
    GROUP BY prod_id, test_id, test.geom, prod.geom
    ORDER BY prod_id                     
    );

-- ADD IoU COLUMN ON RIGHT_JOIN
ALTER TABLE right_join
    ADD COLUMN IoU double precision;
UPDATE right_join
    SET IoU=100*surf_int/surf_uni;

-- Union des tables left_join et right_join

-- UNION OF TABLES LEFT/RIGHT JOIN
DROP TABLE IF EXISTS {2};
CREATE TABLE {2} AS (     
    SELECT * FROM left_join         
    UNION     
    SELECT * FROM right_join     
    ORDER BY test_id);

-- creation d'un index spatial
CREATE INDEX {2}_prod_geom_gist ON {2} USING GIST (prod_geom);
CREATE INDEX {2}_test_geom_gist ON {2} USING GIST (test_geom);


UPDATE {2} 
SET iou=NULL WHERE iou =0;
        