-- Requête : 03_Requete_fp_tp_fn.sql
-- ===
-- Classification des résultats de classification en 3 classes :
--	* faux positifs
--	* faux négatifs
--	* vrais positifs

-- Input parameters :
-- 	0 = iou table

-- =======================================================================

-- Mise à jour de la table IoU
ALTER TABLE {0}
	DROP COLUMN IF EXISTS result_classif;
ALTER TABLE {0}
	ADD COLUMN result_classif varchar;

UPDATE {0}
	SET result_classif = 'faux negatif'
	WHERE {0}.prod_id IS NULL;     
UPDATE {0}
	SET result_classif = 'faux positif'
	WHERE {0}.test_id IS NULL;  
UPDATE {0}
	SET result_classif = 'vrai positif'
	WHERE {0}.test_id IS NOT NULL AND {0}.prod_id IS NOT NULL;