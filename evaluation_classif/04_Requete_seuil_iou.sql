-- Requête : 04_Requete_seuil_iou.sql
-- ===
-- Suppression des entités peu représentatives en fonction d'un seuil sur l'iou
-- n.b. cette opération ne conserve que les intersections valides (vrais positifs)

-- Input parameters :
-- 	0 = input table (output de 03_Requete_fp_tp_fn.sql)
--	1 = output table
-- 	2 = seuil (en %)

-- =======================================================================

DROP TABLE IF EXISTS {1};

CREATE TABLE {1} AS (     
	SELECT * FROM {0} WHERE iou >{2});

-- creation d'un index spatial
CREATE INDEX {1}_prod_geom_gist ON {1} USING GIST (prod_geom);
CREATE INDEX {1}_test_geom_gist ON {1} USING GIST (test_geom);