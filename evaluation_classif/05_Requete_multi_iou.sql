-- Requête : 05_Requete_multi_iou.sql
-- ===
-- Calcul de l'IoU par entité produite en fonction de toutes les entités test intersectées (plusieurs entités test intersectées pour une entité prod)
-- expression : 
--		soit * SI : surface intersectée
--		     * SP : surface de l'entité produite
--			 * ST : surface de l'entité test intersectée
-- 		IoU = SOMME(SI) / (SP + SOMME(ST) - SOMME(SI))

-- Input parameters :
-- 	0 = input table
-- 	1 = output table 

-- =======================================================================

DROP TABLE IF EXISTS {1};
CREATE TABLE {1} AS (     
    SELECT prod_id,
           SUM(surf_int) AS sum_int,
           (prod_surf+SUM(test_surf)-SUM(surf_int)) AS sum_union
    FROM {0}
    GROUP BY prod_id, prod_surf
    ORDER BY prod_id
    );
    
-- ADD IoU COLUMN
ALTER TABLE {1}
    ADD COLUMN IoU double precision;
UPDATE {1}
    SET IoU=100*sum_int/sum_union;