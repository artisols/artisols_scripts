-- Requête : 06_Requete_type_intersection.sql
-- ===
-- Classification des types d'intersection (fonction du nombre d'intersections en prod ou test) :
--		* agrégation
--		* fragmentation
--		* équivalent
--		* mixte (fragmentation et agrégation)

-- Input parameters :
-- 	0 = input table (output de 04_Requete_seuil_iou.sql)
-- 	1 = output table

-- =======================================================================

-- Création de la table des types d'intersection à partir de la table 
-- créée dans la requête 04_Requete_seuil_iou.sql 
-- (uniquement les vrais positifs au-delà d'un seuil d'IoU)
DROP TABLE IF EXISTS {1};

CREATE TABLE {1} AS (
	SELECT prod_id, test_id, prod_geom, test_geom, prod_surf, test_surf, iou  
	FROM {0}
	);

-- Comptage du nombre d’intersections des entités produites
ALTER TABLE {1}
	DROP COLUMN IF EXISTS nb_prod;
ALTER TABLE {1}
	ADD COLUMN nb_prod smallint DEFAULT 0;
	UPDATE {1}
	SET nb_prod = (
		SELECT COUNT (t1.test_id)
			FROM {0} AS t1
			WHERE ST_Intersects(prod_geom, test_geom) AND {1}.prod_id = t1.prod_id);

-- Comptage du nombre d’intersections des entités test
ALTER TABLE {1}
	DROP COLUMN IF EXISTS nb_test;
ALTER TABLE {1}
	ADD COLUMN nb_test smallint DEFAULT 0;
	UPDATE {1}
    SET nb_test = (
		SELECT COUNT (t1.prod_id)
		FROM {0} AS t1
		WHERE ST_Intersects(prod_geom, test_geom) AND {1}.test_id=t1.test_id);
				  
-- Définition du type d’intersection (fragmentation, agrégation, équivalence et mixte). 
-- 	* fragmentation : nb INT(entités test) > nb INT(entités produites)
--	* agrégation    : nb INT(entités test) < nb INT(entités produites)
--	* équivalent    : nb INT(entités test) = 1 AND nb INT(entités prod) = 1
--	* mixte         : nb INT(entités test) > 1 AND nb INT(entités prod) > 1

ALTER TABLE {1}
	DROP COLUMN IF EXISTS type_int;
ALTER TABLE {1}
	ADD COLUMN type_int varchar;
	UPDATE {1}
	SET type_int = 'fragmentation'
		WHERE {1}.nb_test > {1}.nb_prod;
	UPDATE {1}
	SET type_int = 'agregation'
		WHERE {1}.nb_test < {1}.nb_prod;    
	UPDATE {1}
	SET type_int = 'equivalent'
		WHERE {1}.nb_test = 1 AND {1}.nb_prod =1; 
	UPDATE {1}
	SET type_int = 'mixte'
		WHERE {1}.nb_test > 1 AND {1}.nb_prod >1;