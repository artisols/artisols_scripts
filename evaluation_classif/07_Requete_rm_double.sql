-- Requête : 07_Requete_rm_double.sql
-- ===
-- Gestion des doublons liés à la catégorie mixte
-- Description :
-- Les entités mixtes agrègent ou fragmentent également. Il y a donc des doublons dans la table.
-- 2 cas possibles : 
-- 	* laisser de côté le type de qualification mixte et considérer uniquement l'entité comme agrégeant ou fragmentant. 
-- 		-> suppression des lignes où le type_int = 'mixte' 
-- 	* considérer ces entités comme étant uniquement des entités mixtes (chose faite ici)
--		-> créer d'une table temporaire dans laquelle sont recensées les entités en doublon 
--         (même id mais type_int différent, c'est-à-dire agrégation/fragmentation ET mixte)
-- 		-> suppression du doublon, soit la ligne où l'entité est référencée en fragmentation ou agrégation 

-- Input parameters :
-- 	0 = input table

-- =======================================================================

-- Création d'une table temporaire pour extraire uniquement les doublons
DROP TABLE IF EXISTS doublon;

CREATE TABLE doublon AS (
	SELECT DISTINCT *
	FROM {0} AS t1
	WHERE EXISTS (
		SELECT *
		FROM {0} AS t2
		WHERE t1.prod_id = t2.prod_id AND t1.type_int <> t2.type_int )
		)
	ORDER BY prod_id;

-- Création d'une table temporaire à partir de la table temporaire dans laquelle 
-- sont supprimées les lignes de type_int = 'mixte' (conserve uniquement le type agrégation ou fragmentation)
DROP TABLE IF EXISTS doublon_ok;

CREATE TABLE doublon_ok AS (
	SELECT * 
	FROM doublon 
	WHERE type_int != 'mixte'
	)
	ORDER BY prod_id;

-- Suppression, dans la table sortie de la requête 06_Requete_{0}ion.sql, des entités 
-- référencées dans la table doublon_ok. Restent uniquement des occurrences uniques d'entités avec un
-- seul et unique type d'intersection.

DELETE FROM {0} AS t1 WHERE EXISTS (
	SELECT *
	FROM doublon_ok AS t2
	WHERE t1.prod_id = t2.prod_id AND t1.test_id = t2.test_id AND t1.type_int = t2.type_int 
    ORDER BY prod_id
	);

DROP TABLE doublon;
DROP TABLE doublon_ok;