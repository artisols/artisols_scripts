-- Requête : 08_Requete_synthese_results.sql
-- ===
-- Table de synthèse avec :
--  * nombre et % d'entités test et prod
--	* nombre et % de faux positifs
--	* nombre et % de faux négatifs
--	* nombre et % de vrais positifs
--	* nombre et % d'agrégation
--	* nombre et % de fragmentation
--	* nombre et % d'équivalent (test et prod)
--	* nombre et % de mixte
--	* rappel, précision et f-score

-- Input parameters :
-- 	0 = input table iou
--	1 = input table type_intersect
-- 	2 = output table

-- =======================================================================

DROP TABLE IF EXISTS {2};

CREATE TABLE {2} (
	fp double precision, 
	fp_percent double precision, 
	fn double precision, 
	fn_percent double precision, 
	tp_prod double precision, 
	tp_prod_percent double precision, 
	tp_test double precision, 
	entites_prod double precision, 
	entites_test double precision,
	rappel double precision, 
	preciz double precision, 
	fscore double precision, 
	nb_agreg double precision, 
	nb_fragment double precision, 
	nb_equivalent double precision, 
	nb_mixte double precision, 
	agreg_percent double precision, 
	fragment_percent double precision, 
	equivalent_percent_prod double precision, 
	equivalent_percent_test double precision,
	mixte_percent double precision);

INSERT INTO {2}(fp) VALUES(
	(SELECT COUNT(test.*) FROM {0} AS test WHERE test_id is null)
	);

UPDATE {2}
	SET fn = (SELECT COUNT(test.*)
			FROM {0} AS test 
			WHERE prod_id is null),
		tp_prod = (SELECT COUNT(DISTINCT test.prod_id) 
			FROM {0} AS test 
			WHERE test_id is not null and prod_id is not null),
		tp_test = (SELECT COUNT(DISTINCT test.test_id) 
			FROM {0} AS test 
			WHERE test_id is not null and prod_id is not null),
		nb_agreg = (SELECT COUNT(DISTINCT typ.prod_id)
			FROM {1} AS typ 
			WHERE type_int = 'agregation'),
		nb_fragment = (SELECT COUNT(DISTINCT typ.prod_id)
			FROM {1} AS typ 
			WHERE type_int = 'fragmentation'),
		nb_equivalent = (SELECT COUNT(DISTINCT typ.prod_id)
			FROM {1} AS typ 
			WHERE type_int = 'equivalent'),
		nb_mixte = (SELECT COUNT(DISTINCT typ.prod_id)
			FROM {1} AS typ 
			WHERE type_int = 'mixte');
        
UPDATE {2}
	SET entites_prod = fp+tp_prod,
		entites_test = fn + tp_test;

UPDATE {2}
	SET fp_percent = 100*fp/NULLIF(entites_prod,0),
		tp_prod_percent = 100*tp_prod/NULLIF(entites_prod,0),
		fn_percent = 100*fn/NULLIF(entites_test,0),
		agreg_percent = 100*nb_agreg/NULLIF(entites_prod,0),
		fragment_percent = 100*nb_fragment/NULLIF(entites_prod,0),
		equivalent_percent_prod = 100*nb_equivalent/NULLIF(entites_prod,0),
		equivalent_percent_test = 100*nb_equivalent/NULLIF(entites_test,0),
		mixte_percent = nb_mixte/100*NULLIF(entites_prod,0);
        
UPDATE {2}
	SET rappel = tp_test/NULLIF(tp_test+fn,0),
		preciz = tp_prod/NULLIF(entites_prod,0);

UPDATE {2}
	SET fscore = 2*(preciz*rappel)/NULLIF(preciz+rappel,0);