-- Requête : 09_Requete_synthese_results_fscore_surf_equi.sql
-- ===
-- Table de synthèse avec, par range de surface (intersections 1 à 1):
--	* nombre faux positifs
--	* nombre de faux négatifs
--	* nombre de vrais positifs
--	* rappel, précision et f-score

-- Input parameters :
-- 	0 = input table iou
-- 	1 = output table

-- =======================================================================

DROP TABLE IF EXISTS {1};

CREATE TABLE {1} (
	tp_prod_surf1 double precision, 
	tp_test_surf1 double precision,
	tp_prod_surf2 double precision,
	tp_test_surf2 double precision,
	tp_prod_surf3 double precision,
	tp_test_surf3 double precision,
	tp_prod_surf4 double precision,
	tp_test_surf4 double precision,
	tp_prod_surf5 double precision,
	tp_test_surf5 double precision,
	tp_prod_surf6 double precision,
	tp_test_surf6 double precision,
	tp_prod_surf7 double precision,
	tp_test_surf7 double precision,
	tp_prod_surf8 double precision,
	tp_test_surf8 double precision,
	fn_surf1 double precision,
	fn_surf2 double precision,
	fn_surf3 double precision,
	fn_surf4 double precision,
	fn_surf5 double precision,
	fn_surf6 double precision,
	fn_surf7 double precision,
	fn_surf8 double precision,
	fp_surf1 double precision,
	fp_surf2 double precision,
	fp_surf3 double precision,
	fp_surf4 double precision,
	fp_surf5 double precision,
	fp_surf6 double precision,
	fp_surf7 double precision,
	fp_surf8 double precision,
	fscore_surf1 double precision,
	fscore_surf2 double precision,
	fscore_surf3 double precision,
	fscore_surf4 double precision,
	fscore_surf5 double precision,
	fscore_surf6 double precision,
	fscore_surf7 double precision,
	fscore_surf8 double precision,
	rappel_surf1 double precision,
	preciz_surf1 double precision,
	rappel_surf2 double precision,
	preciz_surf2 double precision,		
	rappel_surf3 double precision,
	preciz_surf3 double precision,
	rappel_surf4 double precision,
	preciz_surf4 double precision,
	rappel_surf5 double precision,
	preciz_surf5 double precision,
	rappel_surf6 double precision,
	preciz_surf6 double precision,		
	rappel_surf7 double precision,
	preciz_surf7 double precision,
	rappel_surf8 double precision,
	preciz_surf8 double precision)
	;

INSERT INTO {1}(tp_prod_surf1) VALUES(
	(SELECT COUNT(DISTINCT bati.prod_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 2.25 and test_surf < 10 and type_int = 'equivalent')
	);

UPDATE {1}
	SET tp_prod_surf2 = (SELECT COUNT(DISTINCT bati.prod_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 10 and test_surf < 100 and type_int = 'equivalent'),
		tp_prod_surf3 = (SELECT COUNT(DISTINCT bati.prod_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 100 and test_surf < 200 and type_int = 'equivalent'),
		tp_prod_surf4 = (SELECT COUNT(DISTINCT bati.prod_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 200 and test_surf < 500 and type_int = 'equivalent'),
		tp_prod_surf5 = (SELECT COUNT(DISTINCT bati.prod_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 500 and test_surf < 1000 and type_int = 'equivalent'),
		tp_prod_surf6 = (SELECT COUNT(DISTINCT bati.prod_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 1000 and test_surf < 50000 and type_int = 'equivalent'),
		tp_prod_surf7 = (SELECT COUNT(DISTINCT bati.prod_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 50000 and test_surf < 100000 and type_int = 'equivalent'),
		tp_prod_surf8 = (SELECT COUNT(DISTINCT bati.prod_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 100000 and test_surf < 125000 and type_int = 'equivalent'),
		
		tp_test_surf1 = (SELECT COUNT(DISTINCT bati.test_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 2.25 and test_surf < 10 and type_int = 'equivalent'),
		tp_test_surf2 = (SELECT COUNT(DISTINCT bati.test_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 10 and test_surf < 100 and type_int = 'equivalent'),
		tp_test_surf3 = (SELECT COUNT(DISTINCT bati.test_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 100 and test_surf < 200 and type_int = 'equivalent'),
		tp_test_surf4 = (SELECT COUNT(DISTINCT bati.test_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 200 and test_surf < 500 and type_int = 'equivalent'),
		tp_test_surf5 = (SELECT COUNT(DISTINCT bati.test_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 500 and test_surf < 1000 and type_int = 'equivalent'),
		tp_test_surf6 = (SELECT COUNT(DISTINCT bati.test_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 1000 and test_surf < 50000 and type_int = 'equivalent'),
		tp_test_surf7 = (SELECT COUNT(DISTINCT bati.test_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 50000 and test_surf < 100000 and type_int = 'equivalent'),
		tp_test_surf8 = (SELECT COUNT(DISTINCT bati.test_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= 100000 and test_surf < 125000 and type_int = 'equivalent'),
		
		fn_surf1 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE prod_id is null and test_surf >= 2.25 and test_surf < 10),
		fn_surf2 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE prod_id is null and test_surf >= 10 and test_surf < 100),
		fn_surf3 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE prod_id is null and test_surf >= 100 and test_surf < 200),
		fn_surf4 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE prod_id is null and test_surf >= 200 and test_surf < 500),
		fn_surf5 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE prod_id is null and test_surf >= 500 and test_surf < 1000),
		fn_surf6 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE prod_id is null and test_surf >= 1000 and test_surf < 50000),
		fn_surf7 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE prod_id is null and test_surf >= 50000 and test_surf < 100000),
		fn_surf8 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE prod_id is null and test_surf >= 100000 and test_surf < 125000),
		
		fp_surf1 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE test_id is null and prod_surf >= 2.25 and prod_surf < 10),
		fp_surf2 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE test_id is null and prod_surf >= 10 and prod_surf < 100),
		fp_surf3 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE test_id is null and prod_surf >= 100 and prod_surf < 200),
		fp_surf4 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE test_id is null and prod_surf >= 200 and prod_surf < 500),
		fp_surf5 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE test_id is null and prod_surf >= 500 and prod_surf < 1000),
		fp_surf6 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE test_id is null and prod_surf >= 1000 and prod_surf < 50000),
		fp_surf7 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE test_id is null and prod_surf >= 50000 and prod_surf < 100000),
		fp_surf8 = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE test_id is null and prod_surf >= 100000 and prod_surf < 125000);

UPDATE {1}
	SET rappel_surf1 = tp_test_surf1/NULLIF(tp_test_surf1+fn_surf1,0),
		preciz_surf1 = tp_prod_surf1/NULLIF(tp_prod_surf1+fp_surf1,0),
		rappel_surf2 = tp_test_surf2/NULLIF(tp_test_surf2+fn_surf2,0),
		preciz_surf2 = tp_prod_surf2/NULLIF(tp_prod_surf2+fp_surf2,0),		
		rappel_surf3 = tp_test_surf3/NULLIF(tp_test_surf3+fn_surf3,0),
		preciz_surf3 = tp_prod_surf3/NULLIF(tp_prod_surf3+fp_surf3,0),
		rappel_surf4 = tp_test_surf4/NULLIF(tp_test_surf4+fn_surf4,0),
		preciz_surf4 = tp_prod_surf4/NULLIF(tp_prod_surf4+fp_surf4,0),
	    rappel_surf5 = tp_test_surf5/NULLIF(tp_test_surf5+fn_surf5,0),
		preciz_surf5 = tp_prod_surf5/NULLIF(tp_prod_surf5+fp_surf5,0),
		rappel_surf6 = tp_test_surf6/NULLIF(tp_test_surf6+fn_surf6,0),
		preciz_surf6 = tp_prod_surf6/NULLIF(tp_prod_surf6+fp_surf6,0),		
		rappel_surf7 = tp_test_surf7/NULLIF(tp_test_surf7+fn_surf7,0),
		preciz_surf7 = tp_prod_surf7/NULLIF(tp_prod_surf7+fp_surf7,0),
		rappel_surf8 = tp_test_surf8/NULLIF(tp_test_surf8+fn_surf8,0),
		preciz_surf8 = tp_prod_surf8/NULLIF(tp_prod_surf8+fp_surf8,0);
		
		
UPDATE {1}
	SET fscore_surf1 = 2*(preciz_surf1*rappel_surf1)/NULLIF(preciz_surf1+rappel_surf1,0),
		fscore_surf2 = 2*(preciz_surf2*rappel_surf2)/NULLIF(preciz_surf2+rappel_surf2,0),
		fscore_surf3 = 2*(preciz_surf3*rappel_surf3)/NULLIF(preciz_surf3+rappel_surf3,0),
		fscore_surf4 = 2*(preciz_surf4*rappel_surf4)/NULLIF(preciz_surf4+rappel_surf4,0),
		fscore_surf5 = 2*(preciz_surf5*rappel_surf5)/NULLIF(preciz_surf5+rappel_surf5,0),
		fscore_surf6 = 2*(preciz_surf6*rappel_surf6)/NULLIF(preciz_surf6+rappel_surf6,0),
		fscore_surf7 = 2*(preciz_surf7*rappel_surf7)/NULLIF(preciz_surf7+rappel_surf7,0),
		fscore_surf8 = 2*(preciz_surf8*rappel_surf8)/NULLIF(preciz_surf8+rappel_surf8,0);