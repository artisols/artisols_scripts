-- Requête : 10_Requete_urban_density.sql
-- === Calcul de la densité urbaine par tuile

-- Input parameters :
-- 	0 = input table test
--	1 = input table de synthèse par patch

-- ================================== 

ALTER TABLE {1}
DROP COLUMN IF EXISTS urb_density;
ALTER TABLE {1}
ADD COLUMN urb_density double precision;
UPDATE {1}
SET urb_density = tile.surf FROM (SELECT SUM(ST_AREA({0}.geom))/110250000*100 as surf 
							   from {0}) tile 