-- Requête : 11_Requete_mean_area.sql
-- ===
-- Calcul de la surface moyenne des entités par tuile

-- Input parameters :
-- 	0 = input table test
--	1 = input table de synthèse par patch

-- ================================== 

ALTER TABLE {1}
DROP COLUMN IF EXISTS mean_area;
ALTER TABLE {1}
ADD COLUMN mean_area double precision;
UPDATE {1}
SET mean_area = mean.area FROM (SELECT AVG(tile.area) area from(SELECT ST_AREA({0}.geom) as area FROM {0}) tile) mean

