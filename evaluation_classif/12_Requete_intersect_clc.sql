-- Requête : 12_Requete_intersect_clc.sql
-- ===
-- Identification des entités test qui intersectent la couche de bati clc et qui sont donc considérées comme étant dans la tâche urbaine (et donc de bâti dense)

-- Input parameters :
-- 	0 = input table iou

-- clc_bati_merge_occitanie = vecteur du bâti dense de Corine Land Cover (une seule entité)

-- ================================== 


ALTER TABLE {0}
DROP COLUMN IF EXISTS inter_clc ;

ALTER TABLE {0}
ADD COLUMN inter_clc smallint DEFAULT NULL;

UPDATE {0} AS test
SET inter_clc = (SELECT COUNT (DISTINCT bati.test_id)
                  FROM {0} AS bati, input_data.clc_occitanie AS clc
                  WHERE ST_INTERSECTS(bati.test_geom, clc.geom) and test.test_id = bati.test_id);