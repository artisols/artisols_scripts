-- Requête : 14_Requete_synthese_results_fscore_surf_iou.sql
-- ===
-- Table de synthèse avec, par range de surface et seuil d'IoU :
--	* nombre de faux positifs
--	* nombre de faux négatifs
--	* nombre de vrais positifs
--  * nombre d'entités test et prod
--	* rappel, précision et f-score

-- Input parameters :
-- 	0 = input table iou
-- 	1 = output table  

-- =======================================================================

ALTER TABLE {1}
ADD COLUMN surf{6}_iou{5}_tp_prod double precision,
ADD COLUMN surf{6}_iou{5}_tp_test double precision,
ADD COLUMN surf{6}_iou{5}_fn double precision,
ADD COLUMN surf{6}_iou{5}_fp double precision,
ADD COLUMN surf{6}_iou{5}_rappel double precision,
ADD COLUMN surf{6}_iou{5}_preciz double precision,
ADD COLUMN surf{6}_iou{5}_fscore double precision;
		
 

UPDATE {1}
	SET surf{6}_iou{5}_tp_prod = (SELECT COUNT(DISTINCT bati.prod_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= {3} and test_surf < {4} and iou > {2}),
		surf{6}_iou{5}_tp_test = (SELECT COUNT(DISTINCT bati.test_id) FROM {0} AS bati WHERE test_id is not null and prod_id is not null and test_surf >= {3} and test_surf < {4} and iou > {2}),		
		surf{6}_iou{5}_fn = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE prod_id is null and test_surf >= {3} and test_surf < {4} or test_surf >= {3} and test_surf < {4} and iou < {2}),		
		surf{6}_iou{5}_fp = (SELECT COUNT(bati.*) FROM {0} AS bati WHERE test_id is null and prod_surf >= {3} and prod_surf < {4} or prod_surf >= {3} and prod_surf < {4} and iou < {2});

UPDATE {1}
	SET surf{6}_iou{5}_rappel = surf{6}_iou{5}_tp_test/NULLIF(surf{6}_iou{5}_tp_test+surf{6}_iou{5}_fn,0),
		surf{6}_iou{5}_preciz = surf{6}_iou{5}_tp_prod/NULLIF(surf{6}_iou{5}_tp_prod+surf{6}_iou{5}_fp,0);

UPDATE {1}
	SET surf{6}_iou{5}_fscore = 2*(surf{6}_iou{5}_preciz*surf{6}_iou{5}_rappel)/NULLIF(surf{6}_iou{5}_preciz+surf{6}_iou{5}_rappel,0);
	
/* ALTER TABLE {1}
DROP COLUMN IF EXISTS test; */

