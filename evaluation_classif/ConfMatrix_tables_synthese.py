# Remet en forme la matrice de confusion générée en .csv (otb) et calcule les métriques associées

from pathlib import Path
import sys
import numpy as np
import os

def precision(label, confusion_matrix):
	col = confusion_matrix[:, label]
	return confusion_matrix[label, label] / col.sum()

def recall(label, confusion_matrix):
	row = confusion_matrix[label, :]
	return confusion_matrix[label, label] / row.sum()

def CreateFile (id_tile, matrix) :

	# Search matrix confusion headers
	with open(matrix, 'r') as csv:
		for ln in csv:
			if ln.startswith('#Reference'):
				lab_refe = ln.split(':')[1].strip().split(',')
			if ln.startswith('#Produced'):
				lab_prod = ln.split(':')[1].strip().split(',')

	# Check confusion matrix dimension and load matrix confusion
	cm = np.genfromtxt(matrix, delimiter=',')
	
	if len(lab_refe) == len(lab_prod):
		cm = cm
	
	if len(lab_refe) == 1 and len(lab_prod) == 1 and lab_prod[0] == '0' and lab_refe[0] == '0' :
		nc = np.array([0])
		cm = np.column_stack((cm, nc))
		nr = np.array([0,0])
		cm = np.vstack((cm, nr))
	
	if len(lab_refe) < len(lab_prod) and lab_prod[0] == '0':	
		cm = cm[:,1:]
		
	if len(lab_refe) == 2 and len(lab_prod) == 1 and lab_prod[0] == '0':
		cm = np.reshape(cm,(-1,1))
		nc = np.array([0,0])
		cm = np.column_stack((cm, nc))

	# Evaluation
	for label in range(len(lab_refe)):
		if label == 1 :
			file.write('{},{},{},{},{},{},{},{}\n'.format(id_tile, cm[0,0], cm[0,1], cm[1,0], cm[1,1], (precision(label, cm)), recall(label, cm), 2*precision(label, cm)*recall(label, cm)/(precision(label, cm)+recall(label, cm))))
		if len(lab_refe) == 1 :
			file.write('{},{},{},{},{},'','',''\n'.format(id_tile, cm[0,0], cm[0,1], cm[1,0], cm[1,1],))

# =====================================================================================================

if __name__ == "__main__": 

	# argv[1]: confmat csv path
	path_dir = sys.argv[1]
	
	listMatrix = os.listdir(path_dir)
	
	#Creation of the file with the matrix and the headers
	output_matrix = os.path.join(path_dir, 'matrix_synthese.csv')
	print('output_matrix :', output_matrix)
	file = open(output_matrix, 'w')
	file.write('id_tile,prod0_ref0,prod0_ref255,prod255_ref0,prod255_ref255,precision_pxl,rappel_pxl,fscore_pxl\n')
	
	for matrix in listMatrix:
		if ".csv" in matrix and 'tile' in matrix : 
			path_matrix = os.path.join(path_dir, matrix)
			id_tile = matrix.split('_ConfMat.csv')[0]
			print('id_tile :', id_tile)
			CreateFile (id_tile, path_matrix)
	
	file.close()	
	
# /home/kenji/WORK/ARTISOLS/OUTPUT_VALID_TILES/MAP_TILES_2019/RASTER/tile98001_126001_ConfMat.csv