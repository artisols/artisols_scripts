# Supprime les entités classées 255 dans toutes les tables d'une BD Postgis

import sys, os, time
import psycopg2


def listTables(dbconnection):
    """list of both tables prod and test with same tile id
    @param dbconnection <class object> psycopg2 connection instance
    @return list of lists as [tile_id, table_test, table_prod]
    """
    sqlquer = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'"
    cursor = dbconnection.cursor()
    cursor.execute(sqlquer)
    list_tables = cursor.fetchall()
    list_clean = [i[0] for i in list_tables if 'tile' in i[0]]
    
    dual_list = list()
    for table in list_clean :
        dual_list.append(table)

    return dual_list
		
def delete(dbconnection, table_name):
    """list of both tables prod and test with same tile id
    @param dbconnection <class object> psycopg2 connection instance
    @return list of lists as [tile_id, table_test, table_prod]
    """
    sqlquer = "DELETE FROM {} WHERE class = 255".format(table_name)
    print(sqlquer)
    cursor = dbconnection.cursor()
    cursor.execute(sqlquer)
    
# --- main ---

if __name__ == "__main__":
    # get sql queries directory
    dbname = sys.argv[1]

    # connect database
    try:
        connection = psycopg2.connect(user = "postgres", password = "admin", host = "10.34.192.163", port = "5432", database = dbname)
    except:
        print('probleme de connexion a la database postgres')
    
    list_table = listTables(connection)
    print(list_table)

    for t in list_table : 
        delete(connection, t)
    connection.commit()
