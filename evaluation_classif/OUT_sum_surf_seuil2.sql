-- somme les entités en fonction de leur surface et de l'iou (ici iou = 0 donc entités non détectées)

select test_surf, sum(count)
  from (select test_surf, 
               count(test_surf)
          from total_iou_surf WHERE iou = 0 and test_surf >=2.25 and test_surf<10 group by test_surf) count 
      group by test_surf
	  order by test_surf; 
	  
-- somme les entités en fonction seuil/intervalle de surface et de l'iou



create table sum_surf (seuils_surf varchar(50), iou_non_seuil integer, iou_4 integer, iou_30 integer);
INSERT INTO sum_surf (seuils_surf) VALUES ('2.25 - 10 m²', '10 - 50 m²', '50 - 100 m²', '100 - 500 m²','500 - 1000 m²', 
											'1000 - 5000 m²', '5000 - 10000 m²', '10000 - 50000 m²', '50000 - 100000 m²', '100000 - 125000 m²');
											
INSERT INTO sum_surf (iou_non_seuil) VALUES (select sum(count)
												from (select test_surf, 
												count(test_surf)
												from total_iou_surf WHERE iou = 0 and test_surf >=2.25 and test_surf<10 group by test_surf) count)




('2.25 - 10 m²', (select sum(count)
from (select test_surf, 
count(test_surf)
from total_iou_surf WHERE iou = 0 and test_surf >=2.25 and test_surf<10 group by test_surf) count)), 
('10 - 50 m²', (select sum(count)
from (select test_surf, 
count(test_surf)
from total_iou_surf WHERE iou = 0 and test_surf >=10 and test_surf<50 group by test_surf) count)), 
('50 - 100 m²', (select sum(count)
from (select test_surf, 
count(test_surf)
from total_iou_surf WHERE iou = 0 and test_surf >=50 and test_surf<100 group by test_surf) count)), 
('100 - 500 m²', (select sum(count)
from (select test_surf, 
count(test_surf)
from total_iou_surf WHERE iou = 0 and test_surf >=100 and test_surf<500 group by test_surf) count )),
('500 - 1000 m²', (select sum(count)
from (select test_surf, 
count(test_surf)
from total_iou_surf WHERE iou = 0 and test_surf >=500 and test_surf<1000 group by test_surf) count)), 
('1000 - 5000 m²', (select sum(count)
from (select test_surf, 
count(test_surf)
from total_iou_surf WHERE iou = 0 and test_surf >=1000 and test_surf<5000 group by test_surf) count)), 
('5000 - 10000 m²', (select sum(count)
from (select test_surf, 
count(test_surf)
from total_iou_surf WHERE iou = 0 and test_surf >=5000 and test_surf<10000 group by test_surf) count)),
('10000 - 50000 m²', (select sum(count)
from (select test_surf, 
count(test_surf)
from total_iou_surf WHERE iou = 0 and test_surf >=10000 and test_surf<50000 group by test_surf) count)),
('50000 - 100000 m²', (select sum(count)
from (select test_surf, 
count(test_surf)
from total_iou_surf WHERE iou = 0 and test_surf >=50000 and test_surf<100000 group by test_surf) count)), 
('100000 - 125000 m²', (select sum(count)
from (select test_surf, 
count(test_surf)
from total_iou_surf WHERE iou = 0 and test_surf >=100000 and test_surf<125000 group by test_surf) count));
		  
