# Données de base:

- carte d'occupation du sol de l'Occitanie produite pour les années 2015 à 2019 - raster  
/home/kenji/WORK/ARTISOLS/DATA_REMI/cartes/2019/artisols_map_2019.tif
- données de référence/test (BD créée à partir de diverses BD) - vecteur  
/home/kenji/WORK/ARTISOLS/BD_OCS2018/BATI_OCCITANIE2018.shp
- centroïdes des patches/emprises qui ont servi pour le training - vecteur  
/home/kenji/WORK/ARTISOLS/DATA_REMI/cartes/2019

Les étapes suivantes sont réalisées pour chaque année (ici 2019)  

	Pour l'étude d'une autre année (2015 par exemple), penser avant tout à redécouper la carte (artisols_map_2015) en fonction de artisols_map_2019 (ref) pour produire ensuite le même maillage (création des patches)  
	otbcli_Superimpose -inr 2019/artisols_map_2019.tif -inm 2015/artisols_map_2015.tif -out "2015/artisols_map_2015_ref2019.tif?&gdal:co:COMPRESS=DEFLATE" uint8 -interpolator nn

Etapes :

# Redécouper la carte produite selon les limites administratives de l'Occitanie 

Des tuiles d'évaluation (prod et test) vont être produites sur l'ensemble de la région, incluant des tuiles intersectant les bordures administratives mais n'étant pas entièrement incluses dans les délimitations. La BD de référence/test (BD Topo) étant découpée en fonction des limites administratives, garder la carte produite tel quel (et donc de la donnée hors limites administratives) reviendrait à générer des faux négatifs dans des tuiles situées aux bordures lors de l'évaluation.   
 
-rasterisation du vecteur des limites administratives de la région Occitanie qui servira de masque :  

	gdal_rasterize -te 371564.250 6127638.000 860445.750 6456741.000 -tr 1.5 1.5 -ot Byte -co COMPRESS=DEFLATE adm_occitanie.shp adm_occitanie.tif -burn 1
	
-Redécoupage de la carte en fonction du masque :   

	otbcli_BandMath -il artisols_map_2019.tif ~/WORK/ARTISOLS/SCRIPTS/Extract_Patch/adm_occitanie/adm_occitanie.tif -out "artisols_map_2019_maskOcc.tif?&gdal:co:COMPRESS=DEFLATE" uint8 -exp im1b1*im2b1	
	
# Générer les patches/emprises qui ont servi pour le training afin de ne pas les réutiliser pour la validation (évaluation de la classification)

	training_patches_dev.py

		<répertoire d'entrée des centroïdes des patches qui ont servi pour l'entraînement>
		/home/kenji/WORK/ARTISOLS/DATA_REMI/cartes/2019

		<répertoire de sortie dans lequel seront stockés les shape des patches/emprises>
		/home/kenji/WORK/ARTISOLS/OUTPUT_POLY_PATCHES/POLY_PATCHES_2019

		<chemin du fichier en sortie de l'ensemble des patches de trainning mergés sur toute la région Occitanie>
   		/home/kenji/WORK/ARTISOLS/OUTPUT_POLY_PATCHES/POLY_PATCHES_2019/merge_poly_patches_2019.shp

# Créer des patches de validation (donnée de référence) et de classification sur l'ensemble de l'Occitanie à un pas régulier et en excluant les patches utilisés pour le training (masque)
		
	extract_patch_prod_4Eval.py

		<carte d'occupation du sol de 2019 (redécoupée Occitanie) - raster>
		/home/kenji/WORK/ARTISOLS/DATA_REMI/cartes/2019/artisols_map_2019_maskOcc.tif.tif 
		
		<contours administratifs de la région Occitanie - vecteur>
		/home/kenji/WORK/ARTISOLS/SCRIPTS/Extract_Patch/adm_occitanie/adm_occitanie.shp 
		  
		<répertoire où seront stockées les patches en sortie>
		/home/kenji/WORK/ARTISOLS/OUTPUT_VALID_TILES/MAP_TILES_2019 
		   
		<donnée de référence (BD créée à partir de diverses BD) - vecteur>
		/home/kenji/WORK/ARTISOLS/BD_OCS2018/BATI_OCCITANIE2018.shp

		<patches/emprises du training de l'Occitanie>
		/home/kenji/WORK/ARTISOLS/OUTPUT_POLY_PATCHES/POLY_PATCHES_2019/merge_poly_patches_2019.shp
			
		
Ce script produit dans un premier temps (avant de les supprimer, après que les vecteurs aient été créés):
  - les patches de classification (raster) : la carte d'Occitanie est découpée en un maillage (143 mailles/patches de 7000 pixels de côté) - *tile_prod* (valeur 1:bâti, 2:classe autre, 255:NoData/cloud)
  - les patches des données de référence (raster) : la couche de référence est découpée en 143 patches (à partir des *tile_prod* qui servent désormais de référence) - *tile_test*
  - les patches de training/mask (raster) : la couche *merge_poly_patches_2019* est découpée en 143 patches (à partir des *tile_prod*) - *tile_mask*
 	  
Puis :
 - il applique le masque (les patches de training) sur les patches de classification - *tile_prod_mask* (raster)
 - il applique le masque (les patches de training + valeur 255 NoData) sur les patches de données de référence - *tile_test_mask* (raster)
 - il produit les matrices de confusion (.csv) par patch
 	  
Puis :
 - il vectorise les patches de classification (*tile_prod_mask*) et de référence masqués (*tile_test_mask*) - *prod.shp* / *test.shp*

# Création d'une base de données Postgis "artisols_2019" et intégration des couches  
	
	<database name> "artisols_2019"
	CREATE EXTENSION postgis ;

	patch2pgsql.py
		
		<dossier (sur le serveur) où sont les couches à intégrer>
		/home/kenji/WORK/ARTISOLS/OUTPUT_VALID_TILES/MAP_TILES_2019
		  
		<nom de la base de données Postgis>
		artisols_2019
		
Ce script intègre les vecteurs produits *prod.shp* *test.shp*  

# Suppression des entités classées en 255 de toutes les tuiles  

	Delete_255Value_tables.py  
	
		<nom de la base de données Postgis où sont les tables>
		artisols_2019

# Evaluation Objet
	
## 1. Lancement des requêtes SQL pour analyser et évaluer la classification (*tile_prod* par rapport à *tile_test*)

	RequeteSQL_complete_rev3.py
		  
		<dossier où sont les requêtes auxquelles fait appel ce script>
		/home/kenji/WORK/ARTISOLS/SCRIPT/scripts_requete
		  
		<nom de la base de données Postgis où sont les tables>
		artisols_2019
		  
*Vérifier les noms des variables en entrée (noms de tables notamment) qui ont pu être modifiées au cours des essais*
		  
Ce script lance les requêtes suivantes :

  - **"01_Requete_dissolve.sql"**  
Fusion (dissolve) des entités contigues de la table spatiale en entrée (prod et test). Les entités qui se touchent sont fusionnées quelle que soit la classe de bâti 

*xx_prod_fusion, xx_test_fusion*

  - **"02_Requete_IOU.sql"**  
Création d'une nouvelle table qui comporte toutes les intersections 1 à 1 possibles entre *xx_prod_fusion* et *xx_test_fusion*  

*bati_iou*

	Certains IOU sont NULL (là où il n'y a pas d'intersection, ce qui se justifie) mais d'autres ont la valeur 0. Il s'agit d'un noeud entre les deux entités. Penser à remplacer cette valeur par NULL

  - **"03_Requete_fp_tp_fn.sql"**  
Caractérisation des résultats de classification : faux positifs, faux négatifs, vrais positifs  

*update bati_iou*

===============  
 **Dans le cas où une table seuillée sur l'IoU était générée afin de calculer les IoU multiples par la suite et le type d'intersection**
 
  - **"04_Requete_seuil_iou.sql"**  
Suppression des entités peu représentatives en fonction d'un seuil sur l'iou -- n.b. cette opération ne conserve que les intersections valides (vrais positifs)  

*seuil{}%*
 
  - **"05_Requete_multi_iou.sql"**  
Calcul de l'IoU par entité produite en fonction de toutes les entités test intersectées (dans le cas d'une agrégation par exemple)  
Expression : IoU = SOMME(SI) / (SP + SOMME(ST) - SOMME(SI)) avec :  
	- SI : surface intersectée  
	- SP : surface de l'entité produite  
	- ST : surface de l'entité test intersectée 
	
*_multi*
  
===============  
 
  - **"06_Requete_type_intersection.sql"**  
Cette étape était initialement calculée à partir de la table seuillée sur l'IoU. On peut également la calculer à partir de bati_iou (non seuillée). Chose faite dans un second temps.  
Classification des types d'intersection (fonction du nombre d'intersections en *prod* ou *test*) :  
	- agrégation  
	- fragmentation  
	- équivalent  
	- mixte (fragmentation et agrégation)  
Et récupération des colonnes (dans *bati_iou*)  
	- prod_surf  
	- test_surf  
	- prod_geom  
	- test_geom  
	- iou  

*_type_int*

===============  
**Requête sauvegardée mais finalement plus utilisée dans cette étude**
 - **"07_Requete_rm_double.sql"**  
	Gestion des doublons liés à la catégorie mixte. Les entités mixtes agrègent ou fragmentent également. Il y a donc des doublons dans la table.  
	2 cas possibles :  
	- laisser de côté le type de qualification mixte et considérer uniquement l'entité comme agrégeant ou fragmentant.  
			-> suppression des lignes où le type_int = 'mixte' 
	- considérer ces entités comme étant uniquement des entités mixtes (choix qui a été fait ici)  
			-> création d'une table temporaire dans laquelle sont recensées les entités en doublon (même id mais type_int différent, c'est-à-dire agrégation/fragmentation ET mixte)
			-> suppression du doublon, soit la ligne où l'entité est référencée en fragmentation ou agrégation dans la table *type_int* 

*update _type_int*  

===============

 - **"08_Requete_synthese_results.sql"**  
Table de synthèse par patch avec :  
	- nombre et % de faux positifs (fp)
	- nombre et % de faux négatifs (fn)
	- nombre et % de vrais positifs (tp)
	- nombre et % d'agrégations
	- nombre et % de fragmentations
	- nombre et % d'équivalents
	- nombre et % de mixtes
	- nombre d'entités test et prod
	- rappel, précision et f-score  
	n.b. :  
		- le nombre de fp/fn est calculé à partir de la table initiale d'iou (output de la requête 03_Requete_fp_tp_fn.sql) dans laquelle les entités sans intersection sont recensées.  
		- le nombre de tp est calculé à partir de la table *bati_iou*. On peut également mettre un seuil d'IoU dans le script de la requête pour re-qualifier une "bonne intersection"
		- le rappel est calculé par rapport aux entités test (entités test bien détectées sur l'ensemble des entités test) tandis que la précision est calculée en fonction des entités prod (les entités prod qui sont effectivement du bâti par rapport à l'ensemble de ce qui a été détecté.  

*bati_iou_synthese*

### Analyse de la production quelque soit le type d'intersection (équivalent, agrégation, fragmentation)

 - **"09_Requete_synthese_results_fscore_surf.sql"**  
	Table de synthèse par patch avec, par range de surface :  
	- nombre et % de faux positifs
	- nombre et % de faux négatifs
	- nombre et % de vrais positifs
	- rappel, précision et f-score  

*_fscore_surf*

===============  

**Analyses facultatives**  

 - **"10_Requete_urban_density.sql"**    
Ajout d'une colonne de densité urbaine aux tables *bati_iou_synthese*.  
Calcule la densité urbaine par patch à partir de la *tuile test_fusion* (tuile dont les entités contigues ont été fusionnées. cf. *4."01_Requete_dissolve.sql"*) :  
suface batie (m²) / aire (m²) du patch * 100  

*update bati_iou_synthese*
	
 - **"11_Requete_mean_area.sql"**  
Ajout aux tables de synthèse générées précédemment d'une colonne de la moyenne des surfaces des entités par patch.

*update bati_iou_synthese*

================  
	
 - **"12_Requete_intersect_clc.sql"**  
Ajout à la table *bati_iou* d'une colonne afin de savoir si les entités intersectent l'entité unique de tâche urbaine/bâti dense de Corine Land Cover ("Tissu urbain continu" - centre Montpellier Ecusson, "Tissu urbain discontinu" - reste de la ville, "Zones industrielles ou commerciales et installations publiques").  
Si l'entité intersecte, la valeur '1' lui est assignée. Autrement '0'.  
Cette étape va permettre de recalculer les métriques (fscore, rappel, tp, etc.) dans et en dehors de la tâche urbaine afin de voir si la densité du bâti peut impacter les résultats

*update bati_iou*
	
 - **"13_Requete_synthese_results_fscore_surf_clc.sql"**  
Tables de synthèse avec, par range de surface et selon la densité de surface (clc) : 
	- nombre et % de faux positifs
	- nombre et % de faux négatifs
	- nombre et % de vrais positifs
	- rappel, précision et f-score  
	
*_clc*
			
#### Création d'une table de synthèse reprenant l'ensemble des tables de synthèse générées par table/patch (*bati_iou_synthese*, *fscore_surf*, *_clc*)

	RequeteSQL_complete_rev3_synthese.py
	
		<dossier où sont les requêtes auxquelles fait appel ce script>
		/home/kenji/WORK/ARTISOLS/SCRIPT/scripts_requete
		  
		<nom de la base de données Postgis où sont les tables>
		artisols_2019

*synthese_fscore_iou_surf*  
*synthese_fscore_clc*

#### Création d'une table avec l'ensemble des entités test de tous les patches (facultatif. Répondait à un besoin antérieur)

	Requete_select_suf_iou.py
	
		<nom de la base de données Postgis où sont les tables>
		artisols_2019
	
		<nom de la table qui sera créée>
		total_iou_surf
		
		
*Récupération des colonnes "test_surf" et "iou" dans les tables bati_iou. Le nom de la table est également récupéré*
		
#### Création d'une table avec l'ensemble des métriques par range de surface et selon des seuils IoU (par tuile)

Seule la surface des entités test est prise en compte pour les range de surface. Ce qui signifie que s'il y a intersection entre une entité prod (12m²) et une entité test (105m²), l'intersection sera prise en compte dans le range de surface 100-500 (m²)

	RequeteSQL_fscore_iou_surf.py
	
		<dossier où sont les requêtes auxquelles fait appel ce script>
		/home/kenji/WORK/ARTISOLS/SCRIPT/scripts_requete
		  
		<nom de la base de données Postgis où sont les tables>
		artisols_2019
		
Ce script lance la requête suivante : 

 - **"14_Requete_synthese_results_fscore_surf_iou.sql"**  
	Création d'une table par tuile avec,  pour chaque range de surface, des métriques associées à des seuils d'iou (tp, fp, fn, rappel, précision, fscore)  
	n.b :
	- une table vide ne pouvant être créée afin de pouvoir ensuite boucler sur les surfaces et les seuils iou, une colonne 'test' a été générée en même temps. Elle peut être supprimée par la suite  
	- les seuils d'IoU redéfinissent l'intersection. S'il est fixé à 10, on considère que les intersections < 10 (IoU) ne sont pas prises en compte. Il faut donc que les fp et fn varient en même temps et soient enrichis de ce qui est en dessous du seuil.  

*fscore_iou_surf* (par tuile)
	
#### Création d'une table de synthese reprenant l'ensemble des tables générées précédemment par tuile (*fscore_iou_surf*)
	
	RequeteSQL_complete_rev3_synthese.py
	
		<dossier où sont les requêtes auxquelles fait appel ce script>
		/home/kenji/WORK/ARTISOLS/SCRIPT/scripts_requete
		  
		<nom de la base de données Postgis où sont les tables>
		artisols_2019
		
*synthese_fscore_iou_surf*
		
### Analyse des métriques pour les intersections 1 à 1 (équivalent)   
	  
On travaille désormais sur la table des type_int pour avoir les métriques en fonction de la surface et de la densité urbaine. On reprend les mêmes tables /requêtes précédentes mais en filtrant sur les entités dont l'intersection est 1 à 1 ('type_int' = 'equivalent')
		
 - **"09_Requete_synthese_results_fscore_surf_equi.sql"**  
Tables de synthèse par patch avec, par range de surface :   
	- nombre et % de faux positifs  
	- nombre et % de faux négatifs  
	- nombre et % de vrais positifs  
	- rappel, précision et f-score   		

*_fscore_surf_equi*

 - **"12_Requete_intersect_clc.sql"**  
Ajout à la table *type_int* d'une colonne afin de savoir si les entités intersectent l'entité unique de tâche urbaine/bâti dense de Corine Land Cover ("Tissu urbain continu" - centre Montpellier Ecusson, "Tissu urbain discontinu" - reste de la ville, "Zones industrielles ou commerciales et installations publiques").  
Si l'entité intersecte, la valeur '1' lui est assignée. Autrement '0'.  
Cette étape va permettre de recalculer les métriques (fscore, rappel, tp, etc.) dans et en dehors de la tâche urbaine afin de voir si la densité du bâti peut impacter les résultats

*update type_int*  

 - **"13_Requete_synthese_results_fscore_surf_clc_equi.sql"**   
Tables de synthèse par patch avec, par range de surface et selon la densité de surface (clc) :    
	- nombre et % de faux positifs  
	- nombre et % de faux négatifs  
	- nombre et % de vrais positifs  
	- rappel, précision et f-score   

*_clc_equi*  

#### Création d'une table avec l'ensemble des métriques par range de surface et selon des seuils IoU (par tuile)

Seule la surface des entités test est prise en compte pour les range de surface. Ce qui signifie que s'il y a intersection entre une entité prod (12m²) et une entité test (105m²), l'intersection sera prise en compte dans le range de surface 100-500 (m²)

	RequeteSQL_fscore_iou_surf.py

	<dossier où sont les requêtes auxquelles fait appel ce script>
	/home/kenji/WORK/ARTISOLS/SCRIPT/scripts_requete
	  
	<nom de la base de données Postgis où sont les tables>
	artisols_2019
	
Ce script lance la requête suivante : 

 - **"14_Requete_synthese_results_fscore_surf_iou_equi.sql"**  
Création d'une table, par tuile,  avec,  pour chaque range de surface, des métriques associées à des seuils d'iou (tp, fp, fn, rappel, précision, fscore)  
n.b :
	- une table vide ne pouvant être créée afin de pouvoir ensuite boucler sur les surfaces et les seuils iou, une colonne 'test' a été générée en même temps. Elle peut être supprimée par la suite
	
	- les seuils d'IoU redéfinissent l'intersection. S'il est fixé à 10, on considère que les intersections < 10 (IoU) ne sont pas prises en compte. Il faut donc que les fp et fn varient en même temps et soient enrichis de ce qui est en dessous du seuil.
	
*fscore_iou_surf_equi*

#### Création d'une table de synthese reprenant l'ensemble des tables générées précédemment par tuile (*fscore_surf_equi*, *fscore_iou_surf_equi*, *clc_equi*, *bati_iou_synthese*)

	RequeteSQL_complete_rev3_synthese.py
	
		<dossier où sont les requêtes auxquelles fait appel ce script>
		/home/kenji/WORK/ARTISOLS/SCRIPT/scripts_requete
		  
		<nom de la base de données Postgis où sont les tables>
		artisols_2019		

*synthese_fscore_surf_equi*  
*synthese_fscore_iou_surf_equi*  
*synthese_clc_equi*  
*synthese_fscore_patch*  

==> diagram_histo_moy_fscore_surf_iou_lignes 
		
interprétation : lorsqu'il y a intersection, les entités test comprises dans un range de surface précis, ont en moyenne un fscore de X pour un seuil d'IoU de X. Seules les intersections 1 à 1 sont prises en compte dans les tp (prod et test)

## 2. Diagrammes		

- **diagram_lines_surf_iou_itera.py** - *Courbes*  
pourcentage des objets détectés en fonction de leur IoU (quelque soit le type d'intersection)
	- table de départ : *table recensant l'ensemble de toutes les entités de tous les patches avec leur surface et iou. Cf. Création d'une table avec l'ensemble des entités test de tous les patches*

- **diagram_histo_surf_iou.py** - *Barres*  
pourcentage des objets détectés en fonction d'un seuil IoU (quelque soit le type d'intersection)
	- table de départ : *table recensant l'ensemble de toutes les entités de tous les patches avec leur surface et iou. Cf. Création d'une table avec l'ensemble des entités test de tous les patches*
			
- **diagram_lines_hist_surf_iou.py**  - *Barres / Courbes*  
pourcentage des objets détectés en fonction de seuils IoU (quelque soit le type d'intersection)
	- table de départ : *table recensant l'ensemble de toutes les entités de tous les patches avec leur surface et iou. Cf. Création d'une table avec l'ensemble des entités test de tous les patches* 		

- **diagram_lines_iou_bar_noDetect.py** - *Courbes*  
pourcentage des objets non détectés (BARRES) et détectés en fonction de leur IoU
	- table de départ :	*table recensant l'ensemble de toutes les entités de tous les patches avec leur surface et iou. Cf. Création d'une table avec l'ensemble des entités test de tous les patches*		
			
- **diagram_histo_detection_surf.py** - *Barres*  
pourcentage des objets détectés en fonction de leur surface.  
!!! ATTENTION !!! dans le script du diagramme, pas de SELECT DISTINCT des entités. Donc plusieurs entités comptées plusieurs fois. Partir d'une autre table où il n'y a pas de doublons
	- table de départ :	*table recensant l'ensemble de toutes les entités de tous les patches avec leur surface et iou. Cf. Création d'une table avec l'ensemble des entités test de tous les patches*
	
- **diagram_histo_moy_preciz_rappel_surf_clc.py** - *Barres*  
précision et rappel en fonction de la densité urbaine (moyenne, écart-type)
	- table de départ : *synthese_fscore_clc_equi*
			
- **diagram_histo_moy_fscore_surf_clc.py** - *Barres*  
fscore en fonction de la densité urbaine (moyenne, écart-type)
	- table de départ : *synthese_fscore_clc_equi*
			
- **diagram_histo_moy_fscore_surf_training.py** - *Barres*  
moyenne du fscore par patch en fonction de la surface + nombre d'entités par surface utilisées pour le training
	- table de départ : *synthese_fscore_surf_equi* + *table de toutes les entités du  training et leur surface : clip du merge_poly_patches_2019.shp et de BATI_OCCITANIE2018.shp réalisé en amont et ajout d'une colonne de surface. Ne pas oublier de 'dissolve' les entités (01_Requete_dissolve.sql) dans PgAdmin puis exporter la table en .csv*
			
- **diagram_histo_moy_fscore_surf.py** - *Barres*  training
moyenne du fscore par patch en fonction de la surface 
	- table de départ : *synthese_fscore_surf_equi*
			
- **diagram_histo_moy_fscore_surf_iou_bar.py** - *Barres*  
moyenne du fscore en fonction de la surface et d'un seuil IoU (7 diagrammes pour les 7 ranges de surface)
	- table de départ : *synthese_fscore_iou_surf_equi*
			
- **diagram_histo_moy_fscore_surf_iou_lignes.py** - *Courbes*  
moyenne du fscore en fonction de la surface et d'un seuil IoU  
!!! ATTENTION !!! vérifier moyenne fscore (somme fscore des tuiles / nb de tuiles où il y a un fscore)
	- table de départ : *synthese_fscore_iou_surf_equi* 
			
- **boite_moust_fscore.py** - *Boîte à moustaches*  
moyenne fscore (écart-type etc.)
	- table de départ : *synthese_fscore_surf_equi*			
	
# Evaluation Pixel

## 1. Génération d'un fichier .csv avec la matrice de confusion et les métriques (précision, rappel, fscore) pour l'ensemble des patches

	ConfMatrix_tablessynthese.py
	
		<répertoire des matrices de confusiion .csv>  
		/home/kenji/WORK/ARTISOLS/OUTPUT_VALID_TILES/MAP_TILES_2019
		
*matrix_synthese.csv*

===============

**Analyses facultatives**  

## 1. Génération d'un fichier .txt avec la matrice de confusion (mise en forme) et les métriques (précision, rappel, fscore) par patch 

	processConfMatrix.py
	
		<répertoire des matrices de confusiion .csv>  
		/home/kenji/WORK/ARTISOLS/OUTPUT_VALID_TILES/MAP_TILES_2019

===============

## 2. Intégration dans pgAdmin de la table *matrix_synthese.csv*

	CREATE TABLE matrix_pxl (  
	id_tile VARCHAR(50),  
	prod0_ref0 double precision,  
	prod0_ref255 double precision,  
	prod255_ref0 double precision,  
	prod255_ref255 double precision,  
	precision_pxl double precision,  
	rappel_pxl double precision,  
	fscore_pxl double precision,  
	PRIMARY KEY (id_tile));  
  
	COPY matrix_pxl(id_tile,prod0_ref0,prod0_ref255,prod255_ref0,prod255_ref255,precision_pxl,rappel_pxl,fscore_pxl)  
	FROM '/home/kenji/WORK/ARTISOLS/OUTPUT_VALID_TILES/MAP_TILES_2019/matrix_synthese.csv'  
	DELIMITER ','  
	CSV HEADER;  
	
# Spacialisation de l'erreur (fscore/patch)

## Vectorisation des mailles correspondant à l'ensemble des patches 

	extract_patch_extend.py
	
		<carte d'occupation du sol de 2019 (redécoupée Occitanie) - raster>
		/home/kenji/WORK/ARTISOLS/DATA_REMI/cartes/2019/artisols_map_2019_maskOcc.tif
	
		<contours administratifs de la région Occitanie - vecteur>
		/home/kenji/WORK/ARTISOLS/SCRIPTS/Extract_Patch/adm_occitanie/adm_occitanie.shp 
		  
		<nom du fichier vecteur en sortie>
		/home/kenji/WORK/ARTISOLS/OUTPUT_VALID_TILES/MAP_TILES_2019/grid_ref.shp  

Intégration du vecteur grid_ref dans pgAdmin
	
## Jointure entre les tables *synthese_fscore_surf_equi*, *synthese_fscore_patch*, *matrix_synthese* et *grid_ref* puis export dans QGis pour la cartographie  

	CREATE TABLE grid_fscore AS (  
	SELECT 	grid.*, fsc_patch.fscore, fsc_patch.preciz, fsc_patch.rappel, fsc_patch.tp_prod_percent, fsc_patch.urb_density, 
			fsc_surf.fscore_surf1, fsc_surf.fscore_surf2, fsc_surf.fscore_surf3, fsc_surf.fscore_surf4,fsc_surf.fscore_surf5, fsc_surf.fscore_surf6, fsc_surf.fscore_surf7, fsc_surf.fscore_surf8,
			pxl.precision_pxl, pxl.rappel_pxl, pxl.fscore_pxl  
	FROM 	input_data.grid_ref as grid  
	INNER JOIN synthese_fscore_patch as fsc_patch ON grid.id = fsc_patch.id_tile  
	INNER JOIN synthese_fscore_surf_equi as fsc_surf ON fsc_patch.id_tile = fsc_surf.id_tile  
	INNER JOIN matrix_pxl as pxl ON fsc_surf.id_tile = pxl.id_tile AND grid.id = pxl.id_tile)  
	
## Aides 
	- Récupérer une table dans PGAdmin vers la VM : 
	pgsql2shp -f grid_densUrb.shp  -h 10.34.192.163 -p 5432 -u postgres -P admin artisols_2019 input_data.grid_densurb
		avec -f le nom du shape en sortie, -u l'utilisateur, puis database, table