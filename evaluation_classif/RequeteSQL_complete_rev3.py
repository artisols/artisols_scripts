import sys, os, time
import psycopg2

def listTables(dbconnection):
	"""list of both tables prod and test with same tile id
	@param dbconnection <class object> psycopg2 connection instance
	@return list of lists as [tile_id, table_test, table_prod]
	"""
	sqlquer = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'"
	cursor = dbconnection.cursor()
	cursor.execute(sqlquer)
	list_tables = cursor.fetchall()
	list_clean = [i[0] for i in list_tables if 'tile' in i[0] and '_prod' in i[0]]
	
	dual_list = list()
	for table_prod in list_clean :
		table_id = table_prod.split('_prod')[0]
		table_ref = '{}_test'.format(table_id)
		dual_list.append([table_id, table_ref, table_prod])

	return dual_list

def listDone(dbconnection):
	"""list of tables already done
	"""
	sqlquer = "SELECT table_name FROM information_schema.tables WHERE table_name like '%_clc_equi'"
	cursor = dbconnection.cursor()
	cursor.execute(sqlquer)
	list_done = cursor.fetchall()
	
	done_list = list()
	for table in list_done :
		done_list.append(table)

	return done_list
	
def sqlRequest(sql_file):
	"""sql file parsing without comment lines
	@param sql_file <string> sql file path
	"""
	right_lines = list()
	with open(sql_file, 'r') as file:
		for line in file:
			if "--" in line:
				pass
			else: 
				right_lines.append(line)
		output = ' '.join(right_lines).replace('\n', ' ')
	return output

def launchQuery(dbconnection, sql_file, *args):
	"""launch sql query to distant postgres server
	@param dbconnection <class object> psycopg2 connection instance
	@param sqlfile	  <string>	   sql file path
	@param args		 <string>	   input of sql file
	"""
	sqlquer = sqlRequest(sql_file)
	query = sqlquer.format(*args)
	#print(query)
	cursor = dbconnection.cursor()
	
	try:
		cursor.execute(query)
		dbconnection.commit()
		print("requete {} --> succes".format(sql_file))
	except:
		print("requete {} --> echec".format(sql_file))
	
# --- main ---

if __name__ == "__main__":
	# get sql queries directory
	sqlDir = sys.argv[1]
	dbname = sys.argv[2]

	# connect database
	try:
		connection = psycopg2.connect(user = "postgres", password = "admin", host = "10.34.192.163", port = "5432", database = dbname)
	except:
		print('probleme de connexion a la database postgres')

	# list of pairing tables
	list_num = listTables(connection)
	
	# list of processed tables (if some of tables have already been processed it avoids to restart the processing from the beginning)
	list_done = listDone(connection)
	
	for num in list_num:
		start_time = time.time()
		print('--> traitement des tables id : {}'.format(num[0]))
		
		#param table names
		input_prod = num[2]
		input_test = num[1]
		fusion_prod = '{}_prod_fusion'.format(num[0])
		fusion_test = '{}_test_fusion'.format(num[0])
		iou = '{}_bati_iou'.format(num[0])
		seuil = '30'
		iou_seuil = '{}_seuil{}pct'.format(iou, seuil)
		iou_multi = '{}_multi'.format(iou_seuil)
		iou_type_int = '{}_type_int'.format(iou)
		iou_synthese = '{}_synthese'.format(iou)
		iou_fscore_surf = '{}_fscore_surf'.format(iou)
		iou_fscore_surf_equi = '{}_fscore_surf_equi'.format(iou)
		iou_synthese_clc = '{}_clc'.format(iou)
		iou_synthese_clc_equi = '{}_clc_equi'.format(iou)
	

		if iou_synthese_clc_equi not in [i[0] for i in list_done] :
			
			## launching of SQL queries 
			
			launchQuery(connection, 
					  os.path.join(sqlDir, "01_Requete_dissolve.sql"), 
					  input_prod, fusion_prod)
			launchQuery(connection, 
					  os.path.join(sqlDir, "01_Requete_dissolve.sql"), 
					  input_test, fusion_test)
			launchQuery(connection, 
					  os.path.join(sqlDir, "02_Requete_IOU.sql"), 
					  fusion_prod, fusion_test, iou)
			launchQuery(connection, 
					  os.path.join(sqlDir, "03_Requete_fp_tp_fn.sql"), 
					  iou)
			launchQuery(connection, 
					  os.path.join(sqlDir, "06_Requete_type_intersection.sql"), 
					  iou, iou_type_int)
			launchQuery(connection, 
					  os.path.join(sqlDir, "07_Requete_rm_double.sql"), 
					  iou_type_int)
			launchQuery(connection, 
					  os.path.join(sqlDir, "08_Requete_synthese_results.sql"), 
					  iou, iou_type_int, iou_synthese)
			launchQuery(connection, 
					  os.path.join(sqlDir, "10_Requete_urban_density.sql"), fusion_test, iou_synthese)
					  
			### Queries (independently of the type of intersection) ###
			
			# launchQuery(connection, 
					  # os.path.join(sqlDir, "09_Requete_synthese_results_fscore_surf.sql"), 
					  # iou, iou_fscore_surf)		  
			# launchQuery(connection, 
					  # os.path.join(sqlDir, "11_Requete_mean_area.sql"), fusion_test, iou_synthese)
			# launchQuery(connection, 
					  # os.path.join(sqlDir, "12_Requete_intersect_clc.sql"), iou)
			# launchQuery(connection, 
					  # os.path.join(sqlDir, "13_Requete_synthese_results_fscore_surf_clc.sql"), 
					  # iou, iou_synthese_clc)			  
					  
			### Queries (dependently of the type of intersection - 'equivalent') ###
		
			launchQuery(connection, 
					  os.path.join(sqlDir, "09_Requete_synthese_results_fscore_surf_equi.sql"), 
					  iou_type_int, iou_fscore_surf_equi)
			launchQuery(connection, 
					   os.path.join(sqlDir, "12_Requete_intersect_clc.sql"), iou_type_int) 
			launchQuery(connection, 
					  os.path.join(sqlDir, "13_Requete_synthese_results_fscore_surf_clc_equi.sql"), 
					  iou_type_int, iou_synthese_clc_equi)
			
			end_time = time.time()
			print('--> process duration for {}: {} min'.format(num, (end_time-start_time)/60))
		
		else :
			print('OK, DONE', num[0])