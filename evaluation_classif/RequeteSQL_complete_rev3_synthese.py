import sys, os
import psycopg2

def listTables(dbconnection):
    """ List of both tables prod and test with same tile id
    @param dbconnection <class object> psycopg2 connection instance
    @return list of lists as [tile_id, table_name]
    """
    sqlquer = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'"
    cursor = dbconnection.cursor()
    cursor.execute(sqlquer)
    list_tables = cursor.fetchall()
    list_clean = [i[0] for i in list_tables if 'bati_iou_synthese' in i[0]]
    
    dual_list = list()
    for table_synth in list_clean :
        table_id = table_synth.split('_bati')[0]
        dual_list.append([table_id, table_synth])

    return dual_list

def syntheseTab(list_num):
    """ Union of tables in new table named 'synthese_total'
    @param list_num <list> output list from listTables function
    """
    sqlquer = "DROP TABLE IF EXISTS synthese_fscore_patch; CREATE TABLE synthese_fscore_patch AS ("
    for idx in range(len(list_num)-1):
        sqlquer = "{} SELECT '{}' AS id_tile, * FROM {} UNION ALL ".format(sqlquer, list_num[idx][0], list_num[idx][1])
    sqlquer = "{} SELECT '{}' AS id_tile, * FROM {});".format(sqlquer, list_num[-1][0], list_num[-1][1])
    
    cursor = connection.cursor()
    cursor.execute(sqlquer)
    connection.commit()

# --- main ---

if __name__ == "__main__":
    # get sql queries directory
    sqlDir = sys.argv[1]
    dbname = sys.argv[2]

    # connect database
    try:
        connection = psycopg2.connect(user = "postgres", password = "admin", host = "10.34.192.163", port = "5432", database = dbname)
    except:
        print('probleme de connexion a la database postgres')

    # list of tables and tables id
    list_num = listTables(connection)
    # union of tables with adding of tables id in new column
    syntheseTab(list_num)
    print('--> done')

