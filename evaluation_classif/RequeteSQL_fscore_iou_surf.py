import sys, os, time
import psycopg2

def listTables(dbconnection):
	"""list of both tables prod and test with same tile id
	@param dbconnection <class object> psycopg2 connection instance
	@return list of lists as [tile_id, table_test, table_prod]
	"""
	sqlquer = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'"
	cursor = dbconnection.cursor()
	cursor.execute(sqlquer)
	list_tables = cursor.fetchall()
	list_clean = [i[0] for i in list_tables if 'tile' in i[0] and '_prod' in i[0]]
	
	dual_list = list()
	for table_prod in list_clean :
		table_id = table_prod.split('_prod')[0]
		table_ref = '{}_test'.format(table_id)
		dual_list.append([table_id, table_ref, table_prod])

	return dual_list

def sqlRequest(sql_file):
	"""sql file parsing without comment lines
	@param sql_file <string> sql file path
	"""
	right_lines = list()
	with open(sql_file, 'r') as file:
		for line in file:
			if "--" in line:
				pass
			else: 
				right_lines.append(line)
		output = ' '.join(right_lines).replace('\n', ' ')
	return output



def createTable(dbconnection, name_table):
    """ Union of tables in new table named 'synthese_total'
    @param list_num <list> output list from listTables function
    """
    sqlquer = "DROP TABLE IF EXISTS {0}; CREATE TABLE {0} (test double precision); INSERT INTO {0}(test) VALUES(NULL); ".format(name_table)   
    cursor = connection.cursor()
    cursor.execute(sqlquer)
    connection.commit()
	
	
	
def launchQuery(dbconnection, sql_file, *args):
	"""launch sql query to distant postgres server
	@param dbconnection <class object> psycopg2 connection instance
	@param sqlfile	  <string>	   sql file path
	@param args		 <string>	   input of sql file
	"""
	sqlquer = sqlRequest(sql_file)
	query = sqlquer.format(*args)
	cursor = dbconnection.cursor()
	#print('REQUETE :', query)
	try:
		cursor.execute(query)
		dbconnection.commit()
		print("requete {} --> succes".format(sql_file))
	except:
		print("requete {} --> echec".format(sql_file))
	
# --- main ---

if __name__ == "__main__":
	# get sql queries directory
	sqlDir = sys.argv[1]
	dbname = sys.argv[2]

	# connect database
	try:
		connection = psycopg2.connect(user = "postgres", password = "admin", host = "10.34.192.163", port = "5432", database = dbname)
	except:
		print('probleme de connexion a la database postgres')

	# list of pairing tables
	list_num = listTables(connection)
	print(list_num)
	
	for num in list_num:
		start_time = time.time()
		#print('--> traitement des tables id : {}'.format(num[0]))
		#param table names
		iou = '{}_bati_iou'.format(num[0])
		type_int = '{}_bati_iou_type_int'.format(num[0])
		iou_synthese_fscore = '{}_fscore_iou_surf'.format(iou)
		iou_synthese_fscore_equi = '{}_fscore_iou_surf_equi'.format(num[0])

		surface = [2.25, 10, 100, 200, 500, 1000, 50000, 100000, 125000]
		th = [0,10,30,50,70]	
		# createTable(connection, iou_synthese_fscore)	# on crée une table avec une colonne et une ligne fictive (une table ne pouvant être créée vide) Cette colonne 'test' peut-être supprimée après
		createTable(connection, iou_synthese_fscore_equi)	# on crée une table avec une colonne et une ligne fictive (une table ne pouvant être créée vide) Cette colonne 'test' peut-être supprimée après
		
		for s in range(len(surface)) :
			#print(s)
			seuil_surf1 = surface[s]
			if seuil_surf1 == 125000 :
				break
			else :
				seuil_surf2 = surface[s+1] 
				#print('seuil_surf :',  seuil_surf1, '-', seuil_surf2)
				for iou_seuil in th :
					iou_index = th.index(iou_seuil) 
					#print('index iou_seuil :', iou_index)
					#print('iou_seuil :', iou_seuil)

				#launching of SQL queries (independently of the type of intersection)
					# launchQuery(connection, 
								# os.path.join(sqlDir, "14_Requete_synthese_results_fscore_surf_iou.sql"), 
								# iou, iou_synthese_fscore, iou_seuil, seuil_surf1, seuil_surf2, str(iou_index), str(s))	

				#launching of SQL queries (dependently of the type of intersection - 'equivalent')
					launchQuery(connection, 
								os.path.join(sqlDir, "14_Requete_synthese_results_fscore_surf_iou_equi.sql"), 
								type_int, iou_synthese_fscore_equi, iou_seuil, seuil_surf1, seuil_surf2, str(iou_index), str(s))									
					end_time = time.time()
					#print('--> process duration for {}: {} min'.format(num, (end_time-start_time)/60))