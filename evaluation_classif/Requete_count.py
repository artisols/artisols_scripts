# Compte le nombre de ligne pour l'ensemble dse tuiles/patches


import sys, os, time
import psycopg2


def listTables(dbconnection):
    """list of tables named "_bati_iou"
    @param dbconnection <class object> psycopg2 connection instance
    @return list of lists as [tile]
    """
    sqlquer = "SELECT table_name FROM information_schema.tables WHERE table_name like '%_bati_iou'"
    cursor = dbconnection.cursor()
    cursor.execute(sqlquer)
    list_tables = cursor.fetchall()
    
    dual_list = list()
    for table in list_tables :
        dual_list.append(table)

    return dual_list


def count(dbconnection, table):
    """count number of rows for each table
    """
    sqlquer = "SELECT COUNT ({0}.*) FROM {0}".format(table)
    cursor = dbconnection.cursor()
    cursor.execute(sqlquer)
    list_count = cursor.fetchall()	
	
    count_list = list()
    for nb in list_count :
        count_list.append(nb)
		
    return count_list
	
if __name__ == "__main__":
    # get sql queries directory
    dbname = sys.argv[1]

    # connect database
    try:
        connection = psycopg2.connect(user = "postgres", password = "admin", host = "10.34.192.163", port = "5432", database = dbname)
    except:
        print('probleme de connexion a la database postgres')

    # list of pairing tables
    list_num = listTables(connection)

    for tile in list_num:
        tile_test = tile[0]
        count2 = count(connection,tile_test)
        for nb in count2 :
            nb_tile = nb[0]
            print('tile_test : ', tile_test, 'count: ', nb_tile)


