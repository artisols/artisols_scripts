import sys, os, time
import psycopg2

def listTables(dbconnection):
    """list of both tables prod and test with same tile id
    @param dbconnection <class object> psycopg2 connection instance
    @return list of lists as [tile_id, table_test, table_prod]
    """
    sqlquer = "SELECT table_name FROM information_schema.tables WHERE table_name like '%_bati_iou_type_int'"
    cursor = dbconnection.cursor()
    cursor.execute(sqlquer)
    list_tables = cursor.fetchall()
    
    dual_list = list()
    for table in list_tables :
        dual_list.append(table)

    return dual_list

def createTable(name_table):
    """ New table wherethe entities from all the test patches will be identify  
    """
    sqlquer = "DROP TABLE IF EXISTS {0}; CREATE TABLE {0} (table_name varchar(50), test_surf double precision, iou double precision);".format(name_table)
        
    cursor = connection.cursor()
    cursor.execute(sqlquer)
    connection.commit()

def insertValues(name_table, tile):
    """ Insert in the new table surface and iou columns from the test patchs
    """
    sqlquer = "INSERT INTO {0} (table_name, test_surf, iou) (SELECT '{1}', tile.test_surf, tile.iou FROM {1} as tile WHERE type_int = 'equivalent');".format(name_table, tile)
        
    cursor = connection.cursor()
    cursor.execute(sqlquer)
    connection.commit()

def iou_null(name_table):
    """ Replace NULL with 0 in the column "iou" in the output table
    """
    sqlquer = "UPDATE {0} SET iou=0 WHERE iou IS NULL;".format(name_table)
        
    cursor = connection.cursor()
    cursor.execute(sqlquer)
    connection.commit()

if __name__ == "__main__":
    # get sql queries directory
    dbname = sys.argv[1]
    tablename = sys.argv[2]

    # connect database
    try:
        connection = psycopg2.connect(user = "postgres", password = "admin", host = "10.34.192.163", port = "5432", database = dbname)
    except:
        print('probleme de connexion a la database postgres')

    # list of pairing tables
    list_num = listTables(connection)
    print(list_num)
    # createTable(tablename)

    for tile in list_num:
        tile_test = tile[0]
        print('tile :', tile_test)
        # print('tablename: ', tablename)
        # insertValues(tablename, tile_test)
    
    # iou_null(tablename)
