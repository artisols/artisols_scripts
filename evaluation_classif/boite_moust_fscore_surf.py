import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

artiSol_df = pd.read_csv("synthese_fscore_surf_equi_2019.csv",sep=";")

print(artiSol_df)

# Sélection des colonnes à comparer : la surface des entités test et leur IoU

fscore_surf1 = artiSol_df.loc[:,'fscore_surf1']
fscore_surf1 = fscore_surf1.to_numpy()
fscore_surf1 = np.expand_dims(fscore_surf1,-1)

fscore_surf2 = artiSol_df.loc[:,'fscore_surf2']
fscore_surf2 = fscore_surf2.to_numpy()
fscore_surf2 = np.expand_dims(fscore_surf2,-1)

fscore_surf3 = artiSol_df.loc[:,'fscore_surf3']
fscore_surf3 = fscore_surf3.to_numpy()
fscore_surf3 = np.expand_dims(fscore_surf3,-1)

fscore_surf4 = artiSol_df.loc[:,'fscore_surf4']
fscore_surf4 = fscore_surf4.to_numpy()
fscore_surf4 = np.expand_dims(fscore_surf4,-1)

fscore_surf5 = artiSol_df.loc[:,'fscore_surf5']
fscore_surf5 = fscore_surf5.to_numpy()
fscore_surf5 = np.expand_dims(fscore_surf5,-1)

fscore_surf6 = artiSol_df.loc[:,'fscore_surf6']
fscore_surf6 = fscore_surf6.to_numpy()
fscore_surf6 = np.expand_dims(fscore_surf6,-1)

fscore_surf7 = artiSol_df.loc[:,'fscore_surf7']
fscore_surf7 = fscore_surf7.to_numpy()
fscore_surf7 = np.expand_dims(fscore_surf7,-1)

fscore_surf8 = artiSol_df.loc[:,'fscore_surf8']
fscore_surf8 = fscore_surf8.to_numpy()
fscore_surf8 = np.expand_dims(fscore_surf8,-1)

# Création d'une nouvelle table avec ces deux seules colonnes
table = np.concatenate([fscore_surf1,fscore_surf2, fscore_surf3, fscore_surf4, fscore_surf5,fscore_surf6, fscore_surf7, fscore_surf8],axis=1)

#Création d'une nouvelle table sans les NaN
f_score_vecs = []
for i in range(table.shape[1]):
	temp = table[:,i]
	print(len(temp))
	temp = temp[~np.isnan(temp)]
	print(len(temp))
	print("======")
	f_score_vecs.append( temp)

# Diagramme
genRange = [2.25, 10, 100, 200, 500, 1000, 50000, 100000, 125000]

xlabel=[]
for i in range(len(genRange)-1):
    begin_i = genRange[i]	
    end_i = genRange[i+1]
    label = str(begin_i)+" - "+str(end_i)
    xlabel.append(label)

x1 = range(len(xlabel)) 

plt.boxplot(f_score_vecs, positions = x1, sym = 'none')
plt.xticks([r for r in range(len(xlabel))], xlabel, rotation = 0, fontsize = 10)

plt.xlabel('Surface', fontsize = 12)
plt.ylabel('Moyenne du fscore', fontsize = 12)
plt.title ("Boîte à moustache de la moyenne du Fscore des patches en fonction de la surface des entités (m²)", fontsize = 14)

plt.show()