# Calcule les métriques (standard dévation, mean, etc.) pour une colonne d'une table donnée

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

artiSol_df = pd.read_csv("synthese_fscore_pixel_2019.csv",sep=";")


fscore_pxl = artiSol_df.loc[:,'fscore_pxl']
fscore_pxl = fscore_pxl.to_numpy()
fscore_pxl = np.expand_dims(fscore_pxl,-1)

std = np.nanstd(fscore_pxl, axis = 0)
print( 'STD :', std)

median = np.nanmedian(fscore_pxl, axis = 0)
print( 'median :', median)

mins = np.nanmin(fscore_pxl, axis = 0)
print('MINS: ', mins)

max = np.nanmax(fscore_pxl, axis = 0)
print('MAX: ', max)

mean = np.nanmean(fscore_pxl, axis = 0)
print('MEAN :', mean)

# artiSol_df = pd.read_csv("synthese_fscore_object_2019.csv",sep=";")

# fscore = artiSol_df.loc[:,'fscore']
# fscore = fscore.to_numpy()
# fscore = np.expand_dims(fscore,-1)

# std = np.nanstd(fscore, axis = 0)
# print( 'STD :', std)

# median = np.nanmedian(fscore, axis = 0)
# print( 'median :', median)

# mins = np.nanmin(fscore, axis = 0)
# print('MINS: ', mins)

# max = np.nanmax(fscore, axis = 0)
# print('MAX: ', max)

# mean = np.nanmean(fscore, axis = 0)
# print('MEAN :', mean)

