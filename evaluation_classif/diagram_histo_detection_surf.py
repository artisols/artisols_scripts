import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

artiSol_df = pd.read_csv("table_surf_iou_2019.csv",sep=";")

print(artiSol_df)

# Sélection des colonnes à comparer : la surface des entités test et leur IoU
test_surf = artiSol_df.loc[:,'test_surf']
test_surf = test_surf.to_numpy()
test_surf = np.expand_dims(test_surf,-1)

iou = artiSol_df.loc[:,'iou']
iou = iou.to_numpy()
iou = np.expand_dims(iou,-1)

# Création d'une nouvelle table avec ces deux seules colonnes
table = np.concatenate([test_surf,iou],axis=1)

# Valeurs qui serviront d'intervalles de surface
genRange = [2.25, 10, 100, 200, 500, 1000, 50000, 100000, 125000]

# Association d'un nom à chaque colonne
surf_column = table[:,0]
iou_column = table[:,1]

# Seuil de l'IoU permettant d'identifier les entités détectée
th = 0

# Condition à remplir : True = Entités ayant un IoU > 0 et donc détectées.
thresholded_iou_column = (iou_column > th).astype("int")
#print(thresholded_iou_column)

accumul_detect = []
accumul_notDetect = []

for i in range(len(genRange)-1):
	# crée les intervalles de surface à partir des valeurs indiquées précédemment
    begin_i = genRange[i]
    end_i = genRange[i+1]
    idx1 = np.where(surf_column >= begin_i)
    idx2 = np.where(surf_column <= end_i)

	# récupération des id des entités comprises dans l'intervalle
    intersection = np.intersect1d(idx1[0], idx2[0])  

  # Vérification de la condition : 1 (entités avec IoU >0) 0 (entités avec IoU <0)
    temp_iou = thresholded_iou_column[intersection]  
    print(temp_iou)

  # Comptage de 0 (première colonne) et 1 (deuxième colonne)
    count_per_detection = np.bincount(temp_iou)
    count_per_detection = count_per_detection.astype("float")
    #print('count_per_detection : ', count_per_detection)

  # Pourcentage de 0 et 1
    count_per_detection = (count_per_detection / np.sum(count_per_detection) ) * 100
    print("bincount % ",count_per_detection)
    print("bincount ",np.bincount(genRange))

  # Remplissage des listes (précédemment créées) avec les valeurs de pourcentages
    accumul_detect.append(count_per_detection[1])
    accumul_notDetect.append(count_per_detection[0])
    print("begin_i %f end_i %f with number of samples %d" %(begin_i, end_i, len(intersection) ))
    print('accumul_detect', accumul_detect)
    print("===================")
	
# Création des labels pour l'axe des abscisses (seuils de surface)
xlabel=[]
for i in range(len(genRange)-1):
    begin_i = genRange[i]	
    end_i = genRange[i+1]
    label = str(begin_i)+" - "+str(end_i)
    xlabel.append(label)
    #print(xlabel)

# Paramétrage des barres
width = 0.35   # largeur des barres
x1 = range(len(accumul_detect))  #position des barres d'entités détectées
x2 = [i + width for i in x1]	#position des barres d'entités non détectées 

# Génération des barres
plt.bar(x1, accumul_detect, width, color = 'orange') #barres d'entités détectées
plt.bar(x2, accumul_notDetect, width, color = (0.1, 0.1, 0.1, 0.4)) # barres d'entités non détectées  # color : couleurs R,G,B,Transparancy

# Paramétrage des graduations : plt.xticks([position des labels en abscisses], labels (intervalles), orientation labels, taille police)
plt.xticks([r + width/2 for r in range(len(accumul_detect))], xlabel, rotation = 50, fontsize = 8)

# Titres du diagramme et des axes
plt.xlabel('Surface')
plt.ylabel('Pourcentage')
plt.title ("Pourcentage des objets détectés en fonction de leur surface (m²)", fontsize = 10)

# Légende associée au diagramme
legende = ['objets détectés', 'objets non détectés']
plt.legend(legende, bbox_to_anchor=(1, 1), fontsize= 'small')  # (legende, position en dehors de l'histogramme, taille police)

plt.show()