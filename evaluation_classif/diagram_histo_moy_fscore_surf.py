import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

artiSol_df = pd.read_csv("synthese_fscore_surf_equi_2019.csv",sep=";")

print(artiSol_df)

# Sélection des colonnes à comparer : la surface des entités test et leur IoU

fscore_surf1 = artiSol_df.loc[:,'fscore_surf1']
fscore_surf1 = fscore_surf1.to_numpy()
fscore_surf1 = np.expand_dims(fscore_surf1,-1)

fscore_surf2 = artiSol_df.loc[:,'fscore_surf2']
fscore_surf2 = fscore_surf2.to_numpy()
fscore_surf2 = np.expand_dims(fscore_surf2,-1)

fscore_surf3 = artiSol_df.loc[:,'fscore_surf3']
fscore_surf3 = fscore_surf3.to_numpy()
fscore_surf3 = np.expand_dims(fscore_surf3,-1)

fscore_surf4 = artiSol_df.loc[:,'fscore_surf4']
fscore_surf4 = fscore_surf4.to_numpy()
fscore_surf4 = np.expand_dims(fscore_surf4,-1)

fscore_surf5 = artiSol_df.loc[:,'fscore_surf5']
fscore_surf5 = fscore_surf5.to_numpy()
fscore_surf5 = np.expand_dims(fscore_surf5,-1)

fscore_surf6 = artiSol_df.loc[:,'fscore_surf6']
fscore_surf6 = fscore_surf6.to_numpy()
fscore_surf6 = np.expand_dims(fscore_surf6,-1)

fscore_surf7 = artiSol_df.loc[:,'fscore_surf7']
fscore_surf7 = fscore_surf7.to_numpy()
fscore_surf7 = np.expand_dims(fscore_surf7,-1)

fscore_surf8 = artiSol_df.loc[:,'fscore_surf8']
fscore_surf8 = fscore_surf8.to_numpy()
fscore_surf8 = np.expand_dims(fscore_surf8,-1)

# Création d'une nouvelle table avec ces deux seules colonnes
table = np.concatenate([fscore_surf1,fscore_surf2, fscore_surf3, fscore_surf4, fscore_surf5,fscore_surf6, fscore_surf7, fscore_surf8],axis=1)

# Association d'un nom à chaque colonne
fscore_surf1 = table[:,0]
fscore_surf2 = table[:,1]
fscore_surf3 = table[:,2]
fscore_surf4 = table[:,3]
fscore_surf5 = table[:,4]
fscore_surf6 = table[:,5]
fscore_surf7 = table[:,6]
fscore_surf8 = table[:,7]

std = np.nanstd(table, axis = 0)
print( 'STD :', std)

median = np.nanmedian(table, axis = 0)
print( 'median :', median)

mins = np.nanmin(table, axis = 0)
print('MINS: ', mins)

max = np.nanmax(table, axis = 0)
print('MAX: ', max)

mean = np.nanmean(table, axis = 0)
print('mean :', mean)

#### ----- solution 1 ----------------- ####

# mean_list = [m for m in mean]
# error_std = [s for s in std]
# mins_list = [n for n in mins]
# max_list = [p for p in max]
# print('MEAN_LIST: ', mean_list)


# # Création des labels en abscisses 
# genRange = [2.25, 10, 100, 200, 500, 1000, 50000, 100000, 125000]

# xlabel=[]
# for i in range(len(genRange)-1):
    # begin_i = genRange[i]	
    # end_i = genRange[i+1]
    # label = str(begin_i)+" - "+str(end_i)
    # xlabel.append(label)

# # affichage de la moyenne (en barre) et de l'écart-type (en trait)
# x1 = range(len(mean)) 
# bar = plt.bar(x1, mean_list, yerr = error_std, width = 0.35, align = 'center', alpha = 0.5, color = 'orange', ecolor = 'grey', capsize=2) 

# # Paramétrage des graduations : plt.xticks([position des labels en abscisses], labels (intervalles), orientation labels, taille police)
# plt.xticks([r for r in range(len(mean))], xlabel, rotation = 50, fontsize = 8)

# #Titres du diagramme et des axes
# plt.xlabel('Surface')
# plt.ylabel('Moyenne du fscore')
# plt.title ("Moyenne du Fscore des patches en fonction de leur surface (m²)", fontsize = 10)

# plt.legend([bar, error_std], ('moyenne du fscore', 'qsfd'))  # (legende, position en dehors de l'histogramme, taille police)

# plt.show()


#### ----- solution 2 ----------------- ####

# Création des labels en abscisses 
genRange = [2.25, 10, 100, 200, 500, 1000, 50000, 100000, 125000]

xlabel=[]
for i in range(len(genRange)-1):
    begin_i = genRange[i]	
    end_i = genRange[i+1]
    label = str(begin_i)+" - "+str(end_i)
    xlabel.append(label)

x1 = range(len(mean)) 
bar = plt.bar(x1, mean, width = 0.25, color = (1, 0.5, 0.1, 0.4)) #barres d'entités détectées
#min_max_plt = plt.errorbar(np.arange(8), mean, [mean - mins, max - mean], fmt='none', ecolor=(1, 0.3, 0, 1), lw=1, capsize=3)
#std_plt = plt.errorbar(np.arange(8), mean, std, fmt='none', ecolor='black', lw=1, capsize=2)

# Paramétrage des graduations : plt.xticks([position des labels en abscisses], labels (intervalles), orientation labels, taille police)
plt.xticks([r for r in range(len(mean))], xlabel, rotation = 50, fontsize = 10)

#Titres du diagramme et des axes
plt.xlabel('Area (m²)')
plt.ylabel('Average F-score per tile')
#plt.title ("Moyenne du Fscore des patches en fonction de leur surface (m²)", fontsize = 14)

#plt.legend((bar, min_max_plt, std_plt), ('Moyenne fscore', 'Valeurs min. et max.', 'Ecart-type'), bbox_to_anchor=(1, 1), fontsize= 'small')  # (legende, position en dehors de l'histogramme, taille police)

plt.show()