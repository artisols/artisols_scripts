import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

artiSol_df = pd.read_csv("synthese_fscore_clc_equi_2019.csv",sep=";")

# Sélection des colonnes à comparer : la surface des entités test et leur IoU

fscore_surf1_tache = artiSol_df.loc[:,'fscore_surf1_tache']
fscore_surf1_tache = fscore_surf1_tache.to_numpy()
fscore_surf1_tache = np.expand_dims(fscore_surf1_tache,-1)

fscore_surf2_tache = artiSol_df.loc[:,'fscore_surf2_tache']
fscore_surf2_tache = fscore_surf2_tache.to_numpy()
fscore_surf2_tache = np.expand_dims(fscore_surf2_tache,-1)

fscore_surf3_tache = artiSol_df.loc[:,'fscore_surf3_tache']
fscore_surf3_tache = fscore_surf3_tache.to_numpy()
fscore_surf3_tache = np.expand_dims(fscore_surf3_tache,-1)

fscore_surf4_tache = artiSol_df.loc[:,'fscore_surf4_tache']
fscore_surf4_tache = fscore_surf4_tache.to_numpy()
fscore_surf4_tache = np.expand_dims(fscore_surf4_tache,-1)

fscore_surf5_tache = artiSol_df.loc[:,'fscore_surf5_tache']
fscore_surf5_tache = fscore_surf5_tache.to_numpy()
fscore_surf5_tache = np.expand_dims(fscore_surf5_tache,-1)

fscore_surf6_tache = artiSol_df.loc[:,'fscore_surf6_tache']
fscore_surf6_tache = fscore_surf6_tache.to_numpy()
fscore_surf6_tache = np.expand_dims(fscore_surf6_tache,-1)

fscore_surf7_tache = artiSol_df.loc[:,'fscore_surf7_tache']
fscore_surf7_tache = fscore_surf7_tache.to_numpy()
fscore_surf7_tache = np.expand_dims(fscore_surf7_tache,-1)

fscore_surf8_tache = artiSol_df.loc[:,'fscore_surf8_tache']
fscore_surf8_tache = fscore_surf8_tache.to_numpy()
fscore_surf8_tache = np.expand_dims(fscore_surf8_tache,-1)

fscore_surf1_out = artiSol_df.loc[:,'fscore_surf1_tache_out']
fscore_surf1_out = fscore_surf1_out.to_numpy()
fscore_surf1_out = np.expand_dims(fscore_surf1_out,-1)

fscore_surf2_out = artiSol_df.loc[:,'fscore_surf2_tache_out']
fscore_surf2_out = fscore_surf2_out.to_numpy()
fscore_surf2_out = np.expand_dims(fscore_surf2_out,-1)

fscore_surf3_out = artiSol_df.loc[:,'fscore_surf3_tache_out']
fscore_surf3_out = fscore_surf3_out.to_numpy()
fscore_surf3_out = np.expand_dims(fscore_surf3_out,-1)

fscore_surf4_out = artiSol_df.loc[:,'fscore_surf4_tache_out']
fscore_surf4_out = fscore_surf4_out.to_numpy()
fscore_surf4_out = np.expand_dims(fscore_surf4_out,-1)

fscore_surf5_out = artiSol_df.loc[:,'fscore_surf5_tache_out']
fscore_surf5_out = fscore_surf5_out.to_numpy()
fscore_surf5_out = np.expand_dims(fscore_surf5_out,-1)

fscore_surf6_out = artiSol_df.loc[:,'fscore_surf6_tache_out']
fscore_surf6_out = fscore_surf6_out.to_numpy()
fscore_surf6_out = np.expand_dims(fscore_surf6_out,-1)

fscore_surf7_out = artiSol_df.loc[:,'fscore_surf7_tache_out']
fscore_surf7_out = fscore_surf7_out.to_numpy()
fscore_surf7_out = np.expand_dims(fscore_surf7_out,-1)

fscore_surf8_out = artiSol_df.loc[:,'fscore_surf8_tache_out']
fscore_surf8_out = fscore_surf8_out.to_numpy()
fscore_surf8_out = np.expand_dims(fscore_surf8_out,-1)

# Création d'une nouvelle table avec ces deux seules colonnes
table_tache = np.concatenate([fscore_surf1_tache,fscore_surf2_tache, fscore_surf3_tache, fscore_surf4_tache, fscore_surf5_tache,fscore_surf6_tache, fscore_surf7_tache, fscore_surf8_tache], axis =1) 
table_out = np.concatenate([fscore_surf1_out,fscore_surf2_out, fscore_surf3_out, fscore_surf4_out, fscore_surf5_out,fscore_surf6_out, fscore_surf7_out, fscore_surf8_out],axis=1)

print('TABLE_TCHE :', table_tache)
print('TABLE_OUT :', table_out)
# Association d'un nom à chaque colonne
fscore_surf1_tache = table_tache[:,0]
fscore_surf2_tache = table_tache[:,1]
fscore_surf3_tache = table_tache[:,2]
fscore_surf4_tache = table_tache[:,3]
fscore_surf5_tache = table_tache[:,4]
fscore_surf6_tache = table_tache[:,5]
fscore_surf7_tache = table_tache[:,6]
fscore_surf8_tache = table_tache[:,7]

fscore_surf1_out = table_out[:,0]
fscore_surf2_out = table_out[:,1]
fscore_surf3_out = table_out[:,2]
fscore_surf4_out = table_out[:,3]
fscore_surf5_out = table_out[:,4]
fscore_surf6_out = table_out[:,5]
fscore_surf7_out = table_out[:,6]
fscore_surf8_out = table_out[:,7]

std_tache = np.nanstd(table_tache, axis = 0)
print( 'std_tache :', std_tache)

mins_tache = np.nanmin(table_tache, axis = 0)
print('mins_tache: ', mins_tache)

max_tache = np.nanmax(table_tache, axis = 0)
print('max_tache: ', max_tache)

mean_tache = np.nanmean(table_tache, axis = 0)
print('mean_tache: ', mean_tache)

std_out = np.nanstd(table_out, axis = 0)
print( 'std_out :', std_out)

mins_out = np.nanmin(table_out, axis = 0)
print('mins_out: ', mins_out)

max_out = np.nanmax(table_out, axis = 0)
print('max_out: ', max_out)

mean_out = np.nanmean(table_out, axis = 0)
print('mean_out: ', mean_out)

#### ----- solution 2 ----------------- ####

# Création des labels en abscisses 
genRange = [2.25, 10, 100, 200, 500, 1000, 50000, 100000, 125000]

xlabel=[]
for i in range(len(genRange)-1):
    begin_i = genRange[i]	
    end_i = genRange[i+1]
    label = str(begin_i)+" - "+str(end_i)
    xlabel.append(label)

width = 0.25
x1 = range(len(mean_tache)) #position des barres d'entités détectées
x2 = [i + width for i in x1]

# min_tache = plt.scatter(x1, mins_tache, s = 10,  color = 'green')
# max_tache = plt.scatter(x1, max_tache, s = 10,  color = 'red')
# min_out = plt.scatter(x2, mins_out, s = 10,  color = 'green')
# max_out = plt.scatter(x2, max_out, s = 10,  color = 'red')

bar1 = plt.bar(x1, mean_tache, width, color = (1, 0.5, 0.1, 0.8)) #barres d'entités détectées
bar2 = plt.bar(x2, mean_out, width, color = (1, 0.5, 0.1, 0.4))

# min_max_tache = plt.errorbar(x1, mean_tache, [mean_tache - mins_tache, max_tache - mean_tache], fmt='none', ecolor=(1, 0.3, 0, 1), lw=1, capsize=3)
# min_max_out = plt.errorbar(x2, mean_out, [mean_out - mins_out, max_out - mean_out], fmt='none', ecolor=(1, 0.3, 0, 1), lw=1, capsize=3)


# std_plt_tache = plt.errorbar(x1, mean_tache, std_tache, fmt='none', ecolor='black', lw=1, capsize=2)
# std_plt_out = plt.errorbar(x2, mean_out, std_out, fmt='none', ecolor='black', lw=1, capsize=2)

# Paramétrage des graduations : plt.xticks([position des labels en abscisses], labels (intervalles), orientation labels, taille police)
plt.xticks([r + width/2 for r in range(len(mean_tache))], xlabel, rotation = 50, fontsize = 10)

#Titres du diagramme et des axes
plt.xlabel('Area (m²)')
plt.ylabel('Average F-score')
#plt.title ("Moyenne du Fscore des patches en fonction de la surface des entités (m²) et de la densité urbaine", fontsize = 14)

plt.legend((bar1, bar2), ('Urban area', 'Rural area'), loc = 'upper left', fontsize= 'small')  # (legende, position en dehors de l'histogramme, taille police)

plt.show()