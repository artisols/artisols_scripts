import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

artiSol_df = pd.read_csv("synthese_fscore_iou_surf_equi.csv",sep=";")

surfaces = ['surf0', 'surf1', 'surf2', 'surf3', 'surf4', 'surf5', 'surf6', 'surf7']

tables_surfs = []

for surf in surfaces :
	list_colonne = []
	for x in artiSol_df :	
		#list_colonne = 'list_colonne'+str(j)	
		if surf in x :		
			list_colonne.append(x)
	tables_surfs.append(list_colonne)
	#print(tables_surfs)
tables_total_surf = []
for table in tables_surfs :
	#print('table', table)
	list_colonne_surf = []
	for name in table :
		name = artiSol_df.loc[:,str(name)]
		name = name.to_numpy()
		name = np.expand_dims(name,-1)
		list_colonne_surf.append(name)
	table_def = np.concatenate(list_colonne_surf,axis=1)
	tables_total_surf.append(table_def)

tables_iou = []	
ranges = ['2.25 - 10', '10 - 100', '100 - 200', '200 - 500', '500 - 1000', '1000 - 50000', '50000 - 100000', '100000 - 125000']

for t in tables_total_surf : 
	for r in ranges : 
		#print('table : \n', t)	# tables de chaque surface (avec métriques pour chaque seuil iou)
		mean = np.nanmean(t, axis = 0)	# moyenne pour chaque colonne de chaque table (fscore, rappel, tp, etc.)
		mean_iou = mean[[6,13,20,27]]	# on récupère uniquement les colonnes des 
		print('mean: ', mean)
		print('mean_iou: ', mean_iou)
		

		# #### ----- Diagramme ----------------- ####
		# Création des labels en abscisses 
		genRange = ['iou 0.1', 'iou 0.3', 'iou 0.5', 'iou 0.7']

		width = 0.15
		x1 = range(len(mean_iou)) #position des barres d'entités détectées
		x2 = [i + width/2 for i in x1]

		bar1 = plt.bar(x2, mean_iou, width, color = (1, 0.5, 0.1, 0.7)) #barres d'entités détectées

		# Paramétrage des graduations : plt.xticks([position des labels en abscisses], labels (intervalles), orientation labels, taille police)
		plt.xticks([r + width/2 for r in range(len(mean_iou))], genRange, fontsize = 8)

		#Titres du diagramme et des axes
		plt.xlabel('Seuils IoU')
		plt.ylabel('Moyenne du fscore')
		plt.title ("Moyenne du Fscore des patches en fonction de la surface des entités (m²) et de seuils IoU", fontsize = 10)
		plt.legend(bar1, [r],  bbox_to_anchor=(1, 1), loc='upper left', fancybox=True, fontsize= 'small')  
		plt.show()
