import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

artiSol_df = pd.read_csv("synthese_fscore_iou_surf_equi_2019.csv",sep=";")

print(artiSol_df)

list_colonne_surf = []	
tables_total_surf = []
# Sélection des colonnes de fscore (par surface et iou) et création d'une nouvelle table/matrice tables_total_surf
for table in artiSol_df :
	if 'fscore' in table :	
		column = artiSol_df.loc[:,str(table)]
		column = column.to_numpy()
		column = np.expand_dims(column,-1)
		list_colonne_surf.append(column)
table_def = np.concatenate(list_colonne_surf,axis=1)
tables_total_surf.append(table_def)
print('tables_total_surf : \n', tables_total_surf)

for t in tables_total_surf : 
	mean = np.nanmean(t, axis = 0)	# moyenne pour chaque colonne de chaque table (fscore, rappel, tp, etc.) sur une ligne
	print('mean: ', mean)
	mean_array = np.reshape(mean, (-1, 5)) # matrice ré-ordonnée : colonnes = seuils IoU, lignes = surfaces
	print('mean_array : \n', mean_array)


#DIAGRAMME
# Création des labels pour l'axe des abscisses (seuils d'iou)
genRange = ['iou > 0', 'iou > 10', 'iou > 30', 'iou > 50', 'iou > 70']

m = 0
lines = []
legendes_total = []   
for row in mean_array :
    colors = ['red', 'yellow', 'pink', 'blue', 'green', 'orange', 'purple', 'black']  
# Génération des courbes en fonction du seuil IoU
    line,= plt.plot(row, color = colors[m])
    m = m+1
    lines.append(line)

# Paramétrage des graduations : plt.xticks([position des labels en abscisses], labels (intervalles), orientation labels, taille police)
plt.xticks([r for r in range(len(genRange))], genRange, fontsize = 8)

# Titres du diagramme et des axes
plt.xlabel('IoU thresholds')
plt.ylabel('Average F-score')
#plt.title ("Moyenne du Fscore en fonction de seuils IoU et de la surface des entités", fontsize = 14)
    
# Légende associée au diagramme
seuils = ['2.25-10', '10-100', '100-200', '200-500', '500-1000', '1000-50000', '50000-100000', '100000-125000']
plt.legend((courbe for courbe in lines), (leg for leg in seuils),  bbox_to_anchor=(1, 1), loc='upper left', title = 'Surface area (m²)', fancybox=True, fontsize= 9)  

plt.show()
