import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

##########   FSCORE  ##########

artiSol_df = pd.read_csv("synthese_fscore_surf_equi_2019.csv",sep=";")
#Sélection des colonnes à comparer : la surface des entités test et leur IoU

fscore_surf1 = artiSol_df.loc[:,'fscore_surf1']
fscore_surf1 = fscore_surf1.to_numpy()
fscore_surf1 = np.expand_dims(fscore_surf1,-1)

fscore_surf2 = artiSol_df.loc[:,'fscore_surf2']
fscore_surf2 = fscore_surf2.to_numpy()
fscore_surf2 = np.expand_dims(fscore_surf2,-1)

fscore_surf3 = artiSol_df.loc[:,'fscore_surf3']
fscore_surf3 = fscore_surf3.to_numpy()
fscore_surf3 = np.expand_dims(fscore_surf3,-1)

fscore_surf4 = artiSol_df.loc[:,'fscore_surf4']
fscore_surf4 = fscore_surf4.to_numpy()
fscore_surf4 = np.expand_dims(fscore_surf4,-1)

fscore_surf5 = artiSol_df.loc[:,'fscore_surf5']
fscore_surf5 = fscore_surf5.to_numpy()
fscore_surf5 = np.expand_dims(fscore_surf5,-1)

fscore_surf6 = artiSol_df.loc[:,'fscore_surf6']
fscore_surf6 = fscore_surf6.to_numpy()
fscore_surf6 = np.expand_dims(fscore_surf6,-1)

fscore_surf7 = artiSol_df.loc[:,'fscore_surf7']
fscore_surf7 = fscore_surf7.to_numpy()
fscore_surf7 = np.expand_dims(fscore_surf7,-1)

fscore_surf8 = artiSol_df.loc[:,'fscore_surf8']
fscore_surf8 = fscore_surf8.to_numpy()
fscore_surf8 = np.expand_dims(fscore_surf8,-1)


# Création d'une nouvelle table avec ces seules colonnes
table = np.concatenate([fscore_surf1,fscore_surf2, fscore_surf3, fscore_surf4, fscore_surf5,fscore_surf6, fscore_surf7, fscore_surf8],axis=1)

# Association d'un nom à chaque colonne
fscore_surf1 = table[:,0]
fscore_surf2 = table[:,1]
fscore_surf3 = table[:,2]
fscore_surf4 = table[:,3]
fscore_surf5 = table[:,4]
fscore_surf6 = table[:,5]
fscore_surf7 = table[:,6]
fscore_surf8 = table[:,7]

std = np.nanstd(table, axis = 0)
print( 'STD :', std)

mins = np.nanmin(table, axis = 0)
print('MINS: ', mins)

max = np.nanmax(table, axis = 0)
print('MAX: ', max)

mean = np.nanmean(table, axis = 0)
print('MEAN: ', mean)

######### SURF TRAINING ENTITIES : pour calculer le nombre d'entités par surface ayant servi à l'entraînement et voir si les fscore peuvent être fonction du manque de données pour un range de surface

artiSol_df_training = pd.read_csv("training_bati2018.csv",sep=";")

surf_entities_training = artiSol_df_training.loc[:,'area']
surf_entities_training = surf_entities_training.to_numpy()
surf_entities_training = np.expand_dims(surf_entities_training,-1)

table_training = np.concatenate([surf_entities_training],axis=1)

surf_entities_training = table_training[:,0]


genRange = [2.25, 10, 100, 200, 500, 1000, 50000, 100000, 125000]
accumul_nb = []
for i in range(len(genRange)-1):
# crée les intervalles de surface à partir des valeurs indiquées précédemment
    begin_i = genRange[i]
    end_i = genRange[i+1]
    idx1 = np.where(surf_entities_training >= begin_i)
    idx2 = np.where(surf_entities_training <= end_i)

	# récupération des id des entités comprises dans l'intervalle
    intersection = np.intersect1d(idx1[0], idx2[0])  

  # Comptage de 0 (première colonne) et 1 (deuxième colonne)
    count_per_detection_training = len(intersection)
    print('INTERVAL %f - %f equals to %d' % (begin_i, end_i, count_per_detection_training))

    accumul_nb.append(count_per_detection_training)
    print('accumul_nb', accumul_nb)
    print("===================")

## DIAGRAMME #########

xlabel=[]
for i in range(len(genRange)-1):
    begin_i = genRange[i]	
    end_i = genRange[i+1]
    label = str(begin_i)+" - "+str(end_i)
    xlabel.append(label)


x1 = range(len(mean)) 

fig, ax1 = plt.subplots()
ax2 = ax1.twinx()

width = 0.35

bar = ax1.bar(x1, mean, width, color = (1, 0.5, 0.1, 0.4)) #barres d'entités détectées
nb_training = ax2.scatter(x1, accumul_nb)

#min_max_plt = plt.errorbar(np.arange(8), mean, [mean - mins, max - mean], fmt='none', ecolor=(1, 0.3, 0, 1), lw=1, capsize=3)
#std_plt = plt.errorbar(np.arange(8), mean, std, fmt='none', ecolor='black', lw=1, capsize=2)

# Paramétrage des graduations : plt.xticks([position des labels en abscisses], labels (intervalles), orientation labels, taille police)
ax1.set_xticks([r for r in range(len(mean))])
ax1.set_xticklabels([r for r in xlabel], rotation = 50, fontsize = 9)

#plt.xticks([r + width/2 for r in range(len(mean))], xlabel, rotation = 90, fontsize = 10)

#Titres du diagramme et des axes
ax1.set_xlabel('Area')
ax1.set_ylabel('Average F-score')
ax2.set_ylabel('Number of training entities')
#plt.title ("Moyenne du Fscore des patches en fonction de la surface des entités (m²) et nombre d'entités par surface ayant servi à l'entraînement ", fontsize = 10)

plt.legend((bar, nb_training), ('F-score', 'Entities'), loc = 'upper left', fontsize= 'small', bbox_to_anchor=(0, 0.4, 0.3, 0.5), markerscale=0.5, labelspacing=0.1, columnspacing=0.1)
#plt.legend((bar, std_plt, min_max_plt, nb_training), ('moyenne fscore', 'écart-type', 'valeurs min. et max.', 'nb entités training'), bbox_to_anchor=(1.05, 1), fontsize= 'small')  # (legende, position en dehors de l'histogramme, taille police)

plt.show()