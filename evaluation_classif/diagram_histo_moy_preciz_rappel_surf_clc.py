import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

artiSol_df = pd.read_csv("synthese_fscore_clc_equi_2019.csv",sep=";")

# Sélection des colonnes à comparer : la surface des entités test et leur IoU

rappel_surf1_tache = artiSol_df.loc[:,'rappel_surf1_tache']
rappel_surf1_tache = rappel_surf1_tache.to_numpy()
rappel_surf1_tache = np.expand_dims(rappel_surf1_tache,-1)

rappel_surf2_tache = artiSol_df.loc[:,'rappel_surf2_tache']
rappel_surf2_tache = rappel_surf2_tache.to_numpy()
rappel_surf2_tache = np.expand_dims(rappel_surf2_tache,-1)

rappel_surf3_tache = artiSol_df.loc[:,'rappel_surf3_tache']
rappel_surf3_tache = rappel_surf3_tache.to_numpy()
rappel_surf3_tache = np.expand_dims(rappel_surf3_tache,-1)

rappel_surf4_tache = artiSol_df.loc[:,'rappel_surf4_tache']
rappel_surf4_tache = rappel_surf4_tache.to_numpy()
rappel_surf4_tache = np.expand_dims(rappel_surf4_tache,-1)

rappel_surf5_tache = artiSol_df.loc[:,'rappel_surf5_tache']
rappel_surf5_tache = rappel_surf5_tache.to_numpy()
rappel_surf5_tache = np.expand_dims(rappel_surf5_tache,-1)

rappel_surf6_tache = artiSol_df.loc[:,'rappel_surf6_tache']
rappel_surf6_tache = rappel_surf6_tache.to_numpy()
rappel_surf6_tache = np.expand_dims(rappel_surf6_tache,-1)

rappel_surf7_tache = artiSol_df.loc[:,'rappel_surf7_tache']
rappel_surf7_tache = rappel_surf7_tache.to_numpy()
rappel_surf7_tache = np.expand_dims(rappel_surf7_tache,-1)

rappel_surf8_tache = artiSol_df.loc[:,'rappel_surf8_tache']
rappel_surf8_tache = rappel_surf8_tache.to_numpy()
rappel_surf8_tache = np.expand_dims(rappel_surf8_tache,-1)

rappel_surf1_out = artiSol_df.loc[:,'rappel_surf1_tache_out']
rappel_surf1_out = rappel_surf1_out.to_numpy()
rappel_surf1_out = np.expand_dims(rappel_surf1_out,-1)

rappel_surf2_out = artiSol_df.loc[:,'rappel_surf2_tache_out']
rappel_surf2_out = rappel_surf2_out.to_numpy()
rappel_surf2_out = np.expand_dims(rappel_surf2_out,-1)

rappel_surf3_out = artiSol_df.loc[:,'rappel_surf3_tache_out']
rappel_surf3_out = rappel_surf3_out.to_numpy()
rappel_surf3_out = np.expand_dims(rappel_surf3_out,-1)

rappel_surf4_out = artiSol_df.loc[:,'rappel_surf4_tache_out']
rappel_surf4_out = rappel_surf4_out.to_numpy()
rappel_surf4_out = np.expand_dims(rappel_surf4_out,-1)

rappel_surf5_out = artiSol_df.loc[:,'rappel_surf5_tache_out']
rappel_surf5_out = rappel_surf5_out.to_numpy()
rappel_surf5_out = np.expand_dims(rappel_surf5_out,-1)

rappel_surf6_out = artiSol_df.loc[:,'rappel_surf6_tache_out']
rappel_surf6_out = rappel_surf6_out.to_numpy()
rappel_surf6_out = np.expand_dims(rappel_surf6_out,-1)

rappel_surf7_out = artiSol_df.loc[:,'rappel_surf7_tache_out']
rappel_surf7_out = rappel_surf7_out.to_numpy()
rappel_surf7_out = np.expand_dims(rappel_surf7_out,-1)

rappel_surf8_out = artiSol_df.loc[:,'rappel_surf8_tache_out']
rappel_surf8_out = rappel_surf8_out.to_numpy()
rappel_surf8_out = np.expand_dims(rappel_surf8_out,-1)


preciz_surf1_tache = artiSol_df.loc[:,'preciz_surf1_tache']
preciz_surf1_tache = preciz_surf1_tache.to_numpy()
preciz_surf1_tache = np.expand_dims(preciz_surf1_tache,-1)

preciz_surf2_tache = artiSol_df.loc[:,'preciz_surf2_tache']
preciz_surf2_tache = preciz_surf2_tache.to_numpy()
preciz_surf2_tache = np.expand_dims(preciz_surf2_tache,-1)

preciz_surf3_tache = artiSol_df.loc[:,'preciz_surf3_tache']
preciz_surf3_tache = preciz_surf3_tache.to_numpy()
preciz_surf3_tache = np.expand_dims(preciz_surf3_tache,-1)

preciz_surf4_tache = artiSol_df.loc[:,'preciz_surf4_tache']
preciz_surf4_tache = preciz_surf4_tache.to_numpy()
preciz_surf4_tache = np.expand_dims(preciz_surf4_tache,-1)

preciz_surf5_tache = artiSol_df.loc[:,'preciz_surf5_tache']
preciz_surf5_tache = preciz_surf5_tache.to_numpy()
preciz_surf5_tache = np.expand_dims(preciz_surf5_tache,-1)

preciz_surf6_tache = artiSol_df.loc[:,'preciz_surf6_tache']
preciz_surf6_tache = preciz_surf6_tache.to_numpy()
preciz_surf6_tache = np.expand_dims(preciz_surf6_tache,-1)

preciz_surf7_tache = artiSol_df.loc[:,'preciz_surf7_tache']
preciz_surf7_tache = preciz_surf7_tache.to_numpy()
preciz_surf7_tache = np.expand_dims(preciz_surf7_tache,-1)

preciz_surf8_tache = artiSol_df.loc[:,'preciz_surf8_tache']
preciz_surf8_tache = preciz_surf8_tache.to_numpy()
preciz_surf8_tache = np.expand_dims(preciz_surf8_tache,-1)

preciz_surf1_out = artiSol_df.loc[:,'preciz_surf1_tache_out']
preciz_surf1_out = preciz_surf1_out.to_numpy()
preciz_surf1_out = np.expand_dims(preciz_surf1_out,-1)

preciz_surf2_out = artiSol_df.loc[:,'preciz_surf2_tache_out']
preciz_surf2_out = preciz_surf2_out.to_numpy()
preciz_surf2_out = np.expand_dims(preciz_surf2_out,-1)

preciz_surf3_out = artiSol_df.loc[:,'preciz_surf3_tache_out']
preciz_surf3_out = preciz_surf3_out.to_numpy()
preciz_surf3_out = np.expand_dims(preciz_surf3_out,-1)

preciz_surf4_out = artiSol_df.loc[:,'preciz_surf4_tache_out']
preciz_surf4_out = preciz_surf4_out.to_numpy()
preciz_surf4_out = np.expand_dims(preciz_surf4_out,-1)

preciz_surf5_out = artiSol_df.loc[:,'preciz_surf5_tache_out']
preciz_surf5_out = preciz_surf5_out.to_numpy()
preciz_surf5_out = np.expand_dims(preciz_surf5_out,-1)

preciz_surf6_out = artiSol_df.loc[:,'preciz_surf6_tache_out']
preciz_surf6_out = preciz_surf6_out.to_numpy()
preciz_surf6_out = np.expand_dims(preciz_surf6_out,-1)

preciz_surf7_out = artiSol_df.loc[:,'preciz_surf7_tache_out']
preciz_surf7_out = preciz_surf7_out.to_numpy()
preciz_surf7_out = np.expand_dims(preciz_surf7_out,-1)

preciz_surf8_out = artiSol_df.loc[:,'preciz_surf8_tache_out']
preciz_surf8_out = preciz_surf8_out.to_numpy()
preciz_surf8_out = np.expand_dims(preciz_surf8_out,-1)

# Création d'une nouvelle table avec ces deux seules colonnes
table_rappel_tache = np.concatenate([rappel_surf1_tache, rappel_surf2_tache, rappel_surf3_tache, rappel_surf4_tache, rappel_surf5_tache,rappel_surf6_tache, rappel_surf7_tache, rappel_surf8_tache], axis =1) 
table_preciz_tache = np.concatenate([preciz_surf1_tache, preciz_surf2_tache, preciz_surf3_tache, preciz_surf4_tache, preciz_surf5_tache, preciz_surf6_tache, preciz_surf7_tache, preciz_surf8_tache], axis =1) 
table_rappel_out = np.concatenate([rappel_surf1_out, rappel_surf2_out, rappel_surf3_out, rappel_surf4_out, rappel_surf5_out,rappel_surf6_out, rappel_surf7_out, rappel_surf8_out], axis =1) 
table_preciz_out = np.concatenate([preciz_surf1_out, preciz_surf2_out, preciz_surf3_out, preciz_surf4_out, preciz_surf5_out, preciz_surf6_out, preciz_surf7_out, preciz_surf8_out], axis =1) 

# Association d'un nom à chaque colonne
rappel_surf1_tache = table_rappel_tache[:,0]
rappel_surf2_tache = table_rappel_tache[:,1]
rappel_surf3_tache = table_rappel_tache[:,2]
rappel_surf4_tache = table_rappel_tache[:,3]
rappel_surf5_tache = table_rappel_tache[:,4]
rappel_surf6_tache = table_rappel_tache[:,5]
rappel_surf7_tache = table_rappel_tache[:,6]
rappel_surf8_tache = table_rappel_tache[:,7]

preciz_surf1_tache = table_preciz_tache[:,0]
preciz_surf2_tache = table_preciz_tache[:,1]
preciz_surf3_tache = table_preciz_tache[:,2]
preciz_surf4_tache = table_preciz_tache[:,3]
preciz_surf5_tache = table_preciz_tache[:,4]
preciz_surf6_tache = table_preciz_tache[:,5]
preciz_surf7_tache = table_preciz_tache[:,6]
preciz_surf8_tache = table_preciz_tache[:,7]

rappel_surf1_out = table_rappel_out[:,0]
rappel_surf2_out = table_rappel_out[:,1]
rappel_surf3_out = table_rappel_out[:,2]
rappel_surf4_out = table_rappel_out[:,3]
rappel_surf5_out = table_rappel_out[:,4]
rappel_surf6_out = table_rappel_out[:,5]
rappel_surf7_out = table_rappel_out[:,6]
rappel_surf8_out = table_rappel_out[:,7]

preciz_surf1_out = table_preciz_out[:,0]
preciz_surf2_out = table_preciz_out[:,1]
preciz_surf3_out = table_preciz_out[:,2]
preciz_surf4_out = table_preciz_out[:,3]
preciz_surf5_out = table_preciz_out[:,4]
preciz_surf6_out = table_preciz_out[:,5]
preciz_surf7_out = table_preciz_out[:,6]
preciz_surf8_out = table_preciz_out[:,7]

std_preciz_tache = np.nanstd(table_preciz_tache, axis = 0)
std_rappel_tache = np.nanstd(table_rappel_tache, axis = 0)

mins_preciz_tache = np.nanmin(table_preciz_tache, axis = 0)
mins_rappel_tache = np.nanmin(table_rappel_tache, axis = 0)

max_preciz_tache = np.nanmax(table_preciz_tache, axis = 0)
max_rappel_tache = np.nanmax(table_rappel_tache, axis = 0)

mean_preciz_tache = np.nanmean(table_preciz_tache, axis = 0)
mean_rappel_tache = np.nanmean(table_rappel_tache, axis = 0)

std_preciz_out = np.nanstd(table_preciz_out, axis = 0)
std_rappel_out = np.nanstd(table_rappel_out, axis = 0)

mins_preciz_out = np.nanmin(table_preciz_out, axis = 0)
mins_rappel_out = np.nanmin(table_rappel_out, axis = 0)

max_preciz_out = np.nanmax(table_preciz_out, axis = 0)
max_rappel_out = np.nanmax(table_rappel_out, axis = 0)

mean_preciz_out = np.nanmean(table_preciz_out, axis = 0)
mean_rappel_out = np.nanmean(table_rappel_out, axis = 0)

#### ----- DIAGRAMME ----------------- ####

# Création des labels en abscisses 
genRange = [2.25, 10, 100, 200, 500, 1000, 50000, 100000, 125000]

xlabel=[]
for i in range(len(genRange)-1):
    begin_i = genRange[i]	
    end_i = genRange[i+1]
    label = str(begin_i)+" - "+str(end_i)
    xlabel.append(label)

width = 0.15
x1 = range(len(mean_preciz_tache)) #position des barres d'entités détectées
x2 = [i + width for i in x1]
x3 = [i + 2*width for i in x1] #position des barres d'entités détectées
x4 = [i + 3*width for i in x1]

# min_tache = plt.scatter(x1, mins_tache, s = 5,  color = 'green')
# max_tache = plt.scatter(x1, max_tache, s = 5,  color = 'red')
# min_out = plt.scatter(x2, mins_out, s = 5  color = 'green')
# max_out = plt.scatter(x2, max_out, s = 5,  color = 'red')

preciz_tache = plt.bar(x1, mean_preciz_tache, width, color = (1, 0.5, 0.1, 0.8)) #barres d'entités détectées
preciz_out = plt.bar(x2, mean_preciz_out, width, color = (1, 0.5, 0.1, 0.4)) #barres d'entités détectées

rappel_tache = plt.bar(x3, mean_rappel_tache, width, color = (0.1, 0.5, 1, 0.8)) #barres d'entités détectées
rappel_out = plt.bar(x4, mean_rappel_out, width, color = (0.1, 0.5, 1, 0.4)) #barres d'entités détectées



# min_max_tache = plt.errorbar(x1, mean_tache, [mean_tache - mins_tache, max_tache - mean_tache], fmt='none', ecolor=(1, 0.3, 0, 1), lw=1, capsize=3)
# min_max_out = plt.errorbar(x2, mean_out, [mean_out - mins_out, max_out - mean_out], fmt='none', ecolor=(1, 0.3, 0, 1), lw=1, capsize=3)


# std_plt_tache = plt.errorbar(x1, mean_tache, std_tache, fmt='none', ecolor='black', lw=1, capsize=2)
# std_plt_out = plt.errorbar(x2, mean_out, std_out, fmt='none', ecolor='black', lw=1, capsize=2)

# Paramétrage des graduations : plt.xticks([position des labels en abscisses], labels (intervalles), orientation labels, taille police)
plt.xticks([r + width*1.5 for r in range(len(mean_preciz_tache))], xlabel, rotation = 50, fontsize = 8)

#Titres du diagramme et des axes
plt.xlabel('Area')
plt.ylabel('Average of the F-Score')
# plt.title ("Moyenne du rappel et de la précision en fonction de la surface des entités (m²) et de la densité urbaine", fontsize = 14)

plt.legend((preciz_tache, preciz_out, rappel_tache, rappel_out), ('Précision tache urbaine', 'Précision hors tache urbaine', 'Rappel tache urbaine', 'Rappel hors tache urbaine'), bbox_to_anchor=(1, 1), fontsize= 'small')  # (legende, position en dehors de l'histogramme, taille police)

plt.show()