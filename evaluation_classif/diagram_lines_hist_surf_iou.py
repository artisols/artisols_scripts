import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

artiSol_df = pd.read_csv("table_surf_iou_2019.csv",sep=";")

print(artiSol_df)

# Sélection des colonnes à comparer : la surface des entités test et leur IoU
test_surf = artiSol_df.loc[:,'test_surf']
test_surf = test_surf.to_numpy()
test_surf = np.expand_dims(test_surf,-1)

iou = artiSol_df.loc[:,'iou']
iou = iou.to_numpy()
iou = np.expand_dims(iou,-1)

# Création d'une nouvelle table avec ces deux seules colonnes
table = np.concatenate([test_surf,iou],axis=1)

# Valeurs qui serviront d'intervalles de surface
genRange = [2.25, 10, 100, 200, 500, 1000, 50000, 100000, 125000]

# Association d'un nom à chaque colonne
surf_column = table[:,0]
iou_column = table[:,1]

# Seuil de l'IoU permettant d'identifier les entités détectée
th = 30
th2 = 50
th3 = 70
# Condition à remplir : True = Entités ayant un IoU > 0 et donc détectées.
thresholded_iou_column = (iou_column >= th).astype("int")
thresholded_iou_column2 = (iou_column >= th2).astype("int")
thresholded_iou_column3 = (iou_column >= th3).astype("int")
#print(thresholded_iou_column)

accumul_detect = []
accumul_notDetect = []

accumul_detect2 = []
accumul_notDetect2 = []

accumul_detect3 = []
accumul_notDetect3 = []

for i in range(len(genRange)-1):
    # crée les intervalles de surface à partir des valeurs indiquées précédemment
    begin_i = genRange[i]
    end_i = genRange[i+1]
    idx1 = np.where(surf_column >= begin_i)
    idx2 = np.where(surf_column <= end_i)

    # récupération des id des entités comprises dans l'intervalle
    intersection = np.intersect1d(idx1[0], idx2[0])  

  # Vérification de la condition : 1 (entités avec IoU >0) 0 (entités avec IoU <0)
    temp_iou = thresholded_iou_column[intersection]  
    temp_iou2 = thresholded_iou_column2[intersection] 
    temp_iou3 = thresholded_iou_column3[intersection] 

  # Comptage de 0 (première colonne) et 1 (deuxième colonne)
    count_per_detection = np.bincount(temp_iou)
    count_per_detection = count_per_detection.astype("float")
    
    count_per_detection2 = np.bincount(temp_iou2)
    count_per_detection2 = count_per_detection2.astype("float")
    
    count_per_detection3 = np.bincount(temp_iou3)
    count_per_detection3 = count_per_detection3.astype("float")

  # Pourcentage de 0 et 1
    count_per_detection = (count_per_detection / np.sum(count_per_detection) ) * 100
    print("bincount % ",count_per_detection)
    print("bincount ",np.bincount(genRange))

    count_per_detection2 = (count_per_detection2 / np.sum(count_per_detection2) ) * 100
    
    count_per_detection3 = (count_per_detection3 / np.sum(count_per_detection3) ) * 100
    
  # Remplissage des listes (précédemment créées) avec les valeurs de pourcentages
    accumul_detect.append(count_per_detection[1])
    accumul_notDetect.append(count_per_detection[0])
    print("begin_i %f end_i %f with number of samples %d" %(begin_i, end_i, len(intersection) ))
    print('accumul_detect', accumul_detect)
    print("===================")
    
    accumul_detect2.append(count_per_detection2[1])
    accumul_notDetect2.append(count_per_detection2[0])
    
    accumul_detect3.append(count_per_detection3[1])
    accumul_notDetect3.append(count_per_detection3[0])
    
# Création des labels pour l'axe des abscisses (seuils de surface)
xlabel=[]
for i in range(len(genRange)-1):
    begin_i = genRange[i]    
    end_i = genRange[i+1]
    label = str(begin_i)+" - "+str(end_i)
    xlabel.append(label)
    #print(xlabel)

# Paramétrage des barres
width = 0.35   # largeur des barres
x1 = range(len(accumul_detect))  #position des barres d'entités détectées
x2 = [i + width for i in x1]    #position des barres d'entités non détectées 


# Génération des barres
bar1 = plt.bar(x1, accumul_detect, width, color = (0.8, 0.1, 0.1, 0.4)) #barres d'entités détectées
bar2 = plt.bar(x2, accumul_notDetect, width, color = (0.1, 0.1, 0.1, 0.4)) # barres d'entités non détectées  # color : couleurs R,G,B,Transparancy

line1, = plt.plot(x1, accumul_detect2, color = 'blue') #courbe d'entités détectées

line2, = plt.plot(x1, accumul_detect3, color = 'green') #courbe d'entités détectées

# Paramétrage des graduations : plt.xticks([position des labels en abscisses], labels (intervalles), orientation labels, taille police)
plt.xticks([r + width/2 for r in range(len(accumul_detect))], xlabel, rotation = 50, fontsize = 8)

# Titres du diagramme et des axes
plt.xlabel('Surface')
plt.ylabel('Pourcentage')
plt.title ("Pourcentage des objets détectés en fonction de leur surface (m²) et d'un seuil IoU", fontsize = 10)

# Légende associée au diagramme
legende = 'IoU > {}'.format(th)
legende1 = 'IoU < {}'.format(th)
legend2 = 'IoU > {}'.format(th2)
legend3 = 'IoU > {}'.format(th3)

plt.legend((bar1, bar2, line1, line2), (legende, legende1, legend2, legend3),  bbox_to_anchor=(1, 1), loc='upper left', fancybox=True, fontsize= 'small')   # (legende, position en dehors de l'histogramme, taille police)

plt.show()
