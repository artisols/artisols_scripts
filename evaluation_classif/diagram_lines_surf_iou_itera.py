import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

artiSol_df = pd.read_csv("synthese_fscore_surf.csv",sep=";")

print(artiSol_df)

# Sélection des colonnes à comparer : la surface des entités test et leur IoU
test_surf = artiSol_df.loc[:,'test_surf']
test_surf = test_surf.to_numpy()
test_surf = np.expand_dims(test_surf,-1)

iou = artiSol_df.loc[:,'iou']
iou = iou.to_numpy()
iou = np.expand_dims(iou,-1)

# Création d'une nouvelle table avec ces deux seules colonnes
table = np.concatenate([test_surf,iou],axis=1)

# Valeurs qui serviront d'intervalles de surface
genRange = [2.25, 10, 100, 200, 500, 1000, 50000, 100000, 125000]

# Association d'un nom à chaque colonne
surf_column = table[:,0]
iou_column = table[:,1]

# Seuil de l'IoU permettant d'identifier les entités détectée
th = [40, 50, 70]
j = 0
tables_total = []

for t in th :
    print( 'THRESHOLD : ', t)
    j = j+1
    accumul_detect = 'accumul_detect'+str(j)
    print('accumul_detect : ', accumul_detect)
    accumul_detect = []

    for i in range(len(genRange)-1):
        # crée les intervalles de surface à partir des valeurs indiquées précédemment
        begin_i = genRange[i]
        end_i = genRange[i+1]
        idx1 = np.where(surf_column >= begin_i)
        idx2 = np.where(surf_column <= end_i)

        # récupération des id des entités comprises dans l'intervalle
        intersection = np.intersect1d(idx1[0], idx2[0])      
        # Vérification de la condition : 1 (entités avec IoU >0) 0 (entités avec IoU <0)
        thresholded_iou_column = (iou_column >= t)
        temp_iou = thresholded_iou_column[intersection]  
        # Comptage de 0 (première colonne) et 1 (deuxième colonne)
        count_per_detection = np.bincount(temp_iou)
        count_per_detection = count_per_detection.astype("float")
    
        # Pourcentage de 0 et 1
        count_per_detection = (count_per_detection / np.sum(count_per_detection) ) * 100
        print("bincount % ",count_per_detection)
        print("bincount ",np.bincount(genRange))
 
        # Remplissage des listes (précédemment créées) avec les valeurs de pourcentages
        
        accumul_detect.append(count_per_detection[1])
        print("begin_i %f end_i %f with number of samples %d" %(begin_i, end_i, len(intersection) ))
        print('accumul_detect', accumul_detect)
        print("===================")
    tables_total.append(accumul_detect)
print('tables_total : ', tables_total)


#DIAGRAMME
# Création des labels pour l'axe des abscisses (seuils de surface)
xlabel=[]
for i in range(len(genRange)-1):
    begin_i = genRange[i]    
    end_i = genRange[i+1]
    label = str(begin_i)+" - "+str(end_i)
    xlabel.append(label)

m = 0
lines = []
legendes_total = []   
for table in tables_total :
    colors = ['red', 'orange', 'green']  
# Génération des courbes en fonction du seuil IoU
    line,= plt.plot(table, color = colors[m])
    m = m+1
    lines.append(line)

# Paramétrage des graduations : plt.xticks([position des labels en abscisses], labels (intervalles), orientation labels, taille police)
plt.xticks([r for r in range(len(accumul_detect))], xlabel, rotation = 50, fontsize = 8)

# Titres du diagramme et des axes
plt.xlabel('Surface', fontsize = 11)
plt.ylabel('Pourcentage', fontsize = 11)
plt.title ("Pourcentage des objets détectés en fonction de leur surface (m²) et d'un seuil IoU", fontsize = 14)
    
# Légende associée au diagramme
for t in th :
    legende = 'IoU > {}'.format(t)
    legendes_total.append(legende)
print('LEGENDE : ', legendes_total)

plt.legend((courbe for courbe in lines), (leg for leg in legendes_total),  bbox_to_anchor=(1, 1), loc='upper left', fancybox=True, fontsize= 'small')  

plt.show()
