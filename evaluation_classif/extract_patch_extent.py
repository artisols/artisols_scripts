# Vectorisation des tuiles (avec leur ID) qui ont servi pour l'évaluation (pas de 7000 x 0 pxl)

import sys, os
import gdal, ogr
import otbApplication

def patchList(inputImg, size, step):
	""" List of tiles in pixel units
	@param inputImg: <string> classification image path
	@param size:     <int>    size of tile (square) (in pixels)
	@param step:     <int>    research step (in pixels)
	@return :        <list>   list of tiles with [startX, starty, size]
	"""
	raster_ds = gdal.Open(inputImg)
	cols = raster_ds.RasterXSize
	#cols = 325921
	rows = raster_ds.RasterYSize
	#rows = 219402
	raster_ds = None
	list_startX = list()
	list_startY = list()
	startX = 1
	startY = 1
	while startX < (cols-size):
		list_startX.append(startX)
		startX = startX + size + step
	while startY < (rows-size):
		list_startY.append(startY)
		startY = startY + size + step
	final_list = list()
	for x in list_startX:
		for y in list_startY:
			bloc = [x, y, size]
			final_list.append(bloc)
	return final_list

def filterPatches(list_patch, inputImg, adm_poly, outVectorFile):
	""" List of filtered tiles within polygon entity
	@param list_patch: <list>   list of tiles with [startX, starty, size]
	@param inputImg:   <string> classification image path
	@param adm_poly:   <string> polygon shapefile (in Lambert93) path
	@param outVectorFile : <string> output polygon shapefile (in Lambert93) path
	@return:           <list>   list of filtered tiles with [startX, starty, size]
	"""
	driver = ogr.GetDriverByName('ESRI Shapefile')
	adm_ds = driver.Open(adm_poly, 0)
	adm_layer = adm_ds.GetLayer()
	feat = adm_layer.GetFeature(0)
	refGeom = feat.GetGeometryRef()
	
	if os.path.exists(outVectorFile):
		driver.DeleteDataSource(outVectorFile)
	
	# projection
	dest_srs = ogr.osr.SpatialReference()
	dest_srs.ImportFromEPSG(2154)
	# Create the output shapefile
	outDataSource = driver.CreateDataSource(outVectorFile)
	outLayer = outDataSource.CreateLayer("maille", dest_srs, geom_type=ogr.wkbPolygon)
	
	idField = ogr.FieldDefn("id", ogr.OFTString)
	outLayer.CreateField(idField)
	
	driver = None
	
	raster_ds = gdal.Open(inputImg)
	geotransf = raster_ds.GetGeoTransform()
	# minX = 371564.25
	minX = geotransf[0]
	# maxY = 6456741.00
	maxY = geotransf[3]
	# res = 1.5
	res = geotransf[1]
	raster_ds = None

	filtered_list = list()	
	for patch in list_patch:
		ulx = minX+(patch[0]-1)*res
		uly = maxY-(patch[1]-1)*res
		lrx = minX+(patch[0]-1)*res + patch[2]*res
		lry = maxY-(patch[1]-1)*res - patch[2]*res
		
		ring = ogr.Geometry(ogr.wkbLinearRing)
		ring.AddPoint(ulx, uly)
		ring.AddPoint(lrx, uly)
		ring.AddPoint(lrx, lry)
		ring.AddPoint(ulx, lry)
		ring.CloseRings()
		poly = ogr.Geometry(ogr.wkbPolygon)
		poly.AddGeometry(ring)
		
		# if poly.Overlaps(refGeom):
		if poly.Intersects(refGeom):
			filtered_list.append(patch)
		#print('filtered_list :', filtered_list)
			# Create the feature and set values
			featureDefn = outLayer.GetLayerDefn()
			feature = ogr.Feature(featureDefn)
			feature.SetGeometry(poly)
			feature.SetField("id", 'tile{}_{}'.format(patch[0], patch[1]))
			outLayer.CreateFeature(feature)
			feature = None
	outDataSource = None
	return filtered_list


#--- main ---
if __name__ == "__main__":
	# Parameters:
	#	argv_1 : classification image in L93
	#	argv_2 : admin borders in shapefile L93
	#	argv_3 : output matrix vector file
	# command example: python extract_patch_entent.py 
	#					/home/kenji/WORK/ARTISOLS/DATA_REMI/cartes/2019/artisols_map_2019_maskOcc.tif
	#					/home/kenji/WORK/ARTISOLS/SCRIPTS/Extract_Patch/adm_occitanie/adm_occitanie.shp
	#					/home/kenji/WORK/ARTISOLS/OUTPUT_VALID_TILES/MAP_TILES_2019/grid_ref.shp
	testimg = sys.argv[1]
	admpoly = sys.argv[2]
	outputGrid = sys.argv[3]
	
	# class pixel values to extract according to classification image
	classNum = [11, 12]

	# list of tiles
	list_tile = patchList(testimg, 7000, 0)
	print("total des tuiles : ", len(list_tile))
	# list of filtered tiles
	new_list_tile = filterPatches(list_tile, testimg, admpoly, outputGrid)
	#print("total des tuiles après filtre : ", len(new_list_tile))
		