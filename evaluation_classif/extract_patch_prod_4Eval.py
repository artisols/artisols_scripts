import sys, os
import gdal, ogr
import otbApplication

def patchList(inputImg, size, step):
	""" List of tiles in pixel units
	@param inputImg: <string> classification image path
	@param size:     <int>    size of tile (square) (in pixels)
	@param step:     <int>    research step (in pixels)
	@return :        <list>   list of tiles with [startX, starty, size]
	"""
	raster_ds = gdal.Open(inputImg)
	cols = raster_ds.RasterXSize
	#cols = 325921
	rows = raster_ds.RasterYSize
	#rows = 219402
	raster_ds = None
	list_startX = list()
	list_startY = list()
	startX = 1
	startY = 1
	while startX < (cols-size):
		list_startX.append(startX)
		startX = startX + size + step
	while startY < (rows-size):
		list_startY.append(startY)
		startY = startY + size + step
	final_list = list()
	for x in list_startX:
		for y in list_startY:
			bloc = [x, y, size]
			final_list.append(bloc)
	return final_list

def filterPatches(list_patch, inputImg, adm_poly):
	""" List of filtered tiles within polygon entity
	@param list_patch: <list>   list of tiles with [startX, starty, size]
	@param inputImg:   <string> classification image path
	@param adm_poly:   <string> polygon shapefile (in Lambert93) path
	@return:           <list>   list of filtered tiles with [startX, starty, size]
	"""
	driver = ogr.GetDriverByName('ESRI Shapefile')
	adm_ds = driver.Open(adm_poly, 0)
	adm_layer = adm_ds.GetLayer()
	feat = adm_layer.GetFeature(0)
	refGeom = feat.GetGeometryRef()
	driver = None
	
	raster_ds = gdal.Open(inputImg)
	geotransf = raster_ds.GetGeoTransform()
	# minX = 371564.25
	minX = geotransf[0]
	# maxY = 6456741.00
	maxY = geotransf[3]
	# res = 1.5
	res = geotransf[1]
	raster_ds = None

	filtered_list = list()	
	for patch in list_patch:
		ulx = minX+(patch[0]-1)*res
		uly = maxY-(patch[1]-1)*res
		lrx = minX+(patch[0]-1)*res + patch[2]*res
		lry = maxY-(patch[1]-1)*res - patch[2]*res
		
		ring = ogr.Geometry(ogr.wkbLinearRing)
		ring.AddPoint(ulx, uly)
		ring.AddPoint(lrx, uly)
		ring.AddPoint(lrx, lry)
		ring.AddPoint(ulx, lry)
		ring.CloseRings()
		poly = ogr.Geometry(ogr.wkbPolygon)
		poly.AddGeometry(ring)
		
		# if poly.Overlaps(refGeom):
		if poly.Intersects(refGeom):
			filtered_list.append(patch)
		#print('filtered_list :', filtered_list)
	return filtered_list
	
def extractPatch(inputImg, outputTile, pxl_startx, pxl_starty, pxl_size, classNb, classCloud):
	""" Extraction of classification tiles in raster and vector
	@param inputImg   <string> classification image path
	@param outputTile <string> output image tile path
	@param pxl_startx <int>    startX in pixels
	@param pxl_starty <int>    startY in pixels
	@param pxl_size   <int>    size in pixels
	@param classNb    <list>   list of classes index
	"""
	app_extractRoi = otbApplication.Registry.CreateApplication("ExtractROI")
	app_extractRoi.SetParameterString("in", inputImg)
	app_extractRoi.SetParameterInt("startx", pxl_startx)
	app_extractRoi.SetParameterInt("starty", pxl_starty)
	app_extractRoi.SetParameterInt("sizex", pxl_size)
	app_extractRoi.SetParameterInt("sizey", pxl_size)
	app_extractRoi.SetParameterString("out", "my_roi.tif")
	app_extractRoi.Execute()
	
	# Select noData value (cloud) : 255
	exp_cloud = "(im1b1 == {})?255:0".format(classCloud[0])
	
	# In fine : if pxl value (in img ref/classif) = class (11 & 12 in this case) --> 1
	#			if pxl value (in img ref/classif) = noData (255) --> 255
	
	if len(classNb) == 1:
		exp = "(im1b1 == {})?1:0".format(classNb[0])
	else:
		exp = "(im1b1 == {}".format(classNb[0])
		for nb in classNb[1:]:
			exp = "{} || im1b1 == {}".format(exp, nb)
		exp = "{})?1:{}".format(exp, exp_cloud)
	print(exp)

	app_bandMath = otbApplication.Registry.CreateApplication("BandMath")
	app_bandMath.AddImageToParameterInputImageList("il", app_extractRoi.GetParameterOutputImage("out"))
	app_bandMath.SetParameterString("out", '{}?&gdal:co:COMPRESS=DEFLATE'.format(outputTile))
	app_bandMath.SetParameterString("exp", exp)
	app_bandMath.SetParameterOutputImagePixelType("out", 0)
	app_bandMath.ExecuteAndWriteOutput()

def polygonize(inputTile, outputVector):
	""" Convert raster tile in vector file
	@param inputTile    <string> tile image path
	@param outputVector <string> output vector file path
	"""
	raster_ds = gdal.Open(inputTile)
	band = raster_ds.GetRasterBand(1)
	driver = ogr.GetDriverByName("ESRI Shapefile")
	if os.path.exists(outputVector):
		driver.DeleteDataSource(outputVector)
	dest_srs = ogr.osr.SpatialReference()
	dest_srs.ImportFromEPSG(2154)
	out_ds = driver.CreateDataSource(outputVector)
	outLayer = out_ds.CreateLayer("polygonized", geom_type=ogr.wkbPolygon, srs=dest_srs)
	classField = ogr.FieldDefn('class', ogr.OFTInteger)
	outLayer.CreateField(classField)
	gdal.Polygonize(band, band, outLayer, 0, [], callback=None )
	out_ds.Destroy()
	raster_ds = None
	
	# # Delete entities NoData 255 from shapefiles
	# shapefile = ogr.Open(outputVector, 1)
	# layer = shapefile.GetLayer()
	# layer.SetAttributeFilter("class = 255")

	# for feat in layer:
		# print(feat.GetField("class"))
		# layer.DeleteFeature(feat.GetFID())
	# shapefile.ExecuteSQL('REPACK ' + layer.GetName())

def rasterize(inputVector, imgRef, outputRaster, burnValue):
	""" Rasterize vector file according to input image geometry
	@param inputVector  <string> vector file path
	@param imgRef       <string> reference image path
	@param outputRaster <string> output image path
	"""
	app_rasterize = otbApplication.Registry.CreateApplication("Rasterization")
	app_rasterize.SetParameterString("in", inputVector)
	app_rasterize.SetParameterString("im", imgRef)
	app_rasterize.SetParameterString("out", '{}?&gdal:co:COMPRESS=DEFLATE'.format(outputRaster))
	app_rasterize.SetParameterOutputImagePixelType("out", 0)
	app_rasterize.SetParameterFloat("mode.binary.foreground", burnValue)
	app_rasterize.ExecuteAndWriteOutput()

#--- main ---
if __name__ == "__main__":
	# Parameters:
	#	argv_1 : classification image in L93
	#	argv_2 : admin borders in shapefile L93
	#	argv_3 : output directory
	#	argv_4 : input reference vector file (i.e. BD Topo IGN)
	#	argv_5 : input training vector file for the whole Occitanie
	# command example: python extract_patch_prod_rev.py 
	#					/home/kenji/WORK/ARTISOLS/DATA_REMI/cartes/2019/artisols_map_2019_maskOcc.tif 
	#					/home/kenji/WORK/ARTISOLS/SCRIPTS/Extract_Patch/adm_occitanie/adm_occitanie.shp
	#					/home/kenji/WORK/ARTISOLS/OUTPUT_VALID_TILES/MAP_TILES_2019
	#					/home/kenji/WORK/ARTISOLS/BD_OCS2018/BATI_OCCITANIE2018.shp
	#					/home/kenji/WORK/ARTISOLS/OUTPUT_POLY_PATCHES/POLY_PATCHES_2019/merge_poly_patches_2019.shp
	testimg = sys.argv[1]
	admpoly = sys.argv[2]
	outputD = sys.argv[3]
	refVect = sys.argv[4]
	in_vector_training_patches = sys.argv[5]
	
	# class pixel values to extract according to classification image // class pixel for clouds
	classNum = [11, 12]
	classCloud = [255]
	
	# list of tiles
	list_tile = patchList(testimg, 7000, 0)
	print("total des tuiles : ", len(list_tile))
	# list of filtered tiles
	new_list_tile = filterPatches(list_tile, testimg, admpoly)
	print("total des tuiles après filtre : ", len(new_list_tile))
	
	# tiles extraction
	nb_tile = 1
	for tile in new_list_tile:
		print("---\n-->traitement de la tuile {}/{}".format(nb_tile, len(new_list_tile)))
		nb_tile += 1
		
		startx = tile[0]
		starty = tile[1]
		size = tile[2]
		out_VTile = os.path.join(outputD, "tile{}_{}_prod.shp".format(startx, starty))
		out_RTile = os.path.join(outputD, "tile{}_{}_prod.tif".format(startx, starty))
		out_Vref = os.path.join(outputD, "tile{}_{}_test.shp".format(startx, starty))
		out_Rref = os.path.join(outputD, "tile{}_{}_test.tif".format(startx, starty))
		out_Rmask = os.path.join(outputD, "tile{}_{}_mask.tif".format(startx, starty))
		out_RTile_mask = os.path.join(outputD, "tile{}_{}_prod_mask.tif".format(startx, starty))
		out_Rref_mask = os.path.join(outputD, "tile{}_{}_test_mask.tif".format(startx, starty))
		
		out_Vtile_noext = os.path.splitext(out_VTile)[0]
		out_Vref_noext = os.path.splitext(out_Vref)[0]
		
		if (os.path.exists(out_Vtile_noext+'.shp') and os.path.exists(out_Vtile_noext+'.dbf') and os.path.exists(out_Vtile_noext+'.shx') and os.path.exists(out_Vtile_noext+'.prj')) == False or (os.path.exists(out_Vref_noext +'.shp') and os.path.exists(out_Vref_noext+'.dbf') and os.path.exists(out_Vref_noext+'.shx') and os.path.exists(out_Vref_noext+'.prj')) == False:
		
			extractPatch(testimg, out_RTile, startx, starty, size, classNum, classCloud)  # --> tile_prod (raster)
			rasterize(refVect, out_RTile, out_Rref, 1)  # --> tile_test (raster)	
			rasterize(in_vector_training_patches, out_RTile, out_Rmask, 255)  # --> tile_mask (raster)

			app_bandMath = otbApplication.Registry.CreateApplication("BandMath")
			app_bandMath.SetParameterStringList("il", [out_Rmask, out_RTile])  # --> en entrée : tile_mask et tile_prod
			app_bandMath.SetParameterString("out", '{}?&gdal:co:COMPRESS=DEFLATE'.format(out_RTile_mask))   # --> sortie : tile_prod masked
			app_bandMath.SetParameterString("exp", "im1b1==255?255:im2b1")	# --> if pxl value mask = 255 : 255 otherwise : pxl value tile_prod (1 or 0)
			app_bandMath.SetParameterOutputImagePixelType("out", 0)
			app_bandMath.ExecuteAndWriteOutput()
			
			app_bandMath = otbApplication.Registry.CreateApplication("BandMath")
			app_bandMath.SetParameterStringList("il", [out_Rmask, out_Rref, out_RTile])  # --> en entrée : tile_mask et tile_test
			app_bandMath.SetParameterString("out", '{}?&gdal:co:COMPRESS=DEFLATE'.format(out_Rref_mask))   # --> sortie : tile_test masked
			app_bandMath.SetParameterString("exp", "im1b1==255 || im3b1 == 255?255:im2b1") # if pxl value mask = 255 or prod = 255 : 255 otherwise : pxl value tile_test (1 or 0)
			app_bandMath.SetParameterOutputImagePixelType("out", 0)
			app_bandMath.ExecuteAndWriteOutput()
			
			app_ConfMatrix = otbApplication.Registry.CreateApplication("ComputeConfusionMatrix")  # --> Matrice de confusion pour chaque tuile de prod/test (pixel)
			app_ConfMatrix.SetParameterString("in", out_RTile_mask)
			app_ConfMatrix.SetParameterString("out", os.path.join(outputD, "tile{}_{}_ConfMat.csv".format(startx, starty)))
			app_ConfMatrix.SetParameterString("ref.raster.in", out_Rref_mask)
			app_ConfMatrix.SetParameterInt("ref.raster.nodata", 255)
			app_ConfMatrix.ExecuteAndWriteOutput()
			
			polygonize(out_RTile_mask, out_VTile)   # --> vecteur tile_prod masked
			polygonize(out_Rref_mask, out_Vref)   # --> vecteur tile_test masked
			
			# os.remove(out_RTile)
			# os.remove(out_Rref)
			# os.remove(out_Rmask)
			# os.remove(out_RTile_mask)
			# os.remove(out_Rref_mask)
		else :
			print('=======\nok done\n========')