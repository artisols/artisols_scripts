import os, sys
import subprocess

# argv01: tiles directory path
# argv02: pgsql database name

# --- main ---

if __name__ == "__main__":

    patch_path = sys.argv[1]
    datab_name = sys.argv[2]
    # /home/kenji/WORK/ARTISOLS/OUTPUT_VALID_TILES/MAP_TILES_2019

    for path, subdirs, files in os.walk(patch_path):
      for name in files:
        splitname = os.path.splitext(name)
        if splitname[1] == '.shp':
            print("--> integration de {} dans la base psql".format(name))
            ff = os.path.join(path, name)
            tablename = splitname[0]
            cmd = "shp2pgsql -I -s 2154 {0} public.{1}| PGPASSWORD=admin psql -q -h 10.34.192.163 -p 5432 -d {2} -U postgres".format(ff, tablename, datab_name)
            subprocess.call(cmd, shell=True)