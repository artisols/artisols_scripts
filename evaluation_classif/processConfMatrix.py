# Crée un fichier .txt : remet en forme la matrice de confusion générée en .csv (extract_patch_prod_rev_4pxlEval.py) et calcule les métriques associées

from pathlib import Path
import sys
import numpy as np
import os

def precision(label, confusion_matrix):
	col = confusion_matrix[:, label]
	return confusion_matrix[label, label] / col.sum()

def recall(label, confusion_matrix):
	row = confusion_matrix[label, :]
	return confusion_matrix[label, label] / row.sum()

def precision_macro_average(confusion_matrix):
	rows, columns = confusion_matrix.shape
	sum_of_precisions = 0
	for label in range(rows):
		sum_of_precisions += precision(label, confusion_matrix)
	return sum_of_precisions / rows

def recall_macro_average(confusion_matrix):
	rows, columns = confusion_matrix.shape
	sum_of_recalls = 0
	for label in range(columns):
		sum_of_recalls += recall(label, confusion_matrix)
	return sum_of_recalls / columns

def accuracy(confusion_matrix):
	diagonal_sum = confusion_matrix.trace()
	sum_of_all_elements = confusion_matrix.sum()
	return diagonal_sum / sum_of_all_elements

def MatrixList(inputMat):
	""" List of tiles in pixel units
	@param inputImg: <string> classification image path
	@param size:	 <int>	size of tile (square) (in pixels)
	@param step:	 <int>	research step (in pixels)
	@return :		<list>   list of tiles with [startX, starty, size]
	"""
	raster_ds = gdal.Open(inputImg)
	cols = raster_ds.RasterXSize
	#cols = 325921
	rows = raster_ds.RasterYSize
	#rows = 219402
	raster_ds = None
	list_startX = list()
	list_startY = list()
	startX = 1
	startY = 1
	while startX < (cols-size):
		list_startX.append(startX)
		startX = startX + size + step
	while startY < (rows-size):
		list_startY.append(startY)
		startY = startY + size + step
	final_list = list()
	for x in list_startX:
		for y in list_startY:
			bloc = [x, y, size]
			final_list.append(bloc)
	return final_list

def CreateFile (matrix, output, path_dir) :

	#Creation of the file with the matrix
	file = open('{}.txt'.format(output), 'w')
	
	csvpath = str(Path(os.path.join(path_dir,matrix)))
	print(csvpath)
	file.write('=============================\n')
	file.write ('csv file: {}\n'.format(csvpath))

	# Search matrix confusion headers
	with open(csvpath, 'r') as csv:
		for ln in csv:
			if ln.startswith('#Reference'):
				lab_refe = ln.split(':')[1].strip().split(',')
			if ln.startswith('#Produced'):
				lab_prod = ln.split(':')[1].strip().split(',')
	file.write('===\n')
	file.write ('reference labels: {}\n'.format(lab_refe))
	file.write ('produced labels: {}\n'.format(lab_prod))

	# Load matrix_confusion
	# Check confusion matrix dimension and load matrix confusion
	cm = np.genfromtxt(csvpath, delimiter=',')
	file.write('===\n')
	file.write ('native confusion matrix\n')
	
	if len(lab_refe) == len(lab_prod):	# If the ref as many classes as the prod
		file.write (str(cm))
	
	if len(lab_refe) == 1 and len(lab_prod) == 1 and lab_prod[0] == '0' and lab_refe[0] == '0' :
		file.write('===\n')
		file.write ('new matrix confusion\n')
		nc = np.array([0])
		cm = np.column_stack((cm, nc))
		nr = np.array([0,0])
		cm = np.vstack((cm, nr))
		file.write (str(cm))
		
		
	if len(lab_refe) < len(lab_prod) and lab_prod[0] == '0':	# If the ref as less classes than the prod (no buildings in the ref (class 0) whereas there are in the prod (classes 0 and 1)
		file.write('===\n')
		file.write ('new matrix confusion\n')
		cm = cm[:,1:]
		file.write (str(cm))
		
	if len(lab_refe) == 2 and len(lab_prod) == 1 and lab_prod[0] == '0':	# If the ref as more classes than the prod (no buildings in the prod (class 0) whereas there are in the ref (classes 0 and 1)
		file.write('===\n')
		file.write ('new matrix confusion\n')
		cm = np.reshape(cm,(-1,1))
		nc = np.array([0,0])
		cm = np.column_stack((cm, nc))
		file.write (str(cm))

	# Evaluation
	file.write('\n===\n')
	file.write("label    | precision      | recall     | f-score\n")
	file.write("------------------------------------------------\n")
	for label in range(len(lab_refe)):
		file.write("{}	 | {:.6f}	  | {:.6f}   | {:.6f}\n".format(lab_refe[label],(precision(label, cm)),
														  recall(label, cm),
														  2*precision(label, cm)*recall(label, cm)/(precision(label, cm)+recall(label, cm))))
	file.write('===\n')
	file.write("precision total:  {:.6f}\n".format(precision_macro_average(cm)))
	file.write("recall total:	 {:.6f}\n".format(recall_macro_average(cm)))
	file.write("overall accuracy: {:.6f}\n".format(accuracy(cm)))

	file.close()
	
# =====================================================================================================
# =====================================================================================================

if __name__ == "__main__": 

	# argv[1]: confmat csv path
	path_dir = sys.argv[1]
	
	listMatrix = os.listdir(path_dir)
	for matrix in listMatrix:
		if ".csv" in matrix : 
			id_table = matrix.split('_ConfMat.csv')[0]
			print(id_table)
			output = os.path.join(path_dir, "{}_matrix".format(id_table))
			CreateFile (matrix, output, path_dir)