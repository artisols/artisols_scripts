from pathlib import Path
from sys import argv
import numpy as np

def precision(label, confusion_matrix):
    col = confusion_matrix[:, label]
    return confusion_matrix[label, label] / col.sum()
    
def recall(label, confusion_matrix):
    row = confusion_matrix[label, :]
    return confusion_matrix[label, label] / row.sum()

def precision_macro_average(confusion_matrix):
    rows, columns = confusion_matrix.shape
    sum_of_precisions = 0
    for label in range(rows):
        sum_of_precisions += precision(label, confusion_matrix)
    return sum_of_precisions / rows

def recall_macro_average(confusion_matrix):
    rows, columns = confusion_matrix.shape
    sum_of_recalls = 0
    for label in range(columns):
        sum_of_recalls += recall(label, confusion_matrix)
    return sum_of_recalls / columns

def accuracy(confusion_matrix):
    diagonal_sum = confusion_matrix.trace()
    sum_of_all_elements = confusion_matrix.sum()
    return diagonal_sum / sum_of_all_elements

# =====================================================================================================
# =====================================================================================================

if __name__ == "__main__": 

    # argv[1]: confmat csv path
    
    csvpath = str(Path(argv[1]))
    print('=============================')
    print ('csv file: {}'.format(csvpath))

    # Search matrix confusion headers
    with open(csvpath, 'r') as csv:
        for ln in csv:
            if ln.startswith('#Reference'):
                lab_refe = ln.split(':')[1].strip().split(',')
            if ln.startswith('#Produced'):
                lab_prod = ln.split(':')[1].strip().split(',')
    print('===')
    print ('reference labels: {}'.format(lab_refe))
    print ('produced labels: {}'.format(lab_prod))

    # Check confusion matrix dimension and load matrix confusion
    cm = np.genfromtxt(csvpath, delimiter=',')
    print('===')
    print ('native confusion matrix')
    
    if len(lab_refe) == len(lab_prod):	# If the ref as many classes as the prod
        print ('cm_test', cm)    
    
    if len(lab_refe) < len(lab_prod) and lab_prod[0] == '0':	# If the ref as less classes than the prod (no buildings in the ref (class 0) whereas there are in the prod (classes 0 and 1)
        print('===')
        print ('new matrix confusion')
        cm = cm[:,1:]
        print ('cm_test', cm)
	    
    if len(lab_refe) != len(lab_prod):
        misClass = list(set(lab_refe)- set(lab_prod))[0]
        print("Missing class : ", misClass)
        index = lab_refe.index(misClass)
        print("index of the missing class: ", index)
        cm_test = np.insert (cm, index, 0, axis=1)
        print('cm_test', cm_test)        
        
        # Evaluation
        print('===')
        print("label | precision | recall | f-score")
        print("------------------------------------")
        for label in range(len(lab_refe)):
            print("{}     | {:.2f}      | {:.2f}   | {:.2f}".format(lab_refe[label], precision(label, cm_test),
                                                              recall(label, cm_test),
                                                              2*precision(label, cm_test)*recall(label, cm_test)/(precision(label, cm_test)+recall(label, cm_test))))
        print('===')
        print("precision total:  {:.3f}".format(precision_macro_average(cm_test)))
        print("recall total:     {:.3f}".format(recall_macro_average(cm_test)))
        print("overall accuracy: {:.3f}".format(accuracy(cm_test)))
    
    else : 
    # Evaluation
        print('===')
        print("label | precision | recall | f-score")
        print("------------------------------------")
        for label in range(len(lab_refe)):
            print("{}     | {:.2f}      | {:.2f}   | {:.2f}".format(lab_refe[label], precision(label, cm),
                                                              recall(label, cm),
                                                              2*precision(label, cm)*recall(label, cm)/(precision(label, cm)+recall(label, cm))))
        print('===')
        print("precision total:  {:.3f}".format(precision_macro_average(cm)))
        print("recall total:     {:.3f}".format(recall_macro_average(cm)))
        print("overall accuracy: {:.3f}".format(accuracy(cm)))
