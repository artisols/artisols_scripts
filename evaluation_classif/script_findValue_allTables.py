# Ce script trouve la surface maximale des entités test dans l'ensemble des tables de la base de données

import sys, os, time
import psycopg2

def listTables(dbconnection):
    """list of both tables prod and test with same tile id
    @param dbconnection <class object> psycopg2 connection instance
    @return list of lists as [tile_id, table_test, table_prod]
    """
    sqlquer = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'"
    cursor = dbconnection.cursor()
    cursor.execute(sqlquer)
    list_tables = cursor.fetchall()
    list_clean = [i[0] for i in list_tables if 'bati_iou' in i[0] and 'bati_iou_' not in i[0] ]
    
    dual_list = list()
    for table in list_clean :
        dual_list.append(table)

    return dual_list

def search(dbconnection, table_name):
    """list of both tables prod and test with same tile id
    @param dbconnection <class object> psycopg2 connection instance
    @return list of lists as [tile_id, table_test, table_prod]
    """
    sqlquer = "SELECT MAX(test_surf) FROM {}".format(table_name)
    #print(sqlquer)
    cursor = dbconnection.cursor()
    cursor.execute(sqlquer)
    list_max = cursor.fetchall()	

    count_max = list()
    for nb in list_max :
        count_max.append(nb)

    return count_max
# --- main ---

if __name__ == "__main__":
    # get sql queries directory
    dbname = sys.argv[1]

    # connect database
    try:
        connection = psycopg2.connect(user = "postgres", password = "admin", host = "10.34.192.163", port = "5432", database = dbname)
    except:
        print('probleme de connexion a la database postgres')
    
    list_table = listTables(connection)

    list_max_surf = []

    for tile in list_table:
        max = search(connection, tile)
        for nb in max :
            nb_tile = nb[0]
            print('tile_test : ', tile, 'surf_max: ', nb_tile)
            # list_max_surf.append(nb_tile)
    # max = max(list_max_surf)
    # print(max)


