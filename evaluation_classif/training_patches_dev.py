import os, sys
import ogr, gdal

def listPatchvector(inputDir):
	listPatch = list()
	for path, subdirs, files in os.walk(inputDir):
		for name in files:
			if 'samples_loc_A' in name and '.shp' in name:
				listPatch.append(os.path.join(path, name))
	return listPatch

def pt2polyPatch(listPtPatch, outputDir):
	spatialRef = ogr.osr.SpatialReference()
	spatialRef.ImportFromEPSG(2154)
	
	outputFilesList = list()
	id_patch = 0
	for shp_train in listPtPatch:
		id_patch += 1
		print('traitement du shp : {}'.format(shp_train))
		basename = os.path.splitext(os.path.basename(shp_train))[0]
		shp_polyTrain = os.path.join(outputDir, '{}_poly_{}.shp'.format(basename, id_patch))
		outputFilesList.append(shp_polyTrain)
		driver = ogr.GetDriverByName('ESRI Shapefile')
		train_ds = driver.Open(shp_train, 0)
		#open input data source and get the layer
		if train_ds is None:
			print('Could nor open {}'.format(shp_train))
			sys.exit(1)
		train_layer = train_ds.GetLayer(0)
		
		#create a new data source and layer
		if os.path.exists(shp_polyTrain):
			driver.DeleteDataSource(shp_polyTrain)
		poly_ds = driver.CreateDataSource(shp_polyTrain)
		if poly_ds is None:
			print('Could not create file')
			sys.exit(1)
		poly_layer = poly_ds.CreateLayer('{}_poly'.format(basename), spatialRef, geom_type=ogr.wkbPolygon)
		fieldDefn = ogr.FieldDefn('id', ogr.OFTInteger)
		poly_layer.CreateField(fieldDefn)
		
		cnt = 0
		inTrainFeat = train_layer.GetNextFeature()
		while inTrainFeat:
			geom = inTrainFeat.GetGeometryRef()
			
			x = geom.GetX()
			y = geom.GetY()
			
			ulx = x-32*1.5
			uly = y+32*1.5
			lrx = x+32*1.5
			lry = y-32*1.5
			
			ring = ogr.Geometry(ogr.wkbLinearRing)
			ring.AddPoint(ulx, uly)
			ring.AddPoint(lrx, uly)
			ring.AddPoint(lrx, lry)
			ring.AddPoint(ulx, lry)
			ring.CloseRings()
			
			polygon = ogr.Geometry(ogr.wkbPolygon)
			polygon.AddGeometry(ring)
			
			#get the FeatureDefn for the output layer
			featureDefn = poly_layer.GetLayerDefn()
			#create a new feature
			feature = ogr.Feature(featureDefn)
			feature.SetGeometry(polygon)
			feature.SetField('id', cnt)
			#add the feature to the output layer
			poly_layer.CreateFeature(feature)
			inTrainFeat = train_layer.GetNextFeature()
			cnt += 1
		train_ds = None
		poly_ds = None
	return outputFilesList
	
def mergeLayers(inFilesList, outMergeShp):
	spatialRef = ogr.osr.SpatialReference()
	spatialRef.ImportFromEPSG(2154)

	basename = os.path.splitext(os.path.basename(outMergeShp))[0]
	driver = ogr.GetDriverByName('ESRI Shapefile')
	if os.path.exists(outMergeShp):
		driver.DeleteDataSource(outMergeShp)
	
	out_ds = driver.CreateDataSource(outMergeShp)
	out_layer = out_ds.CreateLayer(basename, spatialRef, geom_type=ogr.wkbPolygon)

	for file in inFilesList:
		ds = ogr.Open(file)
		lyr = ds.GetLayer()
		for feat in lyr:
			out_feat = ogr.Feature(out_layer.GetLayerDefn())
			out_feat.SetGeometry(feat.GetGeometryRef().Clone())
			out_layer.CreateFeature(out_feat)
			out_feat = None
			out_layer.SyncToDisk()

#--- main ---

if __name__ == "__main__":

	inputDir = sys.argv[1]
	# ex. /home/kenji/WORK/ARTISOLS/DATA_REMI/cartes/2019
	outputDir = sys.argv[2]
	# ex. /home/kenji/WORK/ARTISOLS/OUTPUT_POLY_PATCHES/POLY_PATCHES_2019
	outMergeFile = sys.argv[3]
	# ex. /home/kenji/WORK/ARTISOLS/OUTPUT_POLY_PATCHES/POLY_PATCHES_2019/merge_poly_patches_2019.shp
	listPtPatches = listPatchvector(inputDir)
	outList = pt2polyPatch(listPtPatches, outputDir)
	mergeLayers(outList, outMergeFile)