# _*_ coding:utf-8 _*_

from qgis import processing
from qgis.processing import alg
from qgis.core import QgsProject, QgsRasterLayer, QgsVectorLayer
import gdal, ogr, osr
import gdalconst
import os

# RASTERIZE FUNCTION

def createEmptyRaster(inImg, output, nodata):
    """
    create one band raster filled with nodata value according to a reference raster
    """
    # open reference raster file
    dsRaster = gdal.Open(inImg, gdalconst.GA_ReadOnly)
    
    # get raster info
    proj = dsRaster.GetProjection()
    geot = dsRaster.GetGeoTransform()
    cols = dsRaster.RasterXSize
    rows = dsRaster.RasterYSize

    # output raster
    outRaster = gdal.GetDriverByName('GTiff').Create(output, cols, rows, 1, gdal.GDT_Byte)
    outRaster.SetGeoTransform(geot)
    outRaster.SetProjection(proj)
    outBand = outRaster.GetRasterBand(1)
    outBand.SetNoDataValue(nodata)
    outBand.FlushCache()
    
    return outRaster

def burnRaster(inVector, inImg, fieldname, refImg = False):
    """
    burn an existing raster with vector attribute values
    """
    # check if inImg exists
    if not os.path.exists(inImg):
        if refImg == False:
            #print("ERROR: raster file does not exist, add a reference raster file")
            sys.exit(1)
    # create raster file or open existing raster file
        if refImg:
            nodata = 0
            dsRaster = createEmptyRaster(refImg, inImg, nodata)
    else:
        dsRaster = gdal.Open(inImg, gdalconst.GA_Update)

    # open vector file
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dsVector = gdal.OpenEx(inVector, gdal.OF_VECTOR)

    # burn value
    options = gdal.RasterizeOptions(bands=[1], attribute=fieldname)
    gdal.Rasterize(dsRaster, dsVector, options=options)
    
    # close files
    dsRaster = None
    dsVector = None

# CREATION OF A LIST OF VECTOR LAYERS (PER DEPARTMENT) THAT INTERSECT THE SATELLITE IMAGES USED TO RASTERIZE THE VECTOR DATA

def extent(img):
    """
    Get the satellite images extent
    """ 
    layerSat = QgsRasterLayer(img, 'imgSat')
    extentRaster = layerSat.extent()
    return extentRaster

def createList (dirData, listDpt, layerList): #, output):
    """
    Create a list of the paths of the vector layers per department used during the processing
    """
    pathList = list()
    
    for couche in layerList: 
        for num in listDpt:
            for root, reps, files in os.walk(dirData):
                for file in files:
                    path_layer = os.path.join(root, file)
                    # Get the department number (RXX) written in the folder name
                    numDpt = 'D0{}'.format(num)
                    if numDpt in path_layer and couche == file :
                        pathList.append(path_layer)                       
    return pathList
    
def dptImg(extentImg, path_dpt):
    """
    Check which departments intersect the satellite images used and write a list of the department numbers
    """
    selected_fid = []
    # Get the features of the department layer
    vlayerdpt = QgsVectorLayer(path_dpt, 'dpt', 'ogr')
    featDpt = vlayerdpt.getFeatures()
    for fd in featDpt:
    # Get the geometry of the features 
        geom = fd.geometry()
    # Check if the geometry of the features intersects the satellite image extent
        if geom.intersects(extentImg):  
    # Write in a list the department numbers (values in this field)  
            selected_fid.append(fd.attribute('code_insee'))
    # Delete the duplicates of the list
    listeDpt=list(set(selected_fid))
    return listeDpt
    
#--- MAIN FUNCTION ---

def AutoRasterize(INPUT_SatImg, INPUT_BDOCS, INPUT_VectorDpt, listVectorLayer, OUTPUT, feedback):    
    images = list()
    for root, reps, files in os.walk(INPUT_SatImg):
        for file in files :
            if os.path.splitext(file)[1] == '.TIF' and 'MS' in root:
                images.append(os.path.join(root, file))

    for img in images:
        feedback.pushInfo("IMAGE SAT="+img)
        basename = os.path.basename(img)
        outname = 'OCS_{}'.format(basename)
        output = os.path.join(OUTPUT, outname)
        feedback.pushInfo("OUTPUT =" + str(output))
        extentImg = extent(img)
        listDpt = dptImg(extentImg, INPUT_VectorDpt)
        pathList = createList(INPUT_BDOCS, listDpt, listVectorLayer)
        for ocs in pathList:
            feedback.pushInfo("OCS = " + ocs)
            burnRaster(ocs, output, 'CLASSE', img)
    return ()

# --------   PARAMETERS ---------

@alg(name = 'OCS_Rasterize', label = 'OCS_Rasterize', group='Scripts_Artisols', group_label='Scripts_Artisols')
@alg.input(type=alg.FILE, name = 'INPUT_SatImg', label = 'Input folder of satellite images', behavior = 1, default ='D:\Donnees\Images_Sat' )
@alg.input(type=alg.FILE, name = 'INPUT_BDOCS', label = 'Input folder of vector layers', behavior = 1, default = 'D:\Donnees\output_donnees\BD_OCS')
@alg.input(type=alg.SOURCE, name = 'INPUT_VectorLayerDpt', label = 'Input vector layer of department boundaries', default = 'D:\Donnees\departements\dpt_occitanie.shp')
@alg.input(type=alg.STRING, name = 'listVectorLayer', label = 'Name of the vector layers (in the order they will be rasterize)', multiLine = True, default = 'EAU.SHP\nMARAICHAGE.SHP\nVIGNE.SHP\nGRANDE_CULTURE.SHP\nVERGER.SHP\nCARRIERES.SHP\nSPORT_IMPERMEABLE.SHP\nAERODROME.SHP\n')
@alg.input(type=alg.FOLDER_DEST, name = 'FOLDER_OUTPUT', label = 'Folder output', default = 'D:\Donnees\Sortie_test\Raster')

def execution(instance, parameters, context, feedback, inputs):
    """
    The function allows to rasterize vector layers of cover lands per department that intersect the satellite images processed
    The processing is executed according to the following steps:
    - Create a list of multispectral images contained in the folder
    - Get the extent of the images
    - Check the departments (from a vector layer) that intersect the images
    - Create a list of the paths of the vector layers per department intersected depending on the order they will be rasterize 
    """    
    INPUT_SatImg = parameters['INPUT_SatImg']
    INPUT_BDOCS = parameters['INPUT_BDOCS']
    INPUT_VectorDpt = parameters['INPUT_VectorLayerDpt']
    listVectorLayer = (parameters['listVectorLayer'].split('\n'))
    OUTPUT = parameters['FOLDER_OUTPUT']
    
    AutoRasterize(INPUT_SatImg, INPUT_BDOCS, INPUT_VectorDpt, listVectorLayer, OUTPUT, feedback) 