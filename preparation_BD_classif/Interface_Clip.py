import processing
from qgis.processing import alg
import os

def verifValid(rpg):
    """
    Check the geometry of a vector layer. Three vector layers are generated according to 3 groups
    (valid, invalid and error). The vector layer with valid geometries is extracted for the next step of the processing
    """  
    alg_params = { 
            'INPUT_LAYER' : rpg, 
            'METHOD' : 2, 
            'VALID_OUTPUT' : 'TEMPORARY_OUTPUT', 
            'INVALID_OUTPUT': 'TEMPORARY_OUTPUT', 
            'ERROR_OUTPUT' : 'TEMPORARY_OUTPUT'
        }
    verifLayer = processing.run('qgis:checkvalidity', alg_params)
    return verifLayer

def extractByAttr(path, field, listvalue):
    """
    Selection by attribute based on the values of the field class from the input vector layer 
    """
    # Query creation
    if len(listvalue) == 1 :
        value = listvalue[0]
        query = "{} = '{}'".format(field, value)
    else :
        value = listvalue[0]
        query = "{} = '{}'".format(field, value)
        for value in listvalue[1:] :
            query = "{} OR {} = '{}'".format(query,field, value)
    
    # Select by attribute according to the query 
    alg_params = {
            'INPUT': path,
            'EXPRESSION': query,
            'OUTPUT':'TEMPORARY_OUTPUT'
        }
    vectSelect = processing.run('native:extractbyexpression', alg_params)
    return vectSelect

def clip(path, dpt, output):
    """
    Clip a vector layer matching with a department selected in the vector layer of department limits
    """
    alg_params = {
            'INPUT': path,
            'OVERLAY': dpt,
            'OUTPUT': output
        }
   
    vectSelect = processing.run('qgis:clip', alg_params)
    return vectSelect
    
#----- MAIN FUNCTION ------

def clipLayer(outputdir, dir_data, layer_name, field_dpt, dpt_path, num):
    for root, dirs, files in os.walk(os.path.join(dir_data, num)):
        for file in files :
            if layer_name in file :
                # Select by attribute one department every loop in the departmental boundaries layer 
                layerDpt = extractByAttr(dpt_path, field_dpt, [num])
                # Creation of the output path
                path_file = os.path.join(root,file)
                basename = os.path.splitext(os.path.split(file)[1])[0]
                path_clip = os.path.join(outputdir, num, '{}.shp'.format(basename))
                # Check the validity on the geometries of the land cover layer 
                layerValid = verifValid(path_file)
                # Clip the land cover layer with the department
                layerRpgDpt = clip(layerValid['VALID_OUTPUT'], layerDpt['OUTPUT'], path_clip)

# --------   PARAMETERS ---------

@alg(name = 'Clip vector layers (land cover) with department boundaries', label = 'MultiClip OCS/department', group='Scripts_Artisols', group_label='Scripts_Artisols')
@alg.input(type=alg.FILE, name = 'INPUT_layersFolder', label = 'Input folder for the land cover vector layers (e.g BD Topo)', behavior = 1)
@alg.input(type=alg.SOURCE, name = 'INPUT_VectorLayerDpt', label = 'Input vector layer of department boundaries')

@alg.input(type=alg.STRING, name = 'fileExtension', label = 'Extension of the layer', default = '.SHP')

@alg.input(type=alg.FIELD, name = 'field_DPT', label = 'Field of the department number', parentLayerParameterName = 'INPUT_VectorLayerDpt')
@alg.input(type=alg.STRING, name = 'numberList_Dpt', label = 'List of the department numbers', multiLine = True)


@alg.input(type=alg.FOLDER_DEST, name = 'FOLDER_OUTPUT', label = 'Folder output')


# --------   EXECUTION ----------

def execution(instance, parameters, context, feedback, inputs):
    """
    The function allows to clip vector layers (land cover layer for instance) in a folder with a layer of administrative limits
    """

    INPUT_layersFolder = parameters['INPUT_layersFolder']
    INPUT_VectorLayerDpt = parameters['INPUT_VectorLayerDpt']
    fileExtension = parameters['fileExtension']
    field_DPT = parameters['field_DPT']
    numberList_Dpt = (parameters['numberList_Dpt'].split('\n'))
    FOLDER_OUTPUT = parameters['FOLDER_OUTPUT']
    
    for num in numberList_Dpt :
        clipLayer(FOLDER_OUTPUT, INPUT_layersFolder, fileExtension, field_DPT, INPUT_VectorLayerDpt, num)
