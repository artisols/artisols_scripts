from qgis import processing
from qgis.processing import alg
from qgis.core import QgsProject
import os
from qgis.core import *
from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.gui import QgsMessageBar
from qgis.utils import iface


def extractByAttr(path, field, ValueList):
    """
    Selection by attribute based on the values of the field class from the input vector layer 
    """
    # Query creation
    if len(ValueList) == 1 :
        value = ValueList[0]
        query = "{} = '{}'".format(field, value)
    else :
        value = ValueList[0]
        query = "{} = '{}'".format(field, value)
        for value in ValueList[1:] :
            query = "{} OR {} = '{}'".format(query, field, value)
    
    # Select by attribute according to the query 
    alg_params = {
            'INPUT': path,
            'EXPRESSION': query,
            'OUTPUT':'TEMPORARY_OUTPUT'
        }
    vectSelect = processing.run('native:extractbyexpression', alg_params)
    return vectSelect
    
    
def verifValid(rpg, ValidGeomLayer):
    """
    Check the geometry of a vector layer. Three vector layers are generated according to 3 groups
    (valid, invalid and error). The vector layer with valid geometries is extracted for the next step of the processing
    """  
    alg_params = { 
            'INPUT_LAYER' : rpg, 
            'METHOD' : 2, 
            'VALID_OUTPUT' : ValidGeomLayer, 
            'INVALID_OUTPUT': 'TEMPORARY_OUTPUT', 
            'ERROR_OUTPUT' : 'TEMPORARY_OUTPUT'
        }
    verifLayer = processing.run('qgis:checkvalidity', alg_params)
    return verifLayer

    
def extractByLoc(path, dpt):
    """
    Extract features from a vector layer with regional administrative boundaries that match with an input 
    vector layer of departmental administrative limits
    """    
    alg_params = {
            'INPUT': path,
            'PREDICATE': [0],
            'INTERSECT': dpt,
            'OUTPUT': 'TEMPORARY_OUTPUT'
        }
   
    vectSelect = processing.run('native:extractbylocation', alg_params)
    return vectSelect

def layerMerge(layer1, layer2, output):
    """
    Combine two vector layers into a single one 
    Now, a single layer combines classes/features from two different sources and scales
    """
    layerList = [layer1, layer2]
    alg_params = {
            'LAYERS': layerList, 
            'OUTPUT': output}
    processing.run('native:mergevectorlayers', alg_params)
    
def fields(output, Field1, Field2, Field1_Value, Field2_Value):
    """
    Create new fields and assigns a class index (number) and a priority that will
    be reused as a burn-in value to rasterize the vector layers
    """
    # Fields creation
    out_file= QgsVectorLayer(output, '', 'ogr')
    caps = out_file.dataProvider().capabilities()
    if caps and QgsVectorDataProvider.addAttributes :
        newField1 = QgsField(Field1, QVariant.Int)
        newField2 = QgsField(Field2, QVariant.String)
        out_file.dataProvider().addAttributes([newField1, newField2])
        out_file.updateFields()
        
    idFieldPrior = out_file.dataProvider().fieldNameIndex(Field1)
    idFieldClass = out_file.dataProvider().fieldNameIndex(Field2)
    
    # Fields modification
    out_file.startEditing()
    for feat in out_file.getFeatures(): 
        out_file.changeAttributeValue(feat.id(), idFieldPrior, Field1_Value)
        out_file.changeAttributeValue(feat.id(), idFieldClass, Field2_Value)
    out_file.commitChanges()
    
#----- MAIN FUNCTION -----------

def ExtractMerge (outputdir, rpg_path, field_rpg, fieldRpgvalue, dir_data, layer_name, field_vgt, fieldVgtvalue, 
        field_dpt, dpt_path, Field1, Field2, Field1_Value, Field2_Value, outputName, ValidGeomLayer, feedback):
    """
    The main function allows to chain the previous functions and to loop it on a whole folder
    """  
    #  ----- Prepare the progress bar (7 steps in the current processing)
    stepFeedback = QgsProcessingMultiStepFeedback(7, feedback)
    feedback.setProgress(0)
    
    # ----- Select by attribute in the regional layer (e.g. RPG)
    coucheRpg = extractByAttr(rpg_path , field_rpg, fieldRpgvalue)
    feedback.setProgress(12)
    
    # ----- Check validity on the geometries of the regional layer 
    validRpg = verifValid(coucheRpg['OUTPUT'], ValidGeomLayer)
    feedback.setProgress(24)

    # ----- Count the number of files processed(vector layers) in the folder to start the process bar
    nb_files = 0
    for root, dirs, files in os.walk(dir_data):
        for file in files :
           if layer_name in file :
                nb_files += 1
    feedback.pushInfo("nb_files = " + str(nb_files))

    stepFeedback = QgsProcessingMultiStepFeedback(nb_files, feedback)
    
    # ----- Set a counter to reference the progress
    cpt = 0
    
    # ----- Loop in a folder that contains the vector layers for several departments
    for root, dirs, files in os.walk(dir_data):
        for file in files :
           if layer_name in file :
               # Search the department number in the name of the folders
               numDpt = os.path.split(os.path.split(root)[0])[1].split('_')[1]
               
               # Select by attribute the features of an expected class in the "departmental" layer (e.g. BD TOPO)
               path_topo = os.path.join(root,file)
               feedback.pushInfo(path_topo)
               coucheVgt = extractByAttr(path_topo, field_vgt, fieldVgtvalue)
               
               # Select by attribute one department every loop in the departmental boundaries layer 
               coucheDpt = extractByAttr(dpt_path, field_dpt, [numDpt])
                              
               # Extract the features of the regional layer (RPG) matching with the department selected
               coucheRpgDpt = extractByLoc(validRpg['VALID_OUTPUT'], coucheDpt['OUTPUT'])

               # Merge the features from the BD Topo (departmental layer) and the one extracted from the regional layer (RPG) by department
               path_fusion = os.path.join(outputdir, numDpt, "{}{}.shp".format(outputName, numDpt))
               layerMerge(coucheVgt['OUTPUT'], coucheRpgDpt['OUTPUT'],path_fusion)

               # Create the new fields (e.g. 'Class' and 'Priority') and assigns values (class index and priority number)
               fields(path_fusion, Field1, Field2, Field1_Value, Field2_Value)
               
               # Update the progress bar
               cpt += 1
               stepFeedback.setCurrentStep(cpt)
    return     path_fusion

# --------   PARAMETERS ---------

@alg(name = 'Selection by attribute in multisources vector layers and merge into a single one', label = 'Extraction and Merge multisource', group='Scripts_Artisols', group_label='Scripts_Artisols')
@alg.input(type=alg.SOURCE, name = 'INPUT_RegionalVectorLayer', label = 'Input regional vector layer (e.g RPG)')
@alg.input(type=alg.FIELD, name = 'field_RegionalLayer', label = 'Field of the regional layer', parentLayerParameterName = 'INPUT_RegionalVectorLayer')
@alg.input(type=alg.STRING, name = 'ValuesList_RegLayer', label = 'Values in the field of the regional layer', multiLine = True)
@alg.input(type=alg.FILE_DEST, name = 'ValidGeomLayer', label = 'Name for the valid vector layer', default = 'validRPG.shp')

@alg.input(type=alg.FILE, name = 'INPUT_DptLayersFolder', label = 'Input folder of the departmental vector layers (e.g BD Topo)', behavior = 1)
@alg.input(type=alg.STRING, name = 'Vector_Layer_Name', label = 'Departmental vector layer name', default = 'ZONE_VEGETATION.SHP')
@alg.input(type=alg.STRING, name = 'Field_DptLayer', label = 'Field of the departmental layer', default = 'NATURE')
@alg.input(type=alg.STRING, name = 'ValuesList_DptLayer', label = 'Values in the field of the departmental layer', multiLine = True)

@alg.input(type=alg.SOURCE, name = 'INPUT_VectorLayerDpt', label = 'Input vector layer of department boundaries')
@alg.input(type=alg.FIELD, name = 'field_DPT', label = 'Field of the department number', parentLayerParameterName = 'INPUT_VectorLayerDpt')

@alg.input(type=alg.STRING, name = 'Field1', label = 'Field 1', default = 'PRIORITY')
@alg.input(type=alg.NUMBER, name = 'Field1_Value', label = 'Value Field 1')

@alg.input(type=alg.STRING, name = 'Field2', label = 'Field 2', default = 'CLASS')
@alg.input(type=alg.STRING, name = 'Field2_Value', label = 'Value Field 2')

@alg.input(type=alg.FOLDER_DEST, name = 'FOLDER_OUTPUT', label = 'Folder output')
@alg.input(type=alg.STRING, name = 'name_OUTPUT', label = 'Output name of the merged layer', default = 'Verger')

# --------   EXECUTION ---------

def execution(instance, parameters, context, feedback, inputs):
    """
    The function allows to select features from different classes and layers in order to combine it into a single vector layer. The input layers may be from different sources and administrative limits.
    The processing is executed according to the following steps:
    - Selection by attribute of one or several classes into vector layers
    - Check of the geometry of the vector layer and extraction of the new layer with valid geometries. The latter will be reused in the next steps of the processing
    - Selection by attribute of one department every loop in the departmental boundaries layer
    - Extraction by location of the features from a regional scale layer matching with the department selected in the vector layer of departmental administrative limits.
    - Merge of the two layers previously extracted (the layer extracted with the geometry checker algorithm is reused). Now, a single layer combines classes/features from two different sources and scales
    - Creation of new fields and assignment of a class index and a priority that will be reused as a burn-in value during the rasterization of the vector layers
    """

    INPUT_RegionalVectorLayer = parameters['INPUT_RegionalVectorLayer']
    field_RegionalLayer = parameters['field_RegionalLayer']
    ValuesList_RegLayer = (parameters['ValuesList_RegLayer'].split('\n'))
    ValidGeomLayer = parameters['ValidGeomLayer']
    INPUT_DptLayersFolder = parameters['INPUT_DptLayersFolder']
    VECTOR_LAYER_NAME = parameters['Vector_Layer_Name']
    Field_DptLayer = parameters['Field_DptLayer']
    ValuesList_DptLayer = (parameters['ValuesList_DptLayer'].split('\n'))
    INPUT_VectorLayerDpt = parameters['INPUT_VectorLayerDpt']
    field_DPT = parameters['field_DPT']
    FOLDER_OUTPUT = parameters['FOLDER_OUTPUT']
    Field1_Value = parameters['Field1_Value']
    Field2_Value = parameters['Field2_Value']
    Field1 = parameters['Field1']
    Field2 = parameters['Field2']
    OUTPUT_name = parameters['name_OUTPUT']
    
    test = QgsVectorLayer(INPUT_RegionalVectorLayer,'','ogr')
    if test.crs().isValid():
        test = None
        ExtractMerge(FOLDER_OUTPUT, INPUT_RegionalVectorLayer, field_RegionalLayer, ValuesList_RegLayer, INPUT_DptLayersFolder, VECTOR_LAYER_NAME, Field_DptLayer, ValuesList_DptLayer, field_DPT, INPUT_VectorLayerDpt, Field1, Field2, Field1_Value, Field2_Value, OUTPUT_name, ValidGeomLayer, feedback)
    else:
        feedback
        sys.exit(1)