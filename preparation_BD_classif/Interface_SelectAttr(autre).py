# -*- coding: utf-8 -*-

from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.core import (QgsProcessing,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterExpression,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterString,
                       QgsMapLayer,
                       QgsCoordinateReferenceSystem,
                       QgsVectorLayer,
                       QgsProcessingOutputDefinition,
                       QgsVectorFileWriter,
                       QgsProcessingParameterField,
                       QgsField,
                       QgsProcessingParameterFolderDestination,
                       QgsVectorDataProvider)
from qgis import processing
import os

def selectAttr(path_file, valuesList, field, output, classe, nbprior, feedback):
    """
    Selection by attribute based on the values of the field class from the input vector layer
    """
    file = QgsVectorLayer(path_file, '', 'ogr')

    # Query creation
    if len(valuesList) == 1 :
        valeur = valuesList[0]
        requete = "{} = '{}'".format(field, valeur)
    else :
        valeur = valuesList[0]
        requete = "{} = '{}'".format(field, valeur)
        for valeur in valuesList[1:] :
            requete = "{} OR {} = '{}'".format(requete,field, valeur)

    selectValeur = file.selectByExpression(requete)
    outputFile = os.path.join(output,'HAIEXTRACT.shp')
    writer = QgsVectorFileWriter.writeAsVectorFormat(file, outputFile, "utf-8", file.crs(), "ESRI shapefile", onlySelected = True)
    
    feedback.pushInfo('outputFile is {}'.format(str(outputFile)))
    feedback.pushInfo('requete is {}'.format(str(requete)))
    
    # Fields creation
    out_file= QgsVectorLayer(str(outputFile), '', 'ogr')
    
    caps = out_file.dataProvider().capabilities()
    if caps and QgsVectorDataProvider.addAttributes :
        fieldPrior = QgsField('PRIORITE', QVariant.Int)
        fieldClasse = QgsField('CLASSE', QVariant.String)
        out_file.dataProvider().addAttributes([fieldPrior, fieldClasse])
        out_file.updateFields()
        
    idFieldPrior = out_file.dataProvider().fieldNameIndex("PRIORITE")
    idFieldClass = out_file.dataProvider().fieldNameIndex("CLASSE")
    
    # Fields modification
    
    out_file.startEditing()
    
    for feat in out_file.getFeatures(): 
        feat[idFieldPrior] = nbprior
        out_file.changeAttributeValue(feat.id(), idFieldPrior, nbprior)
        out_file.changeAttributeValue(feat.id(), idFieldClass, classe)
    out_file.commitChanges()
        
class ExampleProcessingAlgorithm(QgsProcessingAlgorithm):

    INPUT = 'INPUT'
    VALUES_LIST = 'VALUES_LIST'
    FIELD = 'FIELD'
    OUTPUT = 'OUTPUT'
    NBPRIOR = 'NBPRIOR'
    CLASSE = 'CLASSE'

    def tr(self, string):

        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return ExampleProcessingAlgorithm()

    def name(self):

        return 'myscript'

    def displayName(self):

        return self.tr('My Script')

    def group(self):

        return self.tr('Example scripts')

    def groupId(self):

        return 'examplescripts'

    def shortHelpString(self):

        return self.tr("Example algorithm short description")

    def initAlgorithm(self, config=None):

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Input layer'),
                [QgsProcessing.TypeVectorAnyGeometry]
            )
        )

        self.addParameter(
            QgsProcessingParameterField(
                self.FIELD,
                self.tr('Field'),
                parentLayerParameterName = self.INPUT))
                
        self.addParameter(
            QgsProcessingParameterString(
                self.VALUES_LIST,
                self.tr('mes valeurs'),
                multiLine = True
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.NBPRIOR,
                self.tr('mon nbprior'),
            )
        )

        self.addParameter(
            QgsProcessingParameterString(
                self.CLASSE,
                self.tr('ma classe'),
           )
        )


        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.OUTPUT,
                self.tr('Output folder')))

    def processAlgorithm(self, parameters, context, feedback):
		"""
		The function allows to select features according to its attributes. Then it creates new fields and assigns new values
		"""
              
        INPUT = parameters['INPUT']
        NBPRIOR = parameters['NBPRIOR']
        output = parameters['OUTPUT']
        CLASSE = parameters['CLASSE']
        valuesList = parameters['VALUES_LIST'].split('\n')
        field = parameters['FIELD']
        
        feedback.pushInfo('INPUT is {}'.format(INPUT))
        feedback.pushInfo('OUTPUT is {}'.format(output))

       
        selectAttr(INPUT, valuesList, field, output, CLASSE, NBPRIOR, feedback)
       
        return {self.OUTPUT: output}

        feedback.pushInfo('LE TRAITEMENT EST TERMINE')
