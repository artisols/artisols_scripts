from qgis import processing
from qgis.processing import alg
from qgis.core import QgsProject
import os
from qgis.core import *
from qgis.PyQt.QtCore import QCoreApplication, QVariant


def selectAttr(path_file, valueList, field, output, field1_Value, field2_Value, newField1, newField2, name_output, feedback):
    """
    Selection by attribute based on the values of the field class from the input vector layer
    """
    
    file = QgsVectorLayer(path_file, '', 'ogr')

    # Query creation
    if len(valueList) == 1 :
        value = valueList[0]
        query = "{} = '{}'".format(field, value)
    else :
        value = valueList[0]
        query = "{} = '{}'".format(field, value)
        for value in valueList[1:] :
            query = "{} OR {} = '{}'".format(query,field, value)
            
    feedback.pushInfo('query is {}'.format(query))
    
    selectvalue = file.selectByExpression(query)
    outputFile = os.path.join(output,name_output)
    writer = QgsVectorFileWriter.writeAsVectorFormat(file, outputFile, "utf-8", file.crs(), "ESRI shapefile", onlySelected = True)
   
    # Fields creation
    out_file= QgsVectorLayer(outputFile, '', 'ogr')
    
    caps = out_file.dataProvider().capabilities()
    if caps and QgsVectorDataProvider.addAttributes :
        field1 = QgsField(newField1, QVariant.Int)
        field2 = QgsField(newField2, QVariant.Int)
        out_file.dataProvider().addAttributes([field1, field2])
        out_file.updateFields()
        
    idField1 = out_file.dataProvider().fieldNameIndex(newField1)
    idField2 = out_file.dataProvider().fieldNameIndex(newField2)
    
    # Fields modification
    
    out_file.startEditing()
    
    for feat in out_file.getFeatures(): 
        out_file.changeAttributeValue(feat.id(), idField1, field1_Value)
        out_file.changeAttributeValue(feat.id(), idField2, field2_Value)
    out_file.commitChanges()
    
    return output
    
@alg(name = 'Extract by attribute and fields creation/modification', label = 'ExtractAttribute and FieldsCreation', group='Scripts_Artisols', group_label='Scripts_Artisols')
@alg.input(type=alg.SOURCE, name = 'INPUT', label = 'Input vector layer')

@alg.input(type=alg.FIELD, name = 'field', label = 'Field', parentLayerParameterName = 'INPUT')
@alg.input(type=alg.STRING, name = 'value_LIST', label = 'Values to select in the field', multiLine = True)

@alg.input(type=alg.STRING, name = 'newField1', label = 'New Field 1', default = 'PRIORITY')
@alg.input(type=alg.NUMBER, name = 'field1_Value', label = 'Value (Field 1)')

@alg.input(type=alg.STRING, name = 'newField2', label = 'New Field 2', default = 'CLASS')
@alg.input(type=alg.NUMBER, name = 'field2_Value', label = 'Value (Field 1)')

@alg.input(type=alg.STRING, name = 'name_output', label = 'name_output', default = 'Verger.shp')
@alg.input(type=alg.FOLDER_DEST, name = 'OUTPUT', label = 'Folder output')

    
def fonction(instance, parameters, context, feedback, inputs):
    """
    The function allows to select features according to its attributes. Then it creates new fields and assigns new values
    """
    INPUT = parameters['INPUT']
    field =parameters['field']
    value_LIST = (parameters['value_LIST']).split('\n')
    newField1 = parameters['newField1']
    field1_Value = parameters['field1_Value']
    newField2 = parameters['newField2']
    field2_Value = parameters['field2_Value']
    OUTPUT = parameters['OUTPUT']
    name_output = parameters['name_output']
 
    feedback.pushInfo('field is {}'.format(field))
   
    selectAttr(INPUT, value_LIST, field, OUTPUT, field1_Value, field2_Value, newField1, newField2, name_output, feedback)


    