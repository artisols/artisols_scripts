# _*_ coding:utf-8 _*_

import gdal, ogr, osr
import gdalconst
import os, sys

def createEmptyRaster(inImg, output, nodata):
	"""
	create one band raster filled with nodata value according to a reference raster
	
	@param inImg  <string>  raster reference file path
	@param output <string>  output raster path
	@param nodata <integer> nodata value
	
	@return outRaster <gdal_ds> gdal datasource of output raster
	"""
	# open reference raster file
	dsRaster = gdal.Open(inImg, gdalconst.GA_ReadOnly)
	
	# get raster info
	proj = dsRaster.GetProjection()
	geot = dsRaster.GetGeoTransform()
	cols = dsRaster.RasterXSize
	rows = dsRaster.RasterYSize

	# output raster
	outRaster = gdal.GetDriverByName('GTiff').Create(output, cols, rows, 1, gdal.GDT_Byte)
	outRaster.SetGeoTransform(geot)
	outRaster.SetProjection(proj)
	outBand = outRaster.GetRasterBand(1)
	outBand.SetNoDataValue(nodata)
	outBand.FlushCache()
	
	return outRaster

def rasterize(inVector, inImg, fieldname, output):
	"""
	rasterize vector file according to raster input, 
	burn values stored in a attribute field
	
	@param inVector  <string> vector file path
	@param inImg     <string> raster reference file path
	@param fieldname <string> field name of vector file that stored values to burn
	@param output    <string> output raster path
	
	@return void
	"""
	# open vector file
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dsVector = driver.Open(inVector, 0)
	vLayer = dsVector.GetLayer()

	# output raster
	nodata = 255
	outRaster = createEmptyRaster(inImg, output, nodata)

	# rasterization
	gdal.RasterizeLayer(outRaster, [1], vLayer, options=["ATTRIBUTE={}".format(fieldname)])
	
	# close files
	outRaster = None
	dsVector = None

def burnRaster(inVector, inImg, fieldname, refImg = False):
	"""
	burn an existing raster with vector attribute values
	
	@param inVector  <string> vector file path
	@param inImg     <string> raster file path
	@param fieldname <string> field name of vector file that stored values to burn
	@param refImg    <string> [optional] reference raster file path 
	
	@return void
	"""
	# check if inImg exists
	if not os.path.exists(inImg):
		if refImg == False:
			print("ERROR: raster file does not exist, add a reference raster file")
			sys.exit(1)
	# create raster file or open existing raster file
		if refImg:
			nodata = 0
			dsRaster = createEmptyRaster(refImg, inImg, nodata)
	else:
		dsRaster = gdal.Open(inImg, gdalconst.GA_Update)

	# open vector file
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dsVector = gdal.OpenEx(inVector, gdal.OF_VECTOR)

	# burn value
	options = gdal.RasterizeOptions(bands=[1], attribute=fieldname)
	gdal.Rasterize(dsRaster, dsVector, options=options)
	
	# close files
	dsRaster = None
	dsVector = None
	
#---MAIN---

if __name__ == '__main__':
	if len(sys.argv) == 4:
		inv = sys.argv[1]
		inr = sys.argv[2]
		fdn = sys.argv[3]
		burnRaster(inv, inr, fdn)
	elif len(sys.argv) == 5:
		inv = sys.argv[1]
		inr = sys.argv[2]
		fdn = sys.argv[3]
		ref = sys.argv[4]
		burnRaster(inv, inr, fdn, refImg = ref)
	else:
		print("input parameters:")
		print("	- inVectorFile <string>  vector file path")
		print("	- inRasterFile <string>  raster file path (if not exists, add referRaster parameter)")
		print("	- vectorField  <list>    vector field of values to burn")
		print("	- referRaster  <string>  [optional] reference raster file path")
		print("---")
		print("Exemple: python rasterize.py /inshape.shp /raster.tif nature [/ref_raster.tif]")
		sys.exit(1)
