#-*- coding:utf8 -*-

import os

def extent(img):
    layerSat = QgsRasterLayer(img, 'imgSat')
    # Get the extent
    extendRaster = layerSat.extent()
    print(extendRaster)
    return (extendRaster)
    
def createList (dirData, listDpt, listCouches, output):
    #Creation of the file with the list
    maList = open(output, 'w')
    
    for couche in listCouches: 
        for num in listDpt:
            for root, reps, files in os.walk(dirData):
                for file in files:
                    path_layer = os.path.join(root, file)
                    # Get the department number (D0XX) written in the folder name
                    numDpt = 'R{}'.format(num)
                    if numDpt in path_layer and couche == file :
                        maList.write('{}\n'.format(path_layer))
    maList.close()

# --- PARAMETERS --- 

# Layers order
listCouches = ["AUTRE_VEGETATION.SHP",
                        "SABLE.SHP",
                        "PRAIRIE.SHP",
                        "FORET.SHP",
                        "EAU.SHP",
                        "MARAICHAGE.SHP",
                        "VIGNE.SHP",
                        "GRANDE_CULTURE.SHP",
                        "VERGER.SHP",
                        "CARRIERES.SHP",
                        "SPORT_IMPERMEABLE.SHP",
                        "AERODROME.SHP",
                        "CAMPING.SHP",
                        "GOLF.SHP",
                        "PARKING.SHP",
                        "CIMETIERE.SHP",
                        "HAIE.SHP",
                        "SERRE.SHP",
                        "BATI_ACTIVITE.SHP",
                        "BATI_RESIDENTIEL.SHP"]
                        

dirData = r'D:\Donnees\output_donnees\BD_OCS'
outData =  r'D:\Donnees\output_donnees\listeLayer.txt'  

# Extent parameters
data_sat = r'D:\Donnees\Images_Sat'
images = list()

# Intersect Parameters
path_dpt = r'D:\Donnees\departements\dpt_occitanie.shp'
selected_fid = []

# --- MAIN FUNCTION --- 
for root, reps, files in os.walk(data_sat):
    vlayerdpt = QgsVectorLayer(path_dpt, 'dpt', 'ogr')
    for file in files :
        # Get the satellite images in a folder
        if os.path.splitext(file)[1] == '.TIF' and 'MS' in root :
            images.append(os.path.join(root,file))
            for img in images:
                # Get the extent of each image
                extentImg = extent(img)
                print(extentImg)
				# Get the features of the department layer
                featDpt = vlayerdpt.getFeatures()
                for fd in featDpt:
                    geom = fd.geometry()
					# Check if the geometry of the features intersects the satellite image extent
                    if geom.intersects(extentImg):
					    # Write in a list the department numbers (values in this field) 
                        selected_fid.append(fd.attribute('code_insee'))
						# Delete the duplicates of the list
                        listeDpt=list(set(selected_fid))
                        print(listeDpt)
                        
# Create a list of the paths of the vector layers per department used during the processing                  
createList(dirData, listeDpt, listCouches, outData)

print('traitement terminé')