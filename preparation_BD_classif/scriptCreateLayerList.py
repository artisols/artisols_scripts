#-*- coding:utf8 -*-
import os, sys

""" create a file (.txt) with a defined list"""

def createList (dirData, listDpt, listCouches, output):
    #Creation of the file with the list
	maList = open(output, 'w')
	
	for couche in listCouches: 
		for num in listDpt:
			for root, reps, files in os.walk(dirData):
				for file in files:
					path_layer = os.path.join(root, file)
                    # Get the department number (D0XX) written in the folder name
					numDpt = 'R{}'.format(num)
					if numDpt in path_layer and couche == file :
						maList.write('{}\n'.format(path_layer))
	maList.close()

# --- PARAMETERS --- 

# Layers order
listCouches = ["AUTRE_VEGETATION.SHP",
						"SABLE.SHP",
						"PRAIRIE.SHP",
						"FORET.SHP",
						"EAU.SHP",
						"MARAICHAGE.SHP",
						"VIGNE.SHP",
						"GRANDE_CULTURE.SHP",
						"VERGER.SHP",
						"CARRIERES.SHP",
						"SPORT_IMPERMEABLE.SHP",
						"AERODROME.SHP",
						"CAMPING.SHP",
						"GOLF.SHP",
						"PARKING.SHP",
						"CIMETIERE.SHP",
						"HAIE.SHP",
						"SERRE.SHP",
						"BATI_ACTIVITE.SHP",
						"BATI_RESIDENTIEL.SHP"]
						
dirData = sys.argv[1]
listDpt = sys.argv[2].split(',')	
output =  sys.argv[3]  

# --- MAIN FUNCTION--- 

createList(dirData, listDpt, listCouches, output)
print('traitement termin�')