# _*_ coding:utf-8 _*_

import gdal, ogr, osr
import gdalconst
import os, sys

""" Create an empty raster according to the extent of an existing shapefile"""

def extent(path_emprise):
    emprise = QgsVectorLayer(path_emprise, 'emprise')
    extentDpt = emprise.extent()
    
    return extentDpt
    
def createRaster(QgsRectangle, outputRaster):

    alg_params = {
            'EXTENT': QgsRectangle,
            'TARGET_CRS': 'EPSG:2154',
            'PIXEL_SIZE': 1.5,
            'OUTPUT': outputRaster
        }
   
    raster = processing.run('qgis:createconstantrasterlayer', alg_params)
    return raster
    
path_09 = r"D:\Donnees\Emprises_10km_09.shp"
outRaster = r'D:\Donnees\test\Raster09.tif'

extent09 = extent(path_09)
print(extent09.toString())
type(extent09)
createRaster(extent09, outRaster)

print("fini")
