import os, sys
from osgeo import ogr

"""
Calculate the total area of features of shapefiles
"""
	
# Parameters:
#argv_1 : vector layers folder path
#argv_2 : path of the file (.txt) created

dirData = sys.argv[1]
Surface_Couches = sys.argv[2]

listCouches = ["AUTRE_VEGETATION.SHP",
						"SABLE.SHP",
						"PRAIRIE.SHP",
						"FORET.SHP",
						"EAU.SHP",
						"MARAICHAGE.SHP",
						"VIGNE.SHP",
						"GRANDE_CULTURE.SHP",
						"VERGER.SHP",
						"CARRIERES.SHP",
						"SPORT_IMPERMEABLE.SHP",
						"AERODROME.SHP",
						"CAMPING.SHP",
						"GOLF.SHP",
						"PARKING.SHP",
						"CIMETIERE.SHP",
						"HAIE.SHP",
						"SERRE.SHP",
						"BATI_ACTIVITE.SHP",
						"BATI_RESIDENTIEL.SHP"]
						
fichierTxt = open(Surface_Couches, 'w')
fichierTxt.write('Couche;Area')

for shape in listCouches : 
	somme = 0.
	for root, reps, files in os.walk(dirData):
		for file in files :
			if shape == file :
				path_layer = os.path.join(root, file)
				driver = ogr.GetDriverByName('ESRI Shapefile')
				dataSource = driver.Open(path_layer, 0) # 0 means read-only. 1 means writeable.
				couche = dataSource.GetLayer()
				for f in couche : 
					geom = f.GetGeometryRef()
					area = geom.GetArea()
					somme = somme+area
	fichierTxt.write('\n{};{}'.format(shape,somme))
	print('Total surface de la couche' + shape + ':\n' + str(somme))
fichierTxt.close()
