# _*_ coding:utf-8 _*_

import gdal, ogr, osr
import gdalconst
import os, sys

# RASTERIZE FUNCTION

def createEmptyRaster(inImg, output, nodata):
    """
    create one band raster filled with nodata value according to a reference raster
    
    @param inImg  <string>  raster reference file path
    @param output <string>  output raster path
    @param nodata <integer> nodata value
    
    @return outRaster <gdal_ds> gdal datasource of output raster
    """
    # open reference raster file
    dsRaster = gdal.Open(inImg, gdalconst.GA_ReadOnly)
    
    # get raster info
    proj = dsRaster.GetProjection()
    geot = dsRaster.GetGeoTransform()
    cols = dsRaster.RasterXSize
    rows = dsRaster.RasterYSize

    # output raster
    outRaster = gdal.GetDriverByName('GTiff').Create(output, cols, rows, 1, gdal.GDT_Byte)
    outRaster.SetGeoTransform(geot)
    outRaster.SetProjection(proj)
    outBand = outRaster.GetRasterBand(1)
    outBand.SetNoDataValue(nodata)
    outBand.FlushCache()
    
    return outRaster

def burnRaster(inVector, inImg, fieldname, refImg = False):
    """
    burn an existing raster with vector attribute values
    
    @param inVector  <string> vector file path
    @param inImg     <string> raster file path
    @param fieldname <string> field name of vector file that stored values to burn
    @param refImg    <string> [optional] satellite reference raster file path 
    
    @return void
    """
    # check if inImg exists
    if not os.path.exists(inImg):
        if refImg == False:
            print("ERROR: raster file does not exist, add a reference raster file")
            sys.exit(1)
    # create raster file or open existing raster file
        if refImg:
            nodata = 0
            dsRaster = createEmptyRaster(refImg, inImg, nodata)
    else:
        dsRaster = gdal.Open(inImg, gdalconst.GA_Update)

    # open vector file
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dsVector = gdal.OpenEx(inVector, gdal.OF_VECTOR)

    # burn value
    options = gdal.RasterizeOptions(bands=[1], attribute=fieldname)
    gdal.Rasterize(dsRaster, dsVector, options=options)
    
    # close files
    dsRaster = None
    dsVector = None

# CREATION OF A LIST OF VECTOR LAYERS (PER DEPARTMENT) THAT INTERSECT THE SATELLITE IMAGES USED TO RASTERIZE THE VECTOR DATA

def extent(img):
    """
    Get the satellite images extent
    """ 
    layerSat = QgsRasterLayer(img, 'imgSat')
    extentRaster = layerSat.extent()
    return (extentRaster)

def createList (dirData, listDpt, layerList):
    """
    Create a list of the paths of the vector layers per department used during the processing
    """
    maList = list()
    
    for couche in layerList: 
        for num in listDpt:
            for root, reps, files in os.walk(dirData):
                for file in files:
                    path_layer = os.path.join(root, file)
                    # Get the department number (D0XX) written in the folder name
                    numDpt = 'D0{}'.format(num)
                    if numDpt in path_layer and couche == file :
                        print('path_layer:', path_layer)
                        maList.append(path_layer)
    return maList
    
def dptImg(extentImg, path_dpt):
    """
    Check which departments intersect the satellite images used and write a list of the department numbers
    """
    selected_fid = []
    # Get the features of the department layer
    vlayerdpt = QgsVectorLayer(path_dpt, 'dpt', 'ogr')
    featDpt = vlayerdpt.getFeatures()
    for fd in featDpt:
	    # Get the geometry of the features 
        geom = fd.geometry()
		# Check if the geometry of the features intersects the satellite image extent
        if geom.intersects(extentImg): 
		# Write in a list the department numbers (values in this field) 
            selected_fid.append(fd.attribute('code_insee'))
    # Delete the duplicates of the list
    listeDpt=list(set(selected_fid))
    return listeDpt

#--- PARAMETERS ---

dir_data = r'D:\Donnees\Images_Sat'
dir_ocs = r'D:\Donnees\output_donnees\BD_OCS'
outdir = r'D:\Donnees\Sortie_test\Raster'
path_dpt = r'D:\Donnees\departements\dpt_occitanie.shp'

layerList = ["AUTRE_VEGETATION.SHP",
						"SABLE.SHP",
						"PRAIRIE.SHP",
						"FORET.SHP",
						"EAU.SHP",
						"MARAICHAGE.SHP",
						"VIGNE.SHP",
						"GRANDE_CULTURE.SHP",
						"VERGER.SHP",
						"CARRIERES.SHP",
						"SPORT_IMPERMEABLE.SHP",
						"AERODROME.SHP",
						"CAMPING.SHP",
						"GOLF.SHP",
						"PARKING.SHP",
						"CIMETIERE.SHP",
						"HAIE.SHP",
						"SERRE.SHP",
						"BATI_ACTIVITE.SHP",
						"BATI_RESIDENTIEL.SHP"]


# ---- MAIN FUNCTION ----
images = list()

for root, reps, files in os.walk(dir_data):
    for file in files :
        if os.path.splitext(file)[1] == '.TIF' and 'MS' in root:
            print (file)
            images.append(os.path.join(root, file))

for img in images:
    print('---\ntraitement de : ', img)
    basename = os.path.basename(img)
    outname = 'OCS_{}'.format(basename)
    sortie = os.path.join(outdir, outname)
    print('SORTIE:', sortie)
    extentImg = extent(img)
    dptList = dptImg(extentImg, path_dpt)
    maList = createList(dir_ocs, dptList, layerList)
    for ocs in maList:
        print('ocs', ocs)
        burnRaster(ocs, sortie, 'CLASSE', img)
