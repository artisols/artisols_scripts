"""
Clip a vector layer with a departement polygon by iterating over a layer with departments borders. 
"""

import os, sys
from osgeo import ogr

# ----- SOUS FONCTIONS --------
def verifValid(ocs_layer):
	"""
	Checks the geometry of a vector layer. 
	"""
	# Get the features of the department layer
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dataSource = driver.Open(ocs_layer, 0)
	layerRpg = dataSource.GetLayer()
	for feature in layerRpg:
		geom = feature.GetGeometryRef()
		if not geom.IsValid():
			feature.SetGeometryDirectly(geom.Buffer(0))
			assert feature.GetGeometryRef().IsValid()
		return dataSource, layerRpg

def extractByAttr(path, field, numdpt): 
	"""
	Selection by attribute based on the values of the field class from the input vector layer (department borders)
	"""
	# Get the features of the department layer
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dataSource = driver.Open(path, 0)
	layerDpt = dataSource.GetLayer()
	layername = layerDpt.GetName()
	print(layername)
	
	requeteSQL = "SELECT * FROM {} WHERE {} = '{}'".format(layername, field, numdpt)
	print(requeteSQL)
	
	layer_dptSelect = dataSource.ExecuteSQL(requeteSQL)

	return dataSource, layer_dptSelect

def clip(ocsol_layer, dpt_layer, outfile):
	"""
	Clips a vector layer matching with a department selected in the vector layer of departments borders
	"""	
	## Input
	driver = ogr.GetDriverByName("ESRI Shapefile")
	spatialRef_ocsol = ocsol_layer.GetSpatialRef()
	
	## Clipped Shapefile
	outDataSource = driver.CreateDataSource(outfile)
	outLayer = outDataSource.CreateLayer('FINAL', spatialRef_ocsol, geom_type=ogr.wkbMultiPolygon)

	ogr.Layer.Clip(ocsol_layer, dpt_layer, outLayer)
	print(outLayer.GetFeatureCount())
	outDataSource.Destroy()
	
#----- MAIN FUNCTION ------

def clipLayer(outputdir, dir_data, field_dpt, dpt_path, num):
	for root, dirs, files in os.walk(dir_data):
		for file in files :
			if '.shp' in file or '.SHP' in file: 
				print(file)
				# Selects by attribute one department every loop in the departmental boundaries layer 
				dpt_ds, dptLayer = extractByAttr(dpt_path, field_dpt, num)
				print("type dpt", type(dptLayer))
				print('count dpt: ', dptLayer.GetFeatureCount())
				# Creation of the output path
				chemin_file = os.path.join(root,file)
				basename = os.path.splitext(os.path.split(file)[1])[0]
				os.mkdir(os.path.join(outputdir, num))
				folder_output = os.path.join(outputdir, num)
				path_clip = os.path.join(folder_output, '{}.shp'.format(basename))
				print('Output', path_clip)
				# Checks the validity on the geometries of the land cover layer 
				valid_ds, layerValid = verifValid(chemin_file)
				print("type layervalid", type(layerValid))
				print('count layervalid: ', layerValid.GetFeatureCount())
				coucheRpgDpt = clip(layerValid, dptLayer, path_clip)

# --------   EXECUTION ----------
if __name__ == "__main__":

	# argv_1 : input vector layers folder (land use)
	# argv_2 : input department borders vector file
	# argv_3 : output folder 	
	# argv_4 : field of department number in the department layer
	# i.e. python script_Clip_empriseDptOGR.py D:\test\clc D:\test\dpt_occitanie.shp D:\test\OUT code_insee
	
	# Input
	dir_data = sys.argv[1]
	dpt_path = sys.argv[2]

	# Output
	outputdir = sys.argv[3] 

	# Department extraction
	field_dpt = sys.argv[4]
	numDpt = ['09','11','12','30','31','32','34','46','48','65','66','81','82']
	
	for num in numDpt :
		clipLayer(outputdir, dir_data, field_dpt, dpt_path, num)

	print('fin du traitement')