"""
Clip a vector layer with a departement polygon by iterating over a layer with departments borders. 
"""

import processing
import os, sys

# ----- SOUS FONCTIONS --------
def verifValid(ocs_layer):
	"""
	Checks the geometry of a vector layer. Three vector layers are generated according to 3 groups
	(valid, invalid and error). The vector layer with valid geometries is extracted for the next step of the processing
	"""
	alg_params = { 
			'INPUT_LAYER' : ocs_layer, 
			'METHOD' : 2, 
			'VALID_OUTPUT' : 'TEMPORARY_OUTPUT', 
			'INVALID_OUTPUT': 'TEMPORARY_OUTPUT', 
			'ERROR_OUTPUT' : 'TEMPORARY_OUTPUT'
		}
	verifLayer = processing.run('qgis:checkvalidity', alg_params)
	return verifLayer

def extractByAttr(path, field, listValues):
	"""
	Selection by attribute based on the values of the field class from the input vector layer 
	"""
	# Query 
	if len(listValues) == 1 :
		valeur = listValues[0]
		requete = "{} = '{}'".format(field, valeur)
	else :
		valeur = listValues[0]
		requete = "{} = '{}'".format(field, valeur)
		for valeur in listValues[1:] :
			requete = "{} OR {} = '{}'".format(requete,field, valeur)
	print(requete)
	
	# Select by attribute according to the query 
	alg_params = {
			'INPUT': path,
			'EXPRESSION': requete,
			'OUTPUT':'TEMPORARY_OUTPUT'
		}
	vectSelect = processing.run('native:extractbyexpression', alg_params)
	return vectSelect

def clip(ocsol_layer, dpt_layer, outfile):
	"""
	Clips a vector layer matching with a department selected in the vector layer of department limits
	"""	
	alg_params = {
			'INPUT': ocsol_layer,
			'OVERLAY': dpt_layer,
			'OUTPUT': outfile
		}
		
	vectSelect = processing.run('qgis:clip', alg_params)
	return vectSelect
	
#----- MAIN FUNCTION ------

def clipLayer(outputdir, dir_data, field_dpt, dpt_path, num):
	for root, dirs, files in os.walk(dir_data):
		for file in files :
			if '.SHP' in file or '.shp' in file :
				print(file)
				# Selects by attribute one department every loop in the departmental boundaries layer 
				dptLayer = extractByAttr(dpt_path, field_dpt, [num])
				# Creation of the output path
				chemin_file = os.path.join(root,file)
				basename = os.path.splitext(os.path.split(file)[1])[0]
				os.mkdir(os.path.join(outputdir, num))
				folder_output = os.path.join(outputdir, num)
				path_clip = os.path.join(folder_output, '{}.shp'.format(basename))
				# Checks the validity on the geometries of the land cover layer 
				layerValid = verifValid(chemin_file)
				coucheRpgDpt = clip(layerValid['VALID_OUTPUT'], dptLayer['OUTPUT'], path_clip)

# -----   PARAMETERS ---------

# Input
# argv_1 : input vector layers folder (land use)
# argv_2 : input department borders vector file 
dir_data = r'D:\Donnees\RPG'
dpt_path = r'D:\Donnees\departements\dpt_occitanie.shp'

# Output
# argv_3 : output folder 
outputdir = r'D:\Donnees\output_data'

# Department extraction
# argv_4 : field of department number in the department layer
field_dpt = 'code_insee'
numDpt = ['09','11','12','30','31','32','34','46','48','65','66','81','82']

# --------   EXECUTION ----------

for num in numDpt :
	clipLayer(outputdir, dir_data, field_dpt, dpt_path, num)

print('fin du traitement')