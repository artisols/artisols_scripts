from osgeo import ogr
import os,sys

def verifValid(ocs_layer):
	"""
	Checks the geometry of a vector layer. 
	"""
	# Get the features of the department layer
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dataSource = driver.Open(ocs_layer, 0)
	layerRpg = dataSource.GetLayer()
	for feature in layerRpg:
		geom = feature.GetGeometryRef()
		if not geom.IsValid():
			feature.SetGeometryDirectly(geom.Buffer(0))
			assert feature.GetGeometryRef().IsValid()
		return dataSource, layerRpg
		
def extractByAttr(path, field, listValues): 
	"""
	Selection by attribute based on the values of the field class from the input vector layer (department borders)
	"""
	# Get the features of the department layer
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dataSource = driver.Open(path, 0)
	layerDpt = dataSource.GetLayer()
	layername = layerDpt.GetName()
	print(layername)
	
	# Query
	if len(listValues) == 1 :
		valeur = listValues[0]
		requete = "SELECT * FROM {} WHERE {} = '{}'".format(layername, field, valeur)
	else :
		valeur = listValues[0]
		requete = "SELECT * FROM {} WHERE {} = '{}'".format(layername, field, valeur)
		for valeur in listValues[1:] :
			requete = "{} OR {} = '{}'".format(requete,field, valeur)
	print(requete)
	layer_Select = dataSource.ExecuteSQL(requete)

	return dataSource, layer_Select
	  
# def extractByLoc(chemin, dpt, sortie):
    # """
    # Extracts features from a vector layer with regional administrative boundaries that match with an input 
    # vector layer of departmental administrative limits
    # """   
    # alg_params = {
            # 'INPUT': chemin,
            # 'PREDICATE': [0],
            # 'INTERSECT': dpt,
            # 'OUTPUT': sortie
        # }
   
    # vectSelect = processing.run('native:extractbylocation', alg_params)
    
    # #writer = QgsVectorFileWriter.writeAsVectorFormat(fichier, output, "utf-8", fichier.crs(), "ESRI shapefile", onlySelected = True)
    # return vectSelect
    
# def fields(outputRPG, field1, field2, nbprior, classe):
    # """
    # Creates new fields and assigns a class index (number) and a priority that will
    # be reused as a burn-in value to rasterize the vector layers
    # """
    # # Fields creation
    # out_file= QgsVectorLayer(outputRPG, '', 'ogr')
    # caps = out_file.dataProvider().capabilities()
    # if caps and QgsVectorDataProvider.addAttributes :
        # newField1 = QgsField(field1, QVariant.Int)
        # newField2 = QgsField(field2, QVariant.String)
        # out_file.dataProvider().addAttributes([newField1, newField2])
        # out_file.updateFields()
        
    # idFieldPrior = out_file.dataProvider().fieldNameIndex(field1)
    # idFieldClass = out_file.dataProvider().fieldNameIndex(field2)
    
    # # Fields modification
    # out_file.startEditing()
    # for feat in out_file.getFeatures(): 
        # out_file.changeAttributeValue(feat.id(), idFieldPrior, nbprior)
        # out_file.changeAttributeValue(feat.id(), idFieldClass, classe)
    # out_file.commitChanges()
    
#----- MAIN FUNCTION -----------

def extractRPG(outputdir, rpg_path, field_rpg, field_value, champ_dpt, dpt_chemin, field1, field2, nbprior, classe, nomSortie):
    
	# ----- Checks validity on the geometries of the regional layer 
    valid_ds, validRpg = verifValid(rpg_path)
	
    # ----- Selects by attribute in the regional layer (e.g. RPG)
    print('execution de selection rpg')
    coucheRpg = extractByAttr(validRpg , field_rpg, field_value)

    # # ----- Loops in a folder that contains the vector layers for several departments
    # for num in numDpt: 
        # print(num)
        # # Selects by attribute one department every loop in the departmental boundaries layer 
        # print("Selection du département")
        # coucheDpt = extractByAttr(dpt_chemin, champ_dpt, num)
               
        # # Extracts the features of the regional layer (RPG) matching with the department selected
        # print("Extraction de la classe du RPG en fonction du déparement")
        # path_outputRPG = os.path.join(outputdir, num, nomSortie)
        # print(path_outputRPG)
        # coucheRpgDpt = extractByLoc(validRpg['VALID_OUTPUT'], coucheDpt['OUTPUT'], path_outputRPG)
               
        # # Creates the new fields (e.g. 'Class' and 'Priority') and assigns values (class index and priority number)
        # fields(path_outputRPG, field1, field2, nbprior, classe)

# --------   PARAMETERS ---------

# --- Input
rpg_path = r"D:\Donnees\RPG_2016\PARCELLES_GRAPHIQUES.shp"
dpt_chemin = r"D:\Donnees\departements\dpt_occitanie.shp"
numDpt = ["09","11","12","30","31","32","34","46","48","65","66","81","82"]

# --- Output
outputdir = r'D:\test\OUT'

# --- RPG Extraction
champ_rpg = 'CODE_GROUP'
champRpgValeur = ['16','18','19']

# --- Department Extraction
champ_dpt = 'code_insee'

# --- output name (merge layer)
nomSortie = 'PRAIRIE.shp'

# ---- Fields creation and modification
field1 = 'PRIORITE'
field2 = 'CLASSE'
nbprior = 3
classe = '9'

# --------   EXECUTION ----------
extractRPG(outputdir, rpg_path, champ_rpg, champRpgValeur, champ_dpt, dpt_chemin, field1, field2, nbprior, classe, nomSortie)

print('traitement terminé')