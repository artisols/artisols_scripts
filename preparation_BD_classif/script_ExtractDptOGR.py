from osgeo import ogr
import os,sys
import rtree
import time

""" Extract, per department, features according to their attribute values """


def verifValid(ocs_layer):
	"""
	Checks the geometry of a vector layer. 
	"""
	# Get the features of the department layer
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dataSource = driver.Open(ocs_layer, 0)
	layerRpg = dataSource.GetLayer()
	for feature in layerRpg:
		geom = feature.GetGeometryRef()
		if not geom.IsValid():
			feature.SetGeometryDirectly(geom.Buffer(0))
			assert feature.GetGeometryRef().IsValid()
		return dataSource, layerRpg

def extractByAttr(field, listValues, path = None, ds = None, ogrLayer = None): 
	"""
	Selection by attribute based on the values of the field class from the input vector layer (department borders)
	"""
	if path :
		print('load file from path')
		# Get the features of the department layer
		driver = ogr.GetDriverByName("ESRI Shapefile")
		dataSource = driver.Open(path, 0)
		layer = dataSource.GetLayer()
	elif ds and ogrLayer :
		print('load objet layer with datasource')
		dataSource = ds
		layer = ogrLayer
	else : 
		print('error')
	
	layername = layer.GetName()
	print(layername)
	
	# Query
	if len(listValues) == 1 :
		valeur = listValues[0]
		requete = "SELECT * FROM {} WHERE {} = '{}'".format(layername, field, valeur)
	else :
		valeur = listValues[0]
		requete = "SELECT * FROM {} WHERE {} = '{}'".format(layername, field, valeur)
		for valeur in listValues[1:] :
			requete = "{} OR {} = '{}'".format(requete,field, valeur)
	print(requete)
	
	layer_Select = dataSource.ExecuteSQL(requete) 
	
	return 	dataSource, layer_Select

def extractByLoc(ds_rpg, ogrLayer_rpg, ds_dpt, ogrLayer_dpt, outfile):
	"""
	Extracts features from a vector layer with regional administrative boundaries that match with an input 
	vector layer of departmental administrative limits
	"""   
	## Input
	spatialRef_rpg = ogrLayer_rpg.GetSpatialRef()	
	
	## Intersect Shapefile
	driver = ogr.GetDriverByName("ESRI Shapefile")
	outDataSource = driver.CreateDataSource(outfile)
	outLayer = outDataSource.CreateLayer('FINAL', spatialRef_rpg, geom_type=ogr.wkbMultiPolygon)
		
	ogr.Layer.Intersection(ogrLayer_rpg, ogrLayer_dpt, outLayer)
	print(outLayer.GetFeatureCount())
	outDataSource.Destroy()

# def fields(outputRPG, field_prior, field_class, nbprior, classe):
	# """
	# Creates new fields and assigns a class index (number) and a priority that will
	# be reused as a burn-in value to rasterize the vector layers
	# """
	# # Add fields to the new layer
	# inLayerDefn = outputRPG.GetLayerDefn()
	# for i in range(0, inLayerDefn.GetFieldCount()):
		# fieldDefn = inLayerDefn.GetFieldDefn(i)
		# fieldName = fieldDefn.GetName()
		# outLayer.CreateField(fieldDefn)
		
	# # Add selected features to the new layer
	# for feat in outputRPG:
		# outLayer.CreateFeature(feat)
	
	# # Add fields
	# coord_fld_prior = ogr.FieldDefn(field_prior, ogr.OFTInteger)
	# coord_fld_prior.SetWidth(5)
	# coord_fld_prior.SetPrecision(3)
	# outLayer.CreateField(coord_fld_prior)
	# coord_fld_class = ogr.FieldDefn(field_class, ogr.OFTString)
	# coord_fld_class.SetWidth(5)
	# outLayer.CreateField(coord_fld_class)

	# # Set values in fields	
	# for feature in outLayer:
		# feature.SetField(field_prior, nbprior)
		# feature.SetField(field_class, classe)
		# outLayer.SetFeature(feature) 
	
#----- MAIN FUNCTION -----------

def extractRPG(outputdir, rpg_path, field_rpg, field_value, champ_dpt, dpt_chemin, field1, field2, nbprior, classe, nomSortie):
	starttime = time.time()
	# ----- Checks validity on the geometries of the regional layer 
	valid_ds, validRpg = verifValid(rpg_path)
	
	# ----- Selects by attribute in the regional layer (e.g. RPG)
	print('execution de selection rpg')
	ds_rpg, valid_Rpg = extractByAttr(field_rpg, field_value, ogrLayer = validRpg, ds = valid_ds)
	#extractByAttr('toto', [1,2], ds = 'monpath', ogrLayer = 'truc')#, ds = None, ogrLayer = None)

	# # ----- Loops in a folder that contains the vector layers for several departments
	for num in numDpt: 
		print(num)
		folder_output = os.path.join(outputdir, num)
		os.mkdir(folder_output)

		# Selects by attribute one department every loop in the departmental boundaries layer 
		print("Selection du département")
		ds_dpt, coucheDpt = extractByAttr(champ_dpt, [num], path = dpt_chemin)
			   
		# Extracts the features of the regional layer (RPG) matching with the department selected
		print("Extraction de la classe du RPG en fonction du département")
		path_outputRPG = os.path.join(folder_output, nomSortie)
		print(path_outputRPG)
		coucheRpgDpt = extractByLoc(ds_rpg, validRpg, ds_dpt, coucheDpt, path_outputRPG)
		endtime = time.time()
		print("temps d'execution : ", (endtime-starttime)/60)
		# # Creates the new fields (e.g. 'Class' and 'Priority') and assigns values (class index and priority number)
		# fields(path_outputRPG, field1, field2, nbprior, classe)

# --------   PARAMETERS ---------

# --- Input
rpg_path = r"D:\test\rpg_extract.shp"
dpt_path = r"D:\Donnees\departements\dpt_occitanie.shp"
numDpt = ["09","11","12","30","31","32","34","46","48","65","66","81","82"]

# --- Output
outputdir = r'D:\test\OUT\AL'

# --- RPG Extraction
field_rpg = 'CODE_GROUP'
champRpgValeur = ['16','18','19']

# --- Department Extraction
field_dpt = 'code_insee'

# --- output name (merge layer)
nomSortie = 'PRAIRIE.shp'

# ---- Fields creation and modification
field_prior = 'PRIORITY'
field_class = 'CLASS'
nbprior = 3
classe = '9'

# --------   EXECUTION ----------
extractRPG(outputdir, rpg_path, field_rpg, champRpgValeur, field_dpt, dpt_path, field_prior, field_class, nbprior, classe, nomSortie)

print('traitement terminé')