from osgeo import ogr
import os,sys
import rtree
import time

""" Extract, per department, features according to their attribute values and by creating spatial index """

def verifValid(ocs_layer):
	"""
	Checks the geometry of a vector layer. 
	"""
	# Get the features of the department layer
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dataSource = driver.Open(ocs_layer, 0)
	layerRpg = dataSource.GetLayer()
	for feature in layerRpg:
		geom = feature.GetGeometryRef()
		if not geom.IsValid():
			feature.SetGeometryDirectly(geom.Buffer(0))
			assert feature.GetGeometryRef().IsValid()
		return dataSource, layerRpg

def extractByAttrDpt(path, field, num, outdpt): 
	"""
	Selection by attribute based on the values of the field class from the input vector layer (department borders)
	"""
	# Get the features of the department layer
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dataSource = driver.Open(path, 0)
	layer = dataSource.GetLayer()

	layername = layer.GetName()
	print(layername)

	# Query
	requete = "SELECT * FROM {} WHERE {} = '{}'".format(layername, field, num)

	print(requete)
	layer_Select = dataSource.ExecuteSQL(requete)
	
	spatialRef_rpg = layer.GetSpatialRef()
	driver = ogr.GetDriverByName("ESRI Shapefile")
	outds = driver.CreateDataSource(outdpt)
	outLayer = outds.CreateLayer('attrselect', spatialRef_rpg, geom_type=ogr.wkbMultiPolygon)
	
	## Add fields to the new layer
	inLayerDefn = layer_Select.GetLayerDefn()
	for i in range(0, inLayerDefn.GetFieldCount()):
		fieldDefn = inLayerDefn.GetFieldDefn(i)
		fieldName = fieldDefn.GetName()
		outLayer.CreateField(fieldDefn)
	
	n = 0
	for fid1 in range(0, layer_Select.GetFeatureCount()):
		n += 1
		feature1 = layer_Select.GetFeature(fid1)
		outLayer.CreateFeature(feature1)
	print ('n', n)
		
	return 	dataSource, outLayer
	
	
# def extractByAttr(field, listValues, outdpt, path = None, ds = None, ogrLayer = None): 
	# """
	# Selection by attribute based on the values of the field class from the input vector layer (department borders)
	# """
	# if path :
		# print('load file from path')
		# # Get the features of the department layer
		# driver = ogr.GetDriverByName("ESRI Shapefile")
		# dataSource = driver.Open(path, 0)
		# layer = dataSource.GetLayer()
	# elif ds and ogrLayer :
		# print('load objet layer with datasource')
		# dataSource = ds
		# layer = ogrLayer
	# else : 
		# print('error')
	
	# layername = layer.GetName()
	# print(layername)

	# # Query
	# if len(listValues) == 1 :
		# valeur = listValues[0]
		# requete = "SELECT * FROM {} WHERE {} = '{}'".format(layername, field, valeur)
		# #requete = "{} = '{}'".format(field, valeur)

	# else :
		# valeur = listValues[0]
		# requete = "SELECT * FROM {} WHERE {} = '{}'".format(layername, field, valeur)
		# #requete = "{} = '{}'".format(field, valeur)
		# for valeur in listValues[1:] :
			# requete = "{} OR {} = '{}'".format(requete,field, valeur)
	# print(requete)
	# layer_Select = dataSource.ExecuteSQL(requete)
	
	# if path :

		# spatialRef_rpg = layer.GetSpatialRef()
		# driver = ogr.GetDriverByName("ESRI Shapefile")
		# outds = driver.CreateDataSource(outdpt)
		# outLayer = outds.CreateLayer('attrselect', spatialRef_rpg, geom_type=ogr.wkbMultiPolygon)
		
		# ## Add fields to the new layer
		# inLayerDefn = layer_Select.GetLayerDefn()
		# for i in range(0, inLayerDefn.GetFieldCount()):
			# fieldDefn = inLayerDefn.GetFieldDefn(i)
			# fieldName = fieldDefn.GetName()
			# outLayer.CreateField(fieldDefn)
		
		# n = 0
		# for fid1 in range(0, layer_Select.GetFeatureCount()):
			# n += 1
			# feature1 = layer_Select.GetFeature(fid1)
			# outLayer.CreateFeature(feature1)
		# print ('n', n)
		
	# else : 
	# ## Create new layer
	# # outdriver = ogr.GetDriverByName("Memory")
	# # outds = outdriver.CreateDataSource('out')
		# spatialRef_rpg = layer.GetSpatialRef()
		# driver = ogr.GetDriverByName("ESRI Shapefile")
		# outds = driver.CreateDataSource('D:\test\OUT\test.shp')
		# outLayer = outds.CreateLayer('attrselect', spatialRef_rpg, geom_type=ogr.wkbMultiPolygon)
		
		# ## Add fields to the new layer
		# inLayerDefn = layer_Select.GetLayerDefn()
		# for i in range(0, inLayerDefn.GetFieldCount()):
			# fieldDefn = inLayerDefn.GetFieldDefn(i)
			# fieldName = fieldDefn.GetName()
			# outLayer.CreateField(fieldDefn)
		
		# n = 0
		# for fid1 in range(0, layer_Select.GetFeatureCount()):
			# n += 1
			# feature1 = layer_Select.GetFeature(fid1)
			# outLayer.CreateFeature(feature1)
		# print ('n', n)

	# #layer_select = layer.SetAttributeFilter(requete) 
	
		
	# # #layer_Select = dataSource.ExecuteSQL(requete) 
	# # #dataSource.ReleaseResultSet(layer_Select)
	# # print('inds1', layer.GetFeatureCount())
	# # print('inds2', outLayer.GetFeatureCount())
	# # #out_layer = outds.CopyLayer(layer_Select, 'sexy')
	# # #print('outds', out_layer.GetFeatureCount())
	# # print("dpt_extent", outLayer.GetExtent())
	# return 	dataSource, outLayer

# def extractByLoc(ds_rpg, ogrLayer_rpg, ds_dpt, ogrLayer_dpt, outfile):
	# """
	# Extracts features from a vector layer with regional administrative boundaries that match with an input 
	# vector layer of departmental administrative limits
	# """   
	# # ## Input
	# driver = ogr.GetDriverByName("ESRI Shapefile")
	# spatialRef_rpg = ogrLayer_rpg.GetSpatialRef()	
	
	# outDataSource = driver.CreateDataSource(r'D:\test\OUT\test.shp')
	# outLayer = outDataSource.CreateLayer('FINAL', spatialRef_rpg, geom_type=ogr.wkbMultiPolygon)

	# ## Create new layer
	# # outdriver = ogr.GetDriverByName("Memory")
	# # outds = outdriver.CreateDataSource('out')
	# # outLayer = outds.CreateLayer('attrselect', spatialRef_rpg, geom_type=ogr.wkbMultiPolygon)
	
	# ## Add fields to the new layer
	# inLayerDefn = ogrLayer_rpg.GetLayerDefn()
	# for i in range(0, inLayerDefn.GetFieldCount()):
		# fieldDefn = inLayerDefn.GetFieldDefn(i)
		# fieldName = fieldDefn.GetName()
		# outLayer.CreateField(fieldDefn)
	
	# ## Create index on ogrLayer_rpg
	# index = rtree.index.Index(interleaved=False)
	# for fid1 in range(0, ogrLayer_rpg.GetFeatureCount()):
		# feature1 = ogrLayer_rpg.GetFeature(fid1)
		# geometry1 = feature1.GetGeometryRef()
		# xmin, xmax, ymin, ymax = geometry1.GetEnvelope()
		# index.insert(fid1, (xmin, xmax, ymin, ymax))
		
	# ## Check intersection ogrLayer_dpt avec index ogrLayer_rpg
	# for fid2 in range(0, ogrLayer_dpt.GetFeatureCount()):
		# feature2 = ogrLayer_dpt.GetNextFeature()
		# geometry2 = feature2.GetGeometryRef()
		# xmin, xmax, ymin, ymax = geometry2.GetEnvelope()
		# print(xmin, xmax, ymin, ymax)
		
		# for fid1 in list(index.intersection((xmin, xmax, ymin, ymax))):
			# feature1 = ogrLayer_rpg.GetFeature(fid1)
			# geometry1 = feature1.GetGeometryRef()
			# if geometry2.Intersects(geometry1):
				# # print ('{} intersects {}'.format(fid2, fid1))
				# outLayer.CreateFeature(feature1)
			
		# return 	ds_rpg, outLayer		
			
			# # ogr.Layer.Intersection(ogrLayer_rpg, ogrLayer_dpt, outLayer)
			# # print(outLayer.GetFeatureCount())
			# # outDataSource.Destroy()

# def fields(outputRPG, field_prior, field_class, nbprior, classe):
	# """
	# Creates new fields and assigns a class index (number) and a priority that will
	# be reused as a burn-in value to rasterize the vector layers
	# """
	# # Add fields to the new layer
	# inLayerDefn = outputRPG.GetLayerDefn()
	# for i in range(0, inLayerDefn.GetFieldCount()):
		# fieldDefn = inLayerDefn.GetFieldDefn(i)
		# fieldName = fieldDefn.GetName()
		# outLayer.CreateField(fieldDefn)
		
	# # Add selected features to the new layer
	# for feat in outputRPG:
		# outLayer.CreateFeature(feat)
	
	# # Add fields
	# coord_fld_prior = ogr.FieldDefn(field_prior, ogr.OFTInteger)
	# coord_fld_prior.SetWidth(5)
	# coord_fld_prior.SetPrecision(3)
	# outLayer.CreateField(coord_fld_prior)
	# coord_fld_class = ogr.FieldDefn(field_class, ogr.OFTString)
	# coord_fld_class.SetWidth(5)
	# outLayer.CreateField(coord_fld_class)

	# # Set values in fields	
	# for feature in outLayer:
		# feature.SetField(field_prior, nbprior)
		# feature.SetField(field_class, classe)
		# outLayer.SetFeature(feature) 
	
#----- MAIN FUNCTION -----------

def extractRPG(outputdir, rpg_path, field_rpg, field_value, champ_dpt, dpt_chemin, field1, field2, nbprior, classe, nomSortie):
	#starttime = time.time()
	# ----- Checks validity on the geometries of the regional layer 
	#valid_ds, validRpg = verifValid(rpg_path)
	
	# # ----- Loops in a folder that contains the vector layers for several departments
	for num in numDpt: 
		print(num)
		folder_output = os.path.join(outputdir, num)
		os.mkdir(folder_output)
		# Selects by attribute one department every loop in the departmental boundaries layer 
		print("Selection du département")
		outdpt = os.path.join(outputdir, 'dpt{}.shp'.format(num))
		ds_dpt, coucheDpt = extractByAttrDpt(dpt_path, champ_dpt, num, outdpt)
		#ds_dpt, coucheDpt = extractByAttrDpt(champ_dpt, num, outdpt, path = dpt_chemin)
		#print('featurecount', coucheDpt.GetFeatureCount())
		
		# Extracts the features of the regional layer (RPG) matching with the department selected
		# print("Extraction de la classe du RPG en fonction du département")
		# path_outputRPG = os.path.join(folder_output, nomSortie)
		# print(path_outputRPG)
		# ds_rpg, coucheRpgDpt = extractByLoc(valid_ds, validRpg, ds_dpt, coucheDpt, path_outputRPG)
		
		# ----- Selects by attribute in the regional layer (e.g. RPG)
		# print('execution de selection rpg')
		# Select_Rpg = extractByAttr(field_rpg, field_value, ogrLayer = coucheRpgDpt, ds = ds_rpg)

		# feature1 = coucheDpt.GetNextFeature()
		# geometry1 = feature1.GetGeometryRef()
		# xmin, xmax, ymin, ymax = geometry1.GetEnvelope()
		# print("dpt_extent", coucheDpt.GetExtent())
		# print("dpt_envelope", geometry1.GetEnvelope())


		#print("temps d'execution : ", (endtime-starttime)/60)
		# # Creates the new fields (e.g. 'Class' and 'Priority') and assigns values (class index and priority number)
		# fields(path_outputRPG, field1, field2, nbprior, classe)

# --------   PARAMETERS ---------

# --- Input
rpg_path = r"D:\test\rpg_extract.shp"
dpt_path = r"D:\Donnees\departements\dpt_occitanie.shp"
numDpt = ["09","11","12","30","31","32","34","46","48","65","66","81","82"]

# --- Output
outputdir = r'D:\test\OUT'

# --- RPG Extraction
field_rpg = 'CODE_GROUP'
champRpgValeur = ['16','18','19']

# --- Department Extraction
field_dpt = 'code_insee'

# --- output name (merge layer)
nomSortie = 'PRAIRIE.shp'

# ---- Fields creation and modification
field_prior = 'PRIORITY'
field_class = 'CLASS'
nbprior = 3
classe = '9'

# --------   EXECUTION ----------
extractRPG(outputdir, rpg_path, field_rpg, champRpgValeur, field_dpt, dpt_path, field_prior, field_class, nbprior, classe, nomSortie)

print('traitement terminé')