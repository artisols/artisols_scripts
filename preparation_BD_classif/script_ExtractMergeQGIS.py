import processing

"""
Creation of layers by extracting by attribute (list of land use classes) in BD Topo layers and according to departments (the layers from BD topo are already classed in department folders)
Creation of new fields in the new layer and set values (class and priority)
"""	

def extractByAttr(chemin, champ, listValeur):
    """
    Selection by attribute based on the values of the field class from the input vector layer 
    """
    # Query
    if len(listValeur) == 1 :
        valeur = listValeur[0]
        requete = "{} = '{}'".format(champ, valeur)
    else :
        valeur = listValeur[0]
        requete = "{} = '{}'".format(champ, valeur)
        for valeur in listValeur[1:] :
            requete = "{} OR {} = '{}'".format(requete,champ, valeur)
    print(requete)
    
    # Select by attribute according to the query 
    alg_params = {
            'INPUT': chemin,
            'EXPRESSION': requete,
            'OUTPUT':'TEMPORARY_OUTPUT'
        }
    vectSelect = processing.run('native:extractbyexpression', alg_params)
    return vectSelect
      
def verifValid(rpg, coucheValidRpg):
    """
    Checks the geometry of a vector layer. Three vector layers are generated according to 3 groups
    (valid, invalid and error). The vector layer with valid geometries is extracted for the next step of the processing
    """ 
    alg_params = { 
            'INPUT_LAYER' : rpg, 
            'METHOD' : 2, 
            'VALID_OUTPUT' : coucheValidRpg, 
            'INVALID_OUTPUT': 'TEMPORARY_OUTPUT', 
            'ERROR_OUTPUT' : 'TEMPORARY_OUTPUT'
        }
    verifLayer = processing.run('qgis:checkvalidity', alg_params)
    return verifLayer
   
def extractByLoc(chemin, dpt):
    """
    Extracts features from a vector layer with regional administrative boundaries that match with an input 
    vector layer of departmental administrative limits
    """   
    alg_params = {
            'INPUT': chemin,
            'PREDICATE': [0],
            'INTERSECT': dpt,
            'OUTPUT': 'TEMPORARY_OUTPUT'
        }
   
    vectSelect = processing.run('native:extractbylocation', alg_params)
    return vectSelect

def layerMerge(couche1, couche2, output):
    """
    Combines two vector layers into a single one 
    Now, a single layer combines classes/features from two different sources and scales
    """
    listLayer = [couche1, couche2]
    alg_params = {
            'LAYERS': listLayer, 
            'OUTPUT': output}
    processing.run('native:mergevectorlayers', alg_params)
    
# Création et modification de nouveaux champs 'Classe' et 'Priorité'

def fields(output, field1, field2, nbprior, classe):
    """
    Creates new fields and assigns a class index (number) and a priority that will
    be reused as a burn-in value to rasterize the vector layers
    """
    # Fields creation
    out_file= QgsVectorLayer(output, '', 'ogr')
    caps = out_file.dataProvider().capabilities()
    if caps and QgsVectorDataProvider.addAttributes :
        newField1 = QgsField(field1, QVariant.Int)
        newField2 = QgsField(field2, QVariant.String)
        out_file.dataProvider().addAttributes([newField1, newField2])
        out_file.updateFields()
        
    idFieldPrior = out_file.dataProvider().fieldNameIndex(field1)
    idFieldClass = out_file.dataProvider().fieldNameIndex(field2)
    
    # Fields modification
    out_file.startEditing()
    for feat in out_file.getFeatures(): 
        out_file.changeAttributeValue(feat.id(), idFieldPrior, nbprior)
        out_file.changeAttributeValue(feat.id(), idFieldClass, classe)
    out_file.commitChanges()
    
#----- MAIN FUNCTION -----------

def fusion(outputdir, rpg_chemin, champ_rpg, champRpgValeur, dir_data, nom_couche, champ_vgt, champVgtValeur, champ_dpt, dpt_chemin, field1, field2, nbprior, classe, nomSortie):
    
    # ----- Selects by attribute in the regional layer (e.g. RPG)
    print('execution de selection rpg')
    coucheRpg = extractByAttr(rpg_chemin , champ_rpg, champRpgValeur)

    # ----- Checks validity on the geometries of the regional layer 
    validRpg = verifValid(coucheRpg['OUTPUT'], coucheValidRpg)

    # ----- Loops in a folder that contains the vector layers for several departments
    for root, dirs, files in os.walk(dir_data):
        for file in files :
           if nom_couche in file :
               # Searches the department number in the name of the folders
               numDpt = os.path.split(os.path.split(root)[0])[1].split('_')[1]
               print('---\ntraitement du dpt : ', numDpt)
               
               # Selects by attribute the features of an expected class in the "departmental" layer (e.g. BD TOPO)
               chemin_topo = os.path.join(root,file)
               print(chemin_topo)
               coucheVgt = extractByAttr(chemin_topo, champ_vgt, champVgtValeur)
               
               # Selects by attribute one department every loop in the departmental boundaries layer 
               coucheDpt = extractByAttr(dpt_chemin, champ_dpt, [numDpt])
               
               # Extracts the features of the regional layer (RPG) matching with the department selected
               coucheRpgDpt = extractByLoc(validRpg['VALID_OUTPUT'], coucheDpt['OUTPUT'])
               
               # Merges the features from the BD Topo (departmental layer) and the one extracted from the regional layer (RPG) by department
               path_fusion = os.path.join(outputdir, numDpt, nomSortie.format(numDpt))
               layerMerge(coucheVgt['OUTPUT'], coucheRpgDpt['OUTPUT'], path_fusion)
               
               # Creates the new fields (e.g. 'Class' and 'Priority') and assigns values (class index and priority number)
               fields(path_fusion, field1, field2, nbprior, classe)

# --------   PARAMETERS ---------

# --- Input
rpg_chemin = r"D:\Donnees\RPG_2017\PARCELLES_GRAPHIQUES.shp"
dir_data = r'D:\Donnees\BD_TOPO'
dpt_chemin = r'D:\Donnees\departements\dpt_occitanie.shp'

# --- Output
outputdir = r'D:\Donnees\output_donnees\Dpt'
coucheValidRpg = r'D:\Donnees\validRpg.shp'


# --- RPG Extraction
champ_rpg = 'CODE_GROUP'
champRpgValeur = ['20']

# --- BD_TOPO Extraction
nom_couche = 'ZONE_VEGETATION.SHP'
champ_vgt = 'NATURE'
champVgtValeur = ['Verger']

# --- Department Extraction
champ_dpt = 'code_insee'

# --- output name (merge layer)
nomSortie = 'Verger_{}.shp'

# ---- Fields creation and modification
field1 = 'PRIORITE'
field2 = 'CLASSE'
nbprior = 3
classe = '12'

# --------   EXECUTION ----------
fusion(outputdir, rpg_chemin, champ_rpg, champRpgValeur, dir_data, nom_couche, champ_vgt, champVgtValeur, champ_dpt, dpt_chemin, field1, field2, nbprior, classe, nomSortie)

print('traitement terminé')