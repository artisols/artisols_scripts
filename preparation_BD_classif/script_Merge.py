import processing


def layerMerge(couche1, couche2, output):
    """
    Combines two vector layers into a single one 
    Now, a single layer combines classes/features from two different sources and scales
    """
    listLayer = [couche1, couche2, couche3]
    alg_params = {
            'LAYERS': listLayer, 
            'OUTPUT': output}
    processing.run('native:mergevectorlayers', alg_params)
    
# Création et modification de nouveaux champs 'Classe' et 'Priorité'

def fields(output, field1, field2, nbprior, classe):
    """
    Creates new fields and assigns a class index (number) and a priority that will
    be reused as a burn-in value to rasterize the vector layers
    """
    out_file= QgsVectorLayer(output, '', 'ogr')

    idFieldPrior = out_file.dataProvider().fieldNameIndex(field1)
    idFieldClass = out_file.dataProvider().fieldNameIndex(field2)
    
    # Fields modification
    out_file.startEditing()
    for feat in out_file.getFeatures(): 
        out_file.changeAttributeValue(feat.id(), idFieldPrior, nbprior)
        out_file.changeAttributeValue(feat.id(), idFieldClass, classe)
    out_file.commitChanges()
    
#----- MAIN FUNCTION -----------

def fusion(outputdir, dir_data, field1, field2, nbprior, classe, nomSortie):
    
    # ----- Loops in a folder that contains the vector layers for several departments
    for num in listDpt:  
        for root, dirs, files in os.walk(dir_data):
            for file in files :
               if 'GRANDE_CULTURE' in file or 'MARAICHAGE' in file or 'PRAIRIE' in file :
                   # Searches the department number in the name of the folders
                   numDpt = os.path.split((os.path.split(root)[0])[1].split('D0')[1])[1]
                   print('---\ntraitement du dpt : ', numDpt)
                   
                   # # Merges the features from the BD Topo (departmental layer) and the one extracted from the regional layer (RPG) by department
                   # path_fusion = os.path.join(outputdir, numDpt, nomSortie)
                   # layerMerge(coucheVgt['OUTPUT'], coucheRpgDpt['OUTPUT'], path_fusion)
                   
                   # # Creates the new fields (e.g. 'Class' and 'Priority') and assigns values (class index and priority number)
                   # fields(path_fusion, field1, field2, nbprior, classe)

# --------   PARAMETERS ---------
# --- Input
dir_data = r'D:\Donnees\output_donnees\BD_OCS'

# --- Output
outputdir = r'D:\Donnees\output_donnees\BD_OCS'

# --- Department Extraction
listDpt = ["09","11","12","30","31","32","34","46","48","65","66","81","82"]
# --- output name (merge layer)
nomSortie = 'CULTURES.shp'

# ---- Fields creation and modification
field1 = 'PRIORITE'
field2 = 'CLASSE'
nbprior = 4
classe = '8'

# --------   EXECUTION ----------
fusion(outputdir, dir_data, field1, field2, nbprior, classe, nomSortie)

print('traitement terminé')