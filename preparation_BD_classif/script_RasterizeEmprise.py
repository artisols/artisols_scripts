# _*_ coding:utf-8 _*_

import gdal, ogr, osr
import gdalconst
import os, sys

"""
rasterize vector file according to department boundaries selected in the shapefile department (in this case : 81 et 82)
"""

# RASTERIZE FUNCTION

def createEmptyRaster(QgsRectangle, outputRaster):

    alg_params = {
            'EXTENT': QgsRectangle,
            'TARGET_CRS': 'EPSG:2154',
            'PIXEL_SIZE': 1.5,
            'NUMBER': 0,
            'OUTPUT': outputRaster
        }
   
    raster = processing.run('qgis:createconstantrasterlayer', alg_params)
    return raster
    
def extractByAttr(chemin, sortieExtract, champ, listValeur):

    # Query 
    if len(listValeur) == 1 :
        valeur = listValeur[0]
        requete = "{} = '{}'".format(champ, valeur)
    else :
        valeur = listValeur[0]
        requete = "{} = '{}'".format(champ, valeur)
        for valeur in listValeur[1:] :
            requete = "{} OR {} = '{}'".format(requete,champ, valeur)
    print(requete)
    
    # Select by attribute according to the query 
    alg_params = {
            'INPUT': chemin,
            'EXPRESSION': requete,
            'OUTPUT': sortieExtract
        }
    vectSelect = processing.run('native:extractbyexpression', alg_params)
    return vectSelect

def burnRaster(inVector, fieldname, refImg):

    dsRaster = gdal.Open(refImg, gdalconst.GA_Update)

    # open vector file
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dsVector = gdal.OpenEx(inVector, gdal.OF_VECTOR)

    # burn value
    options = gdal.RasterizeOptions(bands=[1], attribute=fieldname)
    gdal.Rasterize(dsRaster, dsVector, options=options)
    
    # close files
    dsRaster = None
    dsVector = None

# CREATION OF A LIST OF VECTOR LAYERS (PER DEPARTMENT) THAT INTERSECT THE SATELLITE IMAGES USED TO RASTERIZE THE VECTOR DATA

def createList(dirData, layerList):

    maList = list()
    
    for couche in layerList: 
        for root, reps, files in os.walk(dirData):
            for file in files:
                if couche == file :
                    path_layer = os.path.join(root, file)
                    print('LE path_layer EST LE SUIVANT', path_layer )
                    maList.append(path_layer)
#                    print(maList)
    return maList


#--- PARAMETERS ---

dir_ocs = r"D:\Donnees\Evaluation_results\BDOCS_2018"
path_emprise = r"D:\Donnees\Evaluation_results\Emprises_10km_tests.shp"
outdir_Raster = r'D:\Donnees\Evaluation_results\Raster'
outdir_Vecteur = r'D:\Donnees\Evaluation_results\Vecteur'
dptList = ['81','82']
champDpt = 'Dpt'

layerList = ["AUTRE_VEGETATION.SHP",
                        "SABLE.SHP",
                        "CULTURES.SHP",
                        "CARRIERES.SHP",
                        "FORET.SHP",
                        "HAIE.SHP",
                        "EAU.SHP",
                        "MER.SHP",
                        "VIGNE.SHP",
                        "VERGER.SHP",
                        "SPORT_IMPERMEABLE.SHP",
                        "CAMPING.SHP",
                        "GOLF.SHP",
                        "PARKING.SHP",
                        "CIMETIERE.SHP",
                        "SERRE.SHP",
                        "BATI_ACTIVITE.SHP",
                        "BATI_RESIDENTIEL.SHP"]


# ---- MAIN FUNCTION ----

for num in dptList :
    path_folderDpt = os.path.join(dir_ocs, num)
    maList = createList(path_folderDpt, layerList)
    sortieExtract = os.path.join(outdir_Vecteur, 'Emprise{}.shp'.format(num))
    print('LE PATH_EMPRISE', path_emprise)
    print('LA SORTIE EXTRACT', sortieExtract)
    carreDpt = extractByAttr(path_emprise, sortieExtract, champDpt, [num])
    emprise = QgsVectorLayer(sortieExtract, 'emprise')
    extentDpt = emprise.extent()
    print('extent du département :', num, extentDpt.toString())
    #type(extent09)
    outRaster = os.path.join(outdir_Raster, 'raster{}.tif'.format(num))
    print(outRaster)
    emptyRaster = createEmptyRaster(extentDpt, outRaster)
    for ocs in maList:
        print('ocs', ocs)
        burnRaster(ocs, 'CLASSE', outRaster)

print('terminé')