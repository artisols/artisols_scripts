"""
Creation of layers by extracting by attribute (list of land use classes) in BD Topo layers and according to departments (the layers from BD topo are already classed in department folders)
Creation of new fields in the new layer and set values (class and priority)
"""	
from osgeo import ogr
import os,sys

def selectAttr(path_file, listValues, field, output, nbprior, classe):
	# Get the features of the department layer
	driver = ogr.GetDriverByName("ESRI Shapefile")
	dataSource = driver.Open(path_file, 0)
	layerDpt = dataSource.GetLayer()
	spatialRef_ocsol = layerDpt.GetSpatialRef()
	layername = layerDpt.GetName()
	print(layername)

	# Query
	if len(listValues) == 1 :
		valeur = listValues[0]
		requete = "SELECT * FROM {} WHERE {} = '{}'".format(layername, field, valeur)
	else :
		valeur = listValues[0]
		requete = "SELECT * FROM {} WHERE {} = '{}'".format(layername, field, valeur)
		for valeur in listValues[1:] :
			requete = "{} OR {} = '{}'".format(requete,field, valeur)
	print(requete)
	layer_Select = dataSource.ExecuteSQL(requete)

	# Create new layer
	outDataSource = driver.CreateDataSource(output)
	outLayer = outDataSource.CreateLayer('FINAL', spatialRef_ocsol, geom_type=ogr.wkbMultiPolygon)
	
	# Add fields to the new layer
	inLayerDefn = layer_Select.GetLayerDefn()
	for i in range(0, inLayerDefn.GetFieldCount()):
		fieldDefn = inLayerDefn.GetFieldDefn(i)
		fieldName = fieldDefn.GetName()
		outLayer.CreateField(fieldDefn)
		
	# Add selected features to the new layer
	for feat in layer_Select:
		outLayer.CreateFeature(feat)
	
	# Add fields
	coord_fld_prior = ogr.FieldDefn('PRIORITY', ogr.OFTInteger)
	coord_fld_prior.SetWidth(5)
	coord_fld_prior.SetPrecision(3)
	outLayer.CreateField(coord_fld_prior)
	coord_fld_class = ogr.FieldDefn('CLASS', ogr.OFTString)
	coord_fld_class.SetWidth(5)
	outLayer.CreateField(coord_fld_class)

	# Set values in fields	
	for feature in outLayer:
		feature.SetField("PRIORITY", nbprior)
		feature.SetField("CLASS", classe)
		outLayer.SetFeature(feature) 
	
# --- EXECUTION ----

#	argv_1 : input landuse layers folder (BD TOPO) in which the folders (with all the land use layers) per department are. These folders must be named as 'BDTopo_09" for instance (dpt number)
#	argv_2 : output folder 
#	argv_3 : name of the input landuse layer (with the extension) in which will be extracted (by attribute) the expected classes
#	argv_4 : basename of the output layer
#	i.e python script_extractAttrDptOGR.py D:\Donnees\BD_TOPO2018 D:\test\OUT ZONE_VEGETATION.SHP vegetation

if __name__ == "__main__":

	dir_data = sys.argv[1]
	dir_output = sys.argv[2]
	layer_shp = sys.argv[3]
	name_out = sys.argv[4]
	listFile = []

	for root, dirs, files in os.walk(dir_data):
		for file in files :
			if layer_shp in file :
				numDpt = os.path.split(os.path.split(root)[0])[1].split('_')[1]
				repDpt = os.path.join(dir_output, numDpt)
				if not os.path.exists(repDpt):
					os.mkdir(repDpt)
				listFile.append(file)
				path = os.path.join(root, file)
				output_name = str(name_out)+'_'+numDpt+'.shp'
				output = os.path.join(repDpt, output_name)
				selectAttr(path, ['Haie', 'Bois'], 'NATURE', output, 1, '1')
	print('fin du traitement')