#-*- coding:utf8 -*-
"""
Creation of layers by extracting by attribute (list of land use classes) in BD Topo layers and according to departments (the layers from BD topo are already classed in department folders)
Creation of new fields in the new layer and set values (class and priority)
"""	
import os
from qgis.core import * 
from qgis.PyQt.QtCore import QVariant

def selectAttr(path_file, listValues, field, output, nbprior, classe):
  
    fichier = QgsVectorLayer(path_file, '', 'ogr')

    # Query
    if len(listValues) == 1 :
        valeur = listValues[0]
        requete = "{} = '{}'".format(field, valeur)
    else :
        valeur = listValues[0]
        requete = "{} = '{}'".format(field, valeur)
        for valeur in listValues[1:] :
            requete = "{} OR {} = '{}'".format(requete,field, valeur)

    selectValeur = fichier.selectByExpression(requete)
    writer = QgsVectorFileWriter.writeAsVectorFormat(fichier, output, "utf-8", fichier.crs(), "ESRI shapefile", onlySelected = True)

    # Fields creation
    out_file= QgsVectorLayer(output, '', 'ogr')
    
    caps = out_file.dataProvider().capabilities()
    if caps and QgsVectorDataProvider.addAttributes :
        fieldPrior = QgsField('PRIORITE', QVariant.Int)
        fieldClasse = QgsField('CLASSE', QVariant.String)
        out_file.dataProvider().addAttributes([fieldPrior, fieldClasse])
        out_file.updateFields()
        
    idFieldPrior = out_file.dataProvider().fieldNameIndex("PRIORITE")
    idFieldClass = out_file.dataProvider().fieldNameIndex("CLASSE")
    
    # Fields modification
    
    out_file.startEditing()
    
    for feat in out_file.getFeatures(): 
        out_file.changeAttributeValue(feat.id(), idFieldPrior, nbprior)
        out_file.changeAttributeValue(feat.id(), idFieldClass, classe)
    out_file.commitChanges()

# --- PARAMETERS ----
#    argv_1 : input landuse layers folder (BD TOPO) in which the folders (with all the land use layers) per department are. These folders must be named as 'BDTopo_09" for instance (dpt number)
#    argv_2 : output folder 
#    argv_3 : name of the input landuse layer (with the extension) in which will be extracted (by attribute) the expected classes
#    argv_4 : basename of the output layer
dir_data = r'D:\Donnees\BD_TOPO2018'
dir_output = r'D:\test\OUT'
layer_shp = 'ZONE_VEGETATION.SHP'
name_out = 'VEGETATION'
listFile = []

# --- EXECUTION ----
for root, dirs, files in os.walk(dir_data):
    for file in files :
        if layer_shp in file :
            numDpt = os.path.split(os.path.split(root)[0])[1].split('_')[1]
            repDpt = os.path.join(dir_output, numDpt)
            if not os.path.exists(repDpt):
                os.mkdir(repDpt)
            listFile.append(file)
            path = os.path.join(root, file)
            output_name = name_out +'_'+numDpt+'.shp'
            output = os.path.join(repDpt, output_name)
            selectAttr(path, ['Haie', 'Bois'], 'NATURE', output, 1, '1')

print('traitement termin�')