import os
from osgeo import ogr

""" Count the number of features in a shapefile"""

layerList = ["AUTRE_VEGETATION.SHP",
						"SABLE.SHP",
						"PRAIRIE.SHP",
						"FORET.SHP",
						"EAU.SHP",
						"MARAICHAGE.SHP",
						"VIGNE.SHP",
						"GRANDE_CULTURE.SHP",
						"VERGER.SHP",
						"CARRIERES.SHP",
						"SPORT_IMPERMEABLE.SHP",
						"AERODROME.SHP",
						"CAMPING.SHP",
						"GOLF.SHP",
						"PARKING.SHP",
						"CIMETIERE.SHP",
						"HAIE.SHP",
						"SERRE.SHP",
						"BATI_ACTIVITE.SHP",
						"BATI_RESIDENTIEL.SHP"]

dirData = r'D:\Donnees\output_donnees\BD_OCS'

for shape in layerList : 
	listCount = []
	somme = 0
	for root, reps, files in os.walk(dirData):
		for file in files :
			if shape == file :
				path_layer = os.path.join(root, file)
				driver = ogr.GetDriverByName('ESRI Shapefile')
				dataSource = driver.Open(path_layer, 0) # 0 means read-only. 1 means writeable.
				print 'Opened %s' % (path_layer)
				layer = dataSource.GetLayer()
				featureCount = layer.GetFeatureCount()
				print "Number of features in %s: %d" % (os.path.basename(path_layer),featureCount)
				listCount.append(featureCount)
	somme = sum(listCount)
	print('Total feature count' + shape + ':\n' + str(somme))
