# _*_ coding:utf-8 _*_

import gdal, ogr, osr
import gdalconst
import os, sys

def polygonize(chemin, sortie):

    alg_params = {
            'INPUT': chemin,
            'BAND': 1,
            'FIELD': 'CLASSE',
            'EIGHT_CONNECTEDNESS' : 8,
            'OUTPUT': sortie
        }
   
    polygonRaster = processing.run('gdal:polygonize', alg_params)
    
    return polygonRaster


data_dir = r'D:\Donnees\Evaluation_results\Raster2'
out_vect = r'D:\Donnees\Evaluation_results\Vecteur'

for root, dirs, files in os.walk(data_dir):
    for file in files :
        num = os.path.split(os.path.split(file)[1])[1].split('.')[0].split('er')[1]
        print(num)
        path_raster = os.path.join(root,file)
        print(path_raster)
        Vect_out = outRaster = os.path.join(out_vect, 'EmpriseOCS_{}.shp'.format(num))
        print(Vect_out)
        polygonize(path_raster, Vect_out)

print("terminé")
            
    