# _*_ coding:utf-8 _*_

import gdal, ogr, osr
import gdalconst
import os, sys

"""
rasterize vector file according to an existing extent (in this case : Emprise09.shp - Ariège)
"""

# RASTERIZE FUNCTION

def extent(path_emprise):
    emprise = QgsVectorLayer(path_emprise, 'emprise')
    extentDpt = emprise.extent()
    
    return extentDpt

def createEmptyRaster(QgsRectangle, outputRaster):

    alg_params = {
            'EXTENT': QgsRectangle,
            'TARGET_CRS': 'EPSG:2154',
            'PIXEL_SIZE': 1.5,
            'OUTPUT': outputRaster
        }
   
    raster = processing.run('qgis:createconstantrasterlayer', alg_params)
    return raster
    
def extractByAttr(chemin, champ, listValeur):

    # Query 
    if len(listValeur) == 1 :
        valeur = listValeur[0]
        requete = "{} = '{}'".format(champ, valeur)
    else :
        valeur = listValeur[0]
        requete = "{} = '{}'".format(champ, valeur)
        for valeur in listValeur[1:] :
            requete = "{} OR {} = '{}'".format(requete,champ, valeur)
    print(requete)
    
    # Select by attribute according to the query 
    alg_params = {
            'INPUT': chemin,
            'EXPRESSION': requete,
            'OUTPUT':'TEMPORARY_OUTPUT'
        }
    vectSelect = processing.run('native:extractbyexpression', alg_params)
    return vectSelect

def burnRaster(inVector, fieldname, refImg):

    dsRaster = gdal.Open(refImg, gdalconst.GA_Update)

    # open vector file
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dsVector = gdal.OpenEx(inVector, gdal.OF_VECTOR)

    # burn value
    options = gdal.RasterizeOptions(bands=[1], attribute=fieldname)
    gdal.Rasterize(dsRaster, dsVector, options=options)
    
    # close files
    dsRaster = None
    dsVector = None

# CREATION OF A LIST OF VECTOR LAYERS (PER DEPARTMENT) THAT INTERSECT THE SATELLITE IMAGES USED TO RASTERIZE THE VECTOR DATA

def createList (dirData, layerList):

    maList = list()
    
    for couche in layerList: 
        for root, reps, files in os.walk(dirData):
            for file in files:
                if couche == file :
                    path_layer = os.path.join(root, file)
                    print(path_layer)
                    maList.append(path_layer)
                    print(maList)
    return maList


#--- PARAMETERS ---

dir_ocs = r'D:\Donnees\test\09'
path_emprise09 = r"D:\Donnees\Emprise09.shp"
outRaster = r'D:\Donnees\test\Raster09.tif'

layerList = ["AUTRE_VEGETATION.SHP",
                        "SABLE.SHP",
                        "CULTURES.SHP",
                        "CARRIERES.SHP",
                        "FORET.SHP",
                        "HAIE.SHP",
                        "EAU.SHP",
                        "VIGNE.SHP",
                        "VERGER.SHP",
                        "CIMETIERE.SHP",
                        "SERRE.SHP",
                        "BATI_ACTIVITE.SHP",
                        "BATI_RESIDENTIEL.SHP"]


# ---- MAIN FUNCTION ----
            
maList = createList(dir_ocs, layerList)
extent09 = extent(path_emprise09)
print(extent09.toString())
#type(extent09)
emptyRaster = createEmptyRaster(extent09, outRaster)

for ocs in maList:
   print('ocs', ocs)
   burnRaster(ocs, 'CLASSE', outRaster)

print('terminé')